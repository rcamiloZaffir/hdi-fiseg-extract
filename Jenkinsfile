#!groovy?
pipeline {

    agent { label 'node-agent'}

    tools {
        maven 'apache-maven-3'
        jdk 'jdk11' 
    }

    environment {
        ARTIFACT_GROUP = 'mx.interware.hdi'
        ARTIFACT_ID = 'hdi-fiseg-extract'
        ARTIFACT_TYPE = 'jar'
        EMAIL_BODY_BUILD = "Job status "+" - \"${env.JOB_NAME}\" \nBranch: ${env.GIT_BRANCH} \nbuild: ${env.BUILD_NUMBER}\n\nView the log at:\n ${env.BUILD_URL}"
        COMMITER_EMAIL= "devops-iw@interware.com.mx"
        DOCKER_REGISTRY="dockerhub:5000"
        POMVERSION = readMavenPom().getVersion()
        SERVICE_IP="dev.interware.com.mx"
        APP_NAME="${env.JOB_NAME}"
        SERVICE_PORT=8090
        CICD_HOST_IP="173.17.0.1"
    }


    stages {
        stage('Compile') {
            steps {
                script {
                    sh 'env'
                    COMMITER_EMAIL = sh (
                            script: 'git --no-pager show -s --format=\'%ae\'',
                            returnStdout: true
                    ).trim()
                    sh 'mvn install:install-file -Dfile=lib/kafka-avro-serializer-5.3.0.jar -DgroupId=io.confluent -DartifactId=kafka-avro-serializer -Dversion=5.3.0 -Dpackaging=jar'
                    sh 'mvn install:install-file -Dfile=lib/kafka-schema-registry-client-5.3.0.jar -DgroupId=io.confluent -DartifactId=kafka-schema-registry-client -Dversion=5.3.0 -Dpackaging=jar'
                    sh 'mvn install:install-file -Dfile=lib/common-config-5.3.0.jar -DgroupId=io.confluent -DartifactId=common-config -Dversion=5.3.0 -Dpackaging=jar'
                    sh 'mvn install:install-file -Dfile=lib/common-utils-5.3.0.jar -DgroupId=io.confluent -DartifactId=common-utils -Dversion=5.3.0 -Dpackaging=jar'
                    sh 'mvn install:install-file -Dfile=lib/hdi-security-cryptography-1.0.0.jar -DgroupId=hdi-security -DartifactId=hdi-security-cryptography -Dversion=1.0.0 -Dpackaging=jar'
                    sh 'mvn install:install-file -Dfile=lib/hdi-spring-extensions-jdbc-datasource-1.0.0.jar -DgroupId=hdi-security -DartifactId=hdi-spring-extensions-jdbc-datasource -Dversion=1.0.0 -Dpackaging=jar'
                    sh 'mvn clean compile  --settings settings.xml -DskipTests -U'
                }
            }
        }


        stage('Run tests') {
            parallel {
                stage('UnitTest and Sonar') {
                    steps {
//                        sh 'mvn --settings settings.xml -X clean test -U'
                        withSonarQubeEnv('sonarqube') {
                            sh "mvn --settings settings.xml -X clean package sonar:sonar -Dsonar.host.url=http://${CICD_HOST_IP}:9000 -Dsonar.projectKey=${ARTIFACT_ID} -Dsonar.projectName=${ARTIFACT_ID}"                    
                        }
                    }
                    post{ success { junit 'target/surefire-reports/**/*.xml' } }
                }
                stage('JaCoCo') {
                    steps {
                        echo 'Code Coverage'
                        step( [ $class: 'JacocoPublisher' ] )
                    }
                }
            }
        }
        stage("Quality Gate"){
            steps {
                script {
                    try{
                        timeout(time: 1, unit: 'HOURS') {
                            def qg = waitForQualityGate()                        
                            if (qg.status != 'OK') {
                                sendEmail("Pipeline aborted due to quality gate failure: ${qg.status} - ${EMAIL_BODY_BUILD}".replace("status", "FAILED"))
                                error "Pipeline aborted due to quality gate failure: ${qg.status}"
                                }
                            }
                    }catch(e){
                        echo "Error al revisar el QualityGate ${e}"
                    }
                }                    
            }
        }
        stage('Package') {
            steps {
                //sh "sed -i 's/localhost/dev.interware.com.mx/' pom.xml"
                //sh "bin/build-image ${DOCKER_REGISTRY}"                
                sh "docker build -t ${DOCKER_REGISTRY}/${ARTIFACT_ID}:${POMVERSION} -f docker/Dockerfile ."
           }
        }

        stage('Publish') {
            steps {
                script {
                    nexusPublisher nexusInstanceId: 'nexus-devops-release', \
                    nexusRepositoryId: 'devops-releases', \
                    packages: [[$class: 'MavenPackage', \
                            mavenAssetList: [[classifier: '', \
                                    extension: '', \
                                    filePath: "target/${ARTIFACT_ID}-${POMVERSION}.${ARTIFACT_TYPE}"]], \
                            mavenCoordinate: [artifactId: "${ARTIFACT_ID}", \
                                    groupId: ARTIFACT_GROUP, \
                                    packaging: ARTIFACT_TYPE, \
                                    version: POMVERSION ]]]

                    archiveArtifacts artifacts: "target/${ARTIFACT_ID}-${POMVERSION}.${ARTIFACT_TYPE}", fingerprint: true
                    withCredentials([usernamePassword(credentialsId: 'nexusAdmin', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh "docker login -u=$USERNAME -p=$PASSWORD ${DOCKER_REGISTRY}"
                        //sh "bin/push-image"
                        sh "docker push ${DOCKER_REGISTRY}/${ARTIFACT_ID}:${POMVERSION}"
                    }
                }
            }
        }

        stage('Deploy') {
            steps {
                script {
                    sh "sed -i 's/.*image:.*/    image: ${DOCKER_REGISTRY}\\/${ARTIFACT_ID}:${POMVERSION}/' docker/docker-compose.yml"
                    DOCKER_COMPOSE = sh (
                            script: 'base64 docker/docker-compose.yml | tr -d "\n"',
                            returnStdout: true
                    ).trim()
                    ansibleDeploy(buildExtraVars(ARTIFACT_ID,"install_service", "docker_compose", DOCKER_COMPOSE), 'docker_operations', 'docker_swarm')
                }
            }
        }
        
        stage('Check service') {
            steps {
                script {
                        sh "sleep 80"
                        sh "curl http://${SERVICE_IP}:${SERVICE_PORT}/api/ping"
                        sh "curl http://${SERVICE_IP}:${SERVICE_PORT}/api/health/k1"
                }
            }
        }
    }



    post {
        always {
            echo 'JENKINS PIPELINE EXECUTION FINISH'
        }
        success {
            echo 'JENKINS PIPELINE HAS BEEN EXECUTED SUCCESSFULLY'
            sendEmail("${EMAIL_BODY_BUILD}".replace("status", "SUCCESS"))
        }
        failure {
            echo 'JENKINS PIPELINE HAS BEEN FAILED'
            sendEmail("${EMAIL_BODY_BUILD}".replace("status", "FAILED"))
        }
        aborted {
            echo 'JENKINS PIPELINE HAS BEEN ABORTED BY SYSTEM'
            sendEmail("Deploy aborted BY SYSTEM ${EMAIL_BODY_BUILD}".replace("status", "ABORTED"))
        }
    }

}

def sendEmail(String body){
    script {
        echo "Sending mail to: " + "${COMMITER_EMAIL}"
        String subject= "JENKINS Build: \"${env.JOB_NAME} - ${env.GIT_BRANCH}\" ".replace("origin/", "");
        mail    to: "${COMMITER_EMAIL}",
                subject: "${subject}",
                body: "${body}"
        echo "Finish sending email."
        echo 'Clean Workspace'
    }
}



def buildExtraVars(String stack, String operation , String type, String docker_compose ){
    String extraVars = """--- 
                        stack: "${stack}"
                        operation: "${operation}"
                        type: "${type}"
                        docker_compose: "${docker_compose}"
                        """
    extraVars
}


def ansibleDeploy(String extraVars,
                  String jobTemplate,
                  String limit){
    println "Executing ansible playbook."
    echo extraVars
    result = ansibleTower(
            async: false,
            credential: '',
            extraVars: "${extraVars}",
            importTowerLogs: true,
            importWorkflowChildLogs: false,
            inventory: '',
            jobTags: '',
            jobTemplate: "${jobTemplate}",
            jobType: 'run',
            limit: "${limit}",
            removeColor: false,
            skipJobTags: '',
            templateType: 'job',
            throwExceptionWhenFail: false,
            towerCredentialsId: 'ansible_tower',
            towerServer: 'Ansible Tower Demo',
            verbose: false)
    println "Finish ansible playbook." + result
    if (result['JOB_RESULT'] != 'SUCCESS') {
        error "Pipeline aborted, because deploy fail"
    }
}   

