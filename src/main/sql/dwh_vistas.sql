go
CREATE DATABASE HDI_DWH;
go
use HDI_DWH;

if object_id ('dbo.Vw_BI_AutrEmisionDetalle') is not null
  drop table dbo.Vw_BI_AutrEmisionDetalle
go

if object_id ('dbo.Vw_BI_AuttEmisionDetalle') is not null
  drop table dbo.Vw_BI_AuttEmisionDetalle
go
if object_id ('dbo.Vw_Bi_AutrEmisionDetalleCobertura') is not null
  drop table dbo.Vw_Bi_AutrEmisionDetalleCobertura
go
if object_id ('dbo.Vw_Bi_AuttEmisionDetalleCobertura') is not null
  drop table dbo.Vw_Bi_AuttEmisionDetalleCobertura
go

CREATE TABLE Vw_BI_AutrEmisionDetalle(
	FechaTransaccion	INT,
	IdOficina	SMALLINT,
	NumPoliza	BIGINT,
	NumReferencia	VARCHAR(25),
	NumCertificado	VARCHAR(10),
	FechaInicioVigencia	DATE,
	FechaFinVigencia	DATE,
	NumeroSerie	VARCHAR(40),
	IdMarcaVehiculo	INT,
	ModeloVehiculo	SMALLINT,
	TipoPersona	VARCHAR(20),
	Apellido1	VARCHAR(32),
	Apellido2	VARCHAR(32),
	Nombre	VARCHAR(32),
	RazonSocial	VARCHAR(124),
	Y_O	VARCHAR(510),
	IdCanalVenta	SMALLINT,
	DescCanalVenta	VARCHAR(100),
	NipPerfilAgente	INT,
	NombrePerfilAgente	VARCHAR(131),
	NumDocumento	SMALLINT,
	IdTipoDocumento	SMALLINT,
	DescTipoDocumento	VARCHAR(100),
	IdCausaCancelacion	INT,
	DescCausaCancelacion	VARCHAR(100),
	FechaEmision	DATETIME,
	CodigoPostal	VARCHAR(10),
	RFC	VARCHAR(18),
	NombrePais	VARCHAR(80),
	NombreEstado	VARCHAR(80),
	NombreMunicipio	VARCHAR(80),
	NombreCiudad	VARCHAR(80),
	IdPaquete	SMALLINT,
	DescPaquete	VARCHAR(100),
	DescGrupoPaquete	VARCHAR(100),
	Placas	VARCHAR(30),
	IdEstadoCliente	SMALLINT,
	IdTipoPoliza INT,
	NumCotizacion INT,
	IdMarcaTipoOcra INT,
	IdUsoCNSF INT,
	DescUsoCNSF VARCHAR(200),
	CuentaConSeguroObligatorio INT,
	IdClienteEntidad INT,
	IdClienteMunicipio INT,
	IdTipoSeguro INT,
	IdTipoSumaAsegurada INT,
	IdGrupoDocumento SMALLINT,
	DescGrupoDocumento VARCHAR(30),
	TiempoUltimoCambio	DATETIME,
	 AnioPoliza INT
);


CREATE TABLE Vw_BI_AuttEmisionDetalle(
	FechaTransaccion	INT,
	IdOficina	SMALLINT,
	NumPoliza	BIGINT,
	NumReferencia	VARCHAR(25),
	NumCertificado	VARCHAR(10),
	FechaInicioVigencia	DATE,
	FechaFinVigencia	DATE,
	NumeroSerie	VARCHAR(40),
	IdMarcaVehiculo	INT,
	ModeloVehiculo	SMALLINT,
	TipoPersona	VARCHAR(20),
	Apellido1	VARCHAR(32),
	Apellido2	VARCHAR(32),
	Nombre	VARCHAR(32),
	RazonSocial	VARCHAR(124),
	Y_O	VARCHAR(510),
	IdCanalVenta	SMALLINT,
	DescCanalVenta	VARCHAR(100),
	NipPerfilAgente	INT,
	NombrePerfilAgente	VARCHAR(131),
	NumDocumento	SMALLINT,
	IdTipoDocumento	SMALLINT,
	DescTipoDocumento	VARCHAR(100),
	IdCausaCancelacion	INT,
	DescCausaCancelacion	VARCHAR(100),
	FechaEmision	DATETIME,
	CodigoPostal	VARCHAR(10),
	RFC	VARCHAR(18),
	NombrePais	VARCHAR(80),
	NombreEstado	VARCHAR(80),
	NombreMunicipio	VARCHAR(80),
	NombreCiudad	VARCHAR(80),
	IdPaquete	SMALLINT,
	DescPaquete	VARCHAR(100),
	DescGrupoPaquete	VARCHAR(100),
	IdEstadoCliente	SMALLINT,
	IdTipoPoliza INT,
	NumCotizacion INT,
	IdMarcaTipoOcra INT,
	IdUsoCNSF INT,
	DescUsoCNSF VARCHAR(200),
	CuentaConSeguroObligatorio INT,
	IdClienteEntidad INT,
	IdClienteMunicipio INT,
	IdTipoSeguro INT,
	IdTipoSumaAsegurada INT,
	IdGrupoDocumento SMALLINT,
	DescGrupoDocumento VARCHAR(30),
	TiempoUltimoCambio	DATETIME,
	AnioPoliza INT ,
	SubAgente INT 
);
CREATE TABLE Vw_Bi_AutrEmisionDetalleCobertura(
	FechaTransaccion	INT,
	NumDocumento	SMALLINT,
	IdOficina	SMALLINT,
	NumPoliza	BIGINT,
	NumCertificado	VARCHAR(10),
	IdTipoDocumento	SMALLINT,
	IdReglaCobertura	SMALLINT,
	IdCobertura	SMALLINT,
	IdCoberturaRR8	SMALLINT,
	DescCobertura	VARCHAR(100),
	SumaAsegurada	DECIMAL(20,4),
	NumCotizacion INT,
	IdEstatusCobertura INT,
	TiempoUltimoCambio	DATETIME
);

CREATE TABLE Vw_Bi_AuttEmisionDetalleCobertura(
	FechaTransaccion	INT,
	NumDocumento	SMALLINT,
	IdOficina	SMALLINT,
	NumPoliza	BIGINT,
	NumCertificado	VARCHAR(10),
	IdReglaCobertura	SMALLINT,
	IdCobertura	SMALLINT,
	IdCoberturaRR8	SMALLINT,
	DescCobertura	VARCHAR(100),
	SumaAsegurada	DECIMAL(20,4),
	SubAgente	INT,	-- NipPerfilAgente se cambia por SubAgente
	NumCotizacion INT,
	IdEstatusCobertura INT,
	TiempoUltimoCambio	DATETIME
);

-- 	ALTER TABLE Vw_Bi_AuttEmisionDetalleCobertura
--	NipPerfilAgente se cambia por SubAgente
--	EXEC HDI_DWH.sys.sp_rename N'HDI_DWH.dbo.Vw_Bi_AuttEmisionDetalleCobertura.NipPerfilAgente' , N'SubAgente', 'COLUMN' GO

CREATE INDEX Vw_BI_AutrEmisionDetalle_IDX ON Vw_BI_AutrEmisionDetalle (IdOficina, NumPoliza, NumDocumento, IdTipoDocumento);
CREATE INDEX Vw_BI_AuttEmisionDetalle_IDX ON Vw_BI_AuttEmisionDetalle (IdOficina, NumPoliza, NumDocumento, NipPerfilAgente);

CREATE INDEX Vw_Bi_AutrEmisionDetalleCobertura_IDX ON Vw_Bi_AutrEmisionDetalleCobertura (IdOficina, NumPoliza, NumDocumento, IdTipoDocumento);
CREATE INDEX Vw_Bi_AuttEmisionDetalleCobertura_IDX ON Vw_Bi_AuttEmisionDetalleCobertura (IdOficina, NumPoliza, NumDocumento, NipPerfilAgente);

-- 	ALTER TABLE
--	ALTER TABLE HDI_DWH.dbo.Vw_BI_AuttEmisionDetalle ADD SubAgente int NULL GO
