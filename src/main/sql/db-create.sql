CREATE DATABASE MSSQL_MOVIMIENTOSBLOCKCHAIN;
-- -------------------------------------------------------------------------
-- Creación de tablas
-- -------------------------------------------------------------------------
use MSSQL_MOVIMIENTOSBLOCKCHAIN
go


if object_id ('dbo.Tb_Mov_AutPoliza') is not null
  drop table dbo.Tb_Mov_AutPoliza
go

create table Tb_Mov_AutPoliza (
  Pol_Id                        bigint  identity                         not null
  , Pol_Str                     varchar(100)                             not null
  , CanalVenta                  int                  default                 null
  , NumeroPoliza                varchar(25)                              not null
  , NumeroCotizacion            varchar(25)          default                 null
  , NumeroReferencia            bigint               default                 null
  , UbicacionEmision            int                  default                 null
  , LineaNegocio                varchar(10)          default                 null
  , AgenciaId                   varchar(10)          default                 null
  , AgenteId                    varchar(36)          default                 null
  , Pol_FechaAlta               datetime             default getdate()   not null
  , Pol_UsuarioAlta             nvarchar( 36 )       default ''          not null
  , Pol_FechaCambio             datetime             default '19000101'  not null
  , Pol_UsuarioCambio           nvarchar( 36 )       default ''          not null
  , Tab_Estatus                 int                  default 171         not null
  , constraint Pk_Mov_AutPoliza primary key ( Pol_Id )
)
go


if object_id ('dbo.Tb_Mov_AutMovimientoPoliza') is not null
  drop table dbo.Tb_Mov_AutMovimientoPoliza
go

create table Tb_Mov_AutMovimientoPoliza (
  MovPol_Id                     bigint identity                          not null
  , MovPol_Str                  varchar(100)                             not null
  , FechaMovimiento             datetime                                 not null
  , NumeroDocumento             smallint                                 not null
  , TipoMovimiento              smallint                                 not null
  , TipoPoliza                  smallint                                 not null
  , NumeroVehiculo              smallint                                 not null
  , EstatusProcesamiento        smallint                                     null
  , MensajeProcesamiento        varchar(200)                                 null
  , FechaCaptura                datetime                                 not null
  , FechaEmision                datetime                                 not null
  , Pol_Id                      bigint                                   not null
  , FechaCancelacion            datetime                                     null
  , FechaInicioVigencia         datetime                                 not null
  , FechaFinalVigencia          datetime                                 not null
  , MovPol_FechaAlta            datetime             default getdate()   not null
  , MovPol_UsuarioAlta          nvarchar( 36 )       default ''          not null
  , MovPol_FechaCambio          datetime             default '19000101'  not null
  , MovPol_UsuarioCambio        nvarchar( 36 )       default ''          not null 
  , Tab_Estatus                 int                  default 171         not null
  , constraint Pk_Mov_AutMovimientoPoliza primary key ( MovPol_Id )
  , foreign key (Pol_Id) references Tb_Mov_AutPoliza(Pol_Id)
)
go


if object_id ('dbo.Tb_Mov_AutClientePoliza') is not null
  drop table dbo.Tb_Mov_AutClientePoliza
go

create table Tb_Mov_AutClientePoliza (
  CtePol_Id                     bigint                                    not null
  , ApellidoPaterno             varchar(100)                                 null
  , ApellidoMaterno             varchar(100)                                 null
  , Nombre                      varchar(100)                                 null
  , Rfc                         varchar(15)                                  null
  , Curp                        varchar(20)                                  null
  , MovPol_Id                   bigint                                   not null
  , CtePol_FechaAlta            datetime             default getdate()   not null
  , CtePol_UsuarioAlta          nvarchar( 36 )       default ''          not null
  , CtePol_FechaCambio          datetime             default '19000101'  not null
  , CtePol_UsuarioCambio        nvarchar( 36 )       default ''          not null 
  , Tab_Estatus                 int                  default 171         not null
  , constraint Pk_Mov_AutClientePoliza primary key ( CtePol_Id )
  , foreign key (MovPol_Id) references Tb_Mov_AutMovimientoPoliza(MovPol_Id)
)
go


if object_id ('dbo.Tb_Mov_AutCoberturaPoliza') is not null
  drop table dbo.Tb_Mov_AutCoberturaPoliza
go


create table Tb_Mov_AutCoberturaPoliza (
  CobPol_Id                     bigint identity                          not null
  , Cob_Cve                     smallint                                     null
  , SumaAseguradaCve            smallint                                     null
  , SumaAsegurada               decimal(18, 2)                               null
  , Estatus                     smallint                                     null
  , MovPol_Id                   bigint                                   not null
  , CobPol_FechaAlta            datetime             default getdate()   not null
  , CobPol_UsuarioAlta          nvarchar( 36 )       default ''          not null
  , CobPol_FechaCambio          datetime             default '19000101'  not null
  , CobPol_UsuarioCambio        nvarchar( 36 )       default ''          not null
  , Tab_Estatus                 int                  default 171         not null
  , constraint Pk_Mov_AutCoberturaPoliza primary key ( CobPol_Id )
  , foreign key (MovPol_Id) references Tb_Mov_AutMovimientoPoliza(MovPol_Id)
)
go


if object_id ('dbo.Tb_Mov_AutVehiculo') is not null
  drop table dbo.Tb_Mov_AutVehiculo
go


create table Tb_Mov_AutVehiculo (
  Vehiculo_Id                bigint identity                          not null
  , Uso                      int                                      not null
  , DescripcionUso           varchar(200)                             not null
  , Vin                      varchar(100)                                 null
  , Obligatorio              int                                          null
  , Modelo                   int                                          null
  , Placa                    varchar(100)                                 null
  , Inciso                   varchar(100)                                 null
  , MarcaTipo                int                                          null
  , UbicacionAsegurado       int                                          null
  , TipoSeguro               int                                          null
  , Beneficiario_Id          bigint                                       null
  , MovPol_Id                bigint                                       null
  , Vehi_FechaAlta           datetime             default getdate()   not null
  , Vehi_UsuarioAlta         nvarchar( 36 )       default ''          not null
  , Vehi_FechaCambio         datetime             default '19000101'  not null
  , Vehi_UsuarioCambio       nvarchar( 36 )       default ''          not null
  , Tab_Estatus              int                  default 171         not null
  , constraint Pk_Mov_AutVehiculo primary key ( Vehiculo_Id )
  , foreign key (MovPol_Id) references Tb_Mov_AutMovimientoPoliza(MovPol_Id) 
)
go

if object_id ('dbo.Tb_Mov_AutBeneficiario') is not null
  drop table dbo.Tb_Mov_AutBeneficiario
go

create table Tb_Mov_AutBeneficiario (
  Beneficiario_Id             bigint identity                          not null 
  , ApellidoPaterno           varchar(100)                                 null
  , ApellidoMaterno           varchar(100)                                 null
  , Nombre                    varchar(100)                                 null
  , Rfc                       varchar(20)                                  null
  , Curp                      varchar(20)                                  null 
  , TipoPersona               char(2)                                      null
  , Vehiculo_Id               bigint                                   not null
  , Benef_FechaAlta           datetime             default getdate()   not null
  , Benef_UsuarioAlta         nvarchar( 36 )       default ''          not null
  , Benef_FechaCambio         datetime             default '19000101'  not null
  , Benef_UsuarioCambio       nvarchar( 36 )       default ''          not null
  , Tab_Estatus               int                  default 171         not null
  , constraint Pk_Mov_AutBeneficiario primary key ( Beneficiario_Id )
  ,foreign key (Vehiculo_Id) references Tb_Mov_AutVehiculo(Vehiculo_Id)
)
go



if object_id ('dbo.Tb_Mov_AutClienteVehiculo') is not null
  drop table dbo.Tb_Mov_AutClienteVehiculo
go



create table Tb_Mov_AutClienteVehiculo (
  CteVehiculo_Id                bigint identity                          not null 
  , ApellidoPaterno             varchar(100)                                 null
  , ApellidoMaterno             varchar(100)                                 null
  , Nombre                      varchar(100)                             not null
  , Vehiculo_Id                 bigint                                   not null
  , CteVehi_FechaAlta           datetime             default getdate()   not null
  , CteVehi_UsuarioAlta         nvarchar( 36 )       default ''          not null
  , CteVehi_FechaCambio         datetime             default '19000101'  not null
  , CteVehi_UsuarioCambio       nvarchar( 36 )       default ''          not null
  , Tab_Estatus                 int                  default 171         not null
  , constraint Pk_Mov_AutClienteVehiculo primary key ( CteVehiculo_Id )
  ,foreign key (Vehiculo_Id) references Tb_Mov_AutVehiculo(Vehiculo_Id)
)
go


if object_id ('dbo.Tb_Mov_AutCoberturaVehiculo') is not null
  drop table dbo.Tb_Mov_AutCoberturaVehiculo
go



create table Tb_Mov_AutCoberturaVehiculo (
  CobVehiculo_Id                bigint identity                          not null
  , Cob_Cve                     smallint                                     null
  , SumaAseguradaCve            smallint                                 not null
  , SumaAsegurada               decimal(18, 2)                           not null
  , Estatus                     smallint                                     null
  , Vehiculo_Id                 bigint                                   not null
  , CobVehi_FechaAlta           datetime             default getdate()   not null
  , CobVehi_UsuarioAlta         nvarchar( 36 )       default ''          not null
  , CobVehi_FechaCambio         datetime             default '19000101'  not null
  , CobVehi_UsuarioCambio       nvarchar( 36 )       default ''          not null
  , Tab_Estatus                 int                  default 171         not null
  , constraint Pk_Mov_AutCoberturaVehiculo primary key ( CobVehiculo_Id )
  ,foreign key (Vehiculo_Id) references Tb_Mov_AutVehiculo(Vehiculo_Id)
)
go


if object_id ('dbo.Tb_Mov_AutProcesoExtraccion') is not null
  drop table dbo.Tb_Mov_AutProcesoExtraccion
go


create table Tb_Mov_AutProcesoExtraccion (
  ProcExtrac_Id                 int                                      not null
  , TipoProcesamiento           varchar(10)                              not null
  , Descripcion                 varchar(100)                                 null
  , FechaExtraccion             datetime                                 not null
  , UltimoRegistroExtraido      varchar(100)                             not null
  , FechaUltimoRegistro         datetime                                 not null
  , ProcExtr_FechaAlta          datetime             default getdate()   not null
  , ProcExtr_UsuarioAlta        nvarchar( 36 )       default ''          not null
  , ProcExtr_FechaCambio        datetime             default '19000101'  not null
  , ProcExtr_UsuarioCambio      nvarchar( 36 )       default ''          not null
  , Tab_Estatus                 int                  default 171         not null
  , constraint Pk_Mov_AutProcesoExtraccion primary key ( ProcExtrac_Id )
)
go

if object_id ('dbo.Tb_Mov_AutHistoriaProcesamiento') is not null
  drop table dbo.Tb_Mov_AutHistoriaProcesamiento
go


create table Tb_Mov_AutHistoriaProcesamiento (
  HistProcesamiento_Id           bigint identity                          not null 
  , ProcExtrac_Id                int                                      not null
  , ProcExtrac_Uuid              char(36)                                 not null
  , TipoEvento                   smallint                                 not null
  , FechaEvento                  datetime                                 not null
  , CantidadMovimientos          int                                      not null
  , HistProc_FechaAlta           datetime             default getdate()   not null
  , HistProc_UsuarioAlta         nvarchar( 36 )       default ''          not null
  , HistProc_FechaCambio         datetime             default '19000101'  not null
  , HistProc_UsuarioCambio       nvarchar( 36 )       default ''          not null
  , Tab_Estatus                  int                  default 171         not null
  , constraint Pk_Mov_AutHistoriaProcesamiento primary key ( HistProcesamiento_Id )
  , foreign key (ProcExtrac_Id) references Tb_Mov_AutProcesoExtraccion(ProcExtrac_Id) 
)
go



-- -------------------------------------------------------------------------
-- Creación de indices
-- -------------------------------------------------------------------------
create nonclustered index Ix02_Mov_AutPoliza_PolizaStr ON Tb_Mov_AutPoliza(Pol_Str);

create nonclustered index Ix03_Mov_AutMovimientoPoliza_MovPol_Str ON Tb_Mov_AutMovimientoPoliza(MovPol_Str);

create nonclustered index Ix04_Mov_AutMovimientoPoliza_FechaMovimiento on Tb_Mov_AutMovimientoPoliza (FechaMovimiento);

create nonclustered index Ix05_Mov_AutMovimientoPoliza_Pol_Id on Tb_Mov_AutMovimientoPoliza (Pol_Id);

create nonclustered index Ix06_Mov_AutMovimientoPoliza_FechaEmision on Tb_Mov_AutMovimientoPoliza (FechaEmision);

create nonclustered index Ix07_Mov_AutMovimientoPoliza_EstatusProc on Tb_Mov_AutMovimientoPoliza (EstatusProcesamiento);

create nonclustered index Ix08_Mov_AutVehiculo_Vin on Tb_Mov_AutVehiculo (Vin);

create nonclustered index Ix09_Mov_AutVehiculo_MovPol_Id on Tb_Mov_AutVehiculo (MovPol_Id);

create nonclustered index Ix10_Mov_AutClienteVehiculo_Nombre on Tb_Mov_AutClienteVehiculo (Nombre);

create nonclustered index Ix11_Mov_AutClienteVehiculo_Vehiculo_Id on Tb_Mov_AutClienteVehiculo (Vehiculo_Id);

create nonclustered index Ix12_Mov_AutHistoriaProcesamiento_PrcExtrc_Uuid ON Tb_Mov_AutHistoriaProcesamiento(ProcExtrac_Uuid);



 INSERT INTO Tb_Mov_AutProcesoExtraccion 
    ( ProcExtrac_Id
       , TipoProcesamiento
       , Descripcion
       , FechaExtraccion
       , UltimoRegistroExtraido
       , FechaUltimoRegistro
    )
    VALUES 
    ( 1
      ,  'AUTR'
      ,  'Autos residentes'
      , '20200820'
      , '-'
      ,'20200820'
     )
      ,
      ( 2
      , 'AUTT'
      , 'Autos turistas'
      ,'20200820'
      , '-'
      , '20200820'
     );
