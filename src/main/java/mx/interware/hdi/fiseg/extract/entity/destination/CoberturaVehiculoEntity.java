package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entidad que contiene las propiedades de una cobertura d un vehiculo de la
 * base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutCoberturaVehiculo", schema = "{datasources.destination.schema}")
public class CoberturaVehiculoEntity extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = -6026860830691370316L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CobVehiculo_Id")
	@JsonProperty("coberturaVehiculoId")
	private Long coberturaVehiculoId;

	@Column(name = "Cob_Cve")
	@JsonProperty("coberturaCve")
	private Short coberturaCveCobVehi;

	@Column(name = "SumaAseguradaCve")
	@JsonProperty("sumaAseguradaCve")
	private Short sumaAseguradaCveCobVehi;

	@Column(name = "SumaAsegurada")
	@JsonProperty("sumaAsegurada")
	private BigDecimal sumaAseguradaCobVehi;

	@Column(name = "Estatus")
	@JsonProperty("estatus")
	private Short estatusCobVehi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Vehiculo_Id")
	private VehiculoEntity vehiculo;

    @Column(name = "CobVehi_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaCobVehi;

    @Column(name = "CobVehi_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaCobVehi;

    @Column(name = "CobVehi_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioCobVehi;

    @Column(name = "CobVehi_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioCobVehi;

	/**
	 * @return the coberturaVehiculoId
	 */
	public Long getCoberturaVehiculoId() {
		return coberturaVehiculoId;
	}

	/**
	 * @param coberturaVehiculoId the coberturaVehiculoId to set
	 */
	public void setCoberturaVehiculoId(Long coberturaVehiculoId) {
		this.coberturaVehiculoId = coberturaVehiculoId;
	}

	/**
	 * @return the coberturaCve
	 */
	public Short getCoberturaCveCobVehi() {
		return coberturaCveCobVehi;
	}

	/**
	 * @param coberturaCve the coberturaCve to set
	 */
	public void setCoberturaCveCobVehi(Short coberturaCve) {
		this.coberturaCveCobVehi = coberturaCve;
	}

	/**
	 * @return the sumaAseguradaCve
	 */
	public Short getSumaAseguradaCveCobVehi() {
		return sumaAseguradaCveCobVehi;
	}

	/**
	 * @param sumaAseguradaCve the sumaAseguradaCve to set
	 */
	public void setSumaAseguradaCveCobVehi(Short sumaAseguradaCve) {
		this.sumaAseguradaCveCobVehi = sumaAseguradaCve;
	}

	/**
	 * @return the sumaAsegurada
	 */
	public BigDecimal getSumaAseguradaCobVehi() {
		return sumaAseguradaCobVehi;
	}

	/**
	 * @param sumaAsegurada the sumaAsegurada to set
	 */
	public void setSumaAseguradaCobVehi(BigDecimal sumaAsegurada) {
		this.sumaAseguradaCobVehi = sumaAsegurada;
	}

	/**
	 * @return the estatus
	 */
	public Short getEstatusCobVehi() {
		return estatusCobVehi;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatusCobVehi(Short estatus) {
		this.estatusCobVehi = estatus;
	}

	/**
	 * @return the vehiculo
	 */
	public VehiculoEntity getVehiculo() {
		return vehiculo;
	}

	/**
	 * @param vehiculo the vehiculo to set
	 */
	public void setVehiculo(VehiculoEntity vehiculo) {
		this.vehiculo = vehiculo;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CoberturaVehiculoEntity)) {
			return false;
		}

		Long id = ((CoberturaVehiculoEntity) o).getCoberturaVehiculoId();
		return (coberturaVehiculoId != null) && (coberturaVehiculoId.equals(id));
	}

	@Override
	public int hashCode() {
		return 773;
	}

    /**
     * @return the fechaAltaCobVehi
     */
    public Date getFechaAltaCobVehi() {
        return fechaAltaCobVehi;
    }

    /**
     * @param fechaAltaCobVehi the fechaAltaCobVehi to set
     */
    public void setFechaAltaCobVehi(Date fechaAltaCobVehi) {
        this.fechaAltaCobVehi = fechaAltaCobVehi;
    }

    /**
     * @return the usuarioAltaCobVehi
     */
    public String getUsuarioAltaCobVehi() {
        return usuarioAltaCobVehi;
    }

    /**
     * @param usuarioAltaCobVehi the usuarioAltaCobVehi to set
     */
    public void setUsuarioAltaCobVehi(String usuarioAltaCobVehi) {
        this.usuarioAltaCobVehi = usuarioAltaCobVehi;
    }

    /**
     * @return the fechaCambioCobVehi
     */
    public Date getFechaCambioCobVehi() {
        return fechaCambioCobVehi;
    }

    /**
     * @param fechaCambioCobVehi the fechaCambioCobVehi to set
     */
    public void setFechaCambioCobVehi(Date fechaCambioCobVehi) {
        this.fechaCambioCobVehi = fechaCambioCobVehi;
    }

    /**
     * @return the usuarioCambioCobVehi
     */
    public String getUsuarioCambioCobVehi() {
        return usuarioCambioCobVehi;
    }

    /**
     * @param usuarioCambioCobVehi the usuarioCambioCobVehi to set
     */
    public void setUsuarioCambioCobVehi(String usuarioCambioCobVehi) {
        this.usuarioCambioCobVehi = usuarioCambioCobVehi;
    }
	
}
