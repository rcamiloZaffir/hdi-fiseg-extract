package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.interware.hdi.fiseg.extract.entity.destination.VehiculoEntity;
/**
 * Repository con las operaciones necesarias para el manejo de las entidades Vehiculo
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface VehiculoDAO extends JpaRepository<VehiculoEntity, Long> {

    @Query("SELECT v FROM VehiculoEntity v WHERE v.movimientoPoliza.movimientoPolizaId = ?1")
    VehiculoEntity findByIdMovimientoPoliza(Long idMovimiento);
}
