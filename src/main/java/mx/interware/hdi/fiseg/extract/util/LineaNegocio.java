package mx.interware.hdi.fiseg.extract.util;
/**
 * Enumeraci&oacute;n con los posibles lineas de negocio del proceso 
 * 
 * @author Interware
 * @since 2020-10-18
 *
 */
public enum LineaNegocio {
	AUTR((short) 1, "AUTR", "Autos residentes"), AUTT((short) 2, "AUTT", "Autos turistas");

	private Short key;
	private String id;
	private String description;
	/**
	 * Constructor que incializa el Enum
	 * 
	 * @param key         Key
	 * @param id          Identificador
	 * @param description Descripcio&oacute;n
	 */
	LineaNegocio(Short key, String id, String description) {
		this.key = key;
		this.id = id;
		this.description = description;
	}
	/**
	 * Retorna el Key
	 * 
	 * @return Key
	 */
	public Short getKey() {
		return key;
	}
	/**
	 * Retorna el Identificador
	 * 
	 * @return Identificador
	 */
	public String getId() {
		return id;
	}
	/**
	 * Retorna la descripci&oacute;n
	 * 
	 * @return Descripci&oacute;n
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * Busca una Linea de negocio por el id enviado
	 * 
	 * @param id Identificador para la b&uacute;squeda
	 * @return Objeto encontrado
	 */
	public static LineaNegocio findById(String id) {
		for (LineaNegocio lineaNegocio : values()) {
			if (lineaNegocio.getId().equals(id)) {
				return lineaNegocio;
			}
		}
		return null;
	}
}