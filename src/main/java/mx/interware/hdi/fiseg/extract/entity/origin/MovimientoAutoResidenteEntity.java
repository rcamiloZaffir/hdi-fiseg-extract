package mx.interware.hdi.fiseg.extract.entity.origin;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad que contiene las propiedades de la vista con informacion de movimiento de Vehiculos Residentes
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@IdClass(MovimientoResidentesIdDWH.class)
@Table(name = "Vw_BI_AutrEmisionDetalle", schema = "{datasources.origin.schema}", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "IdOficina", "NumPoliza", "NumCertificado", "NumDocumento", "IdTipoPoliza", "NumCotizacion" }) })
public class MovimientoAutoResidenteEntity extends MovimientoDWH implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "Placas")
    private String placas;

    /**
     * @return the placas
     */
    public String getPlacas() {
        return placas;
    }

    /**
     * @param placas the placas to set
     */
    public void setPlacas(String placas) {
        this.placas = placas;
    }


}
