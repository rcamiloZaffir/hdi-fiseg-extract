package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entidad que contiene las propiedades de un cliente asociado a un vehiculo de la base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutClienteVehiculo", schema = "{datasources.destination.schema}")
public class ClienteVehiculoEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 8734964900406335760L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CteVehiculo_Id")
    @JsonProperty("clienteVehiculoId")
    private Long clienteVehiculoId;

    @Column(name = "ApellidoPaterno")
    @JsonProperty("apellidoPaterno")
    private String apellidoPaternoCte;

    @Column(name = "ApellidoMaterno")
    @JsonProperty("apellidoMaterno")
    private String apellidoMaternoCte;

    @Column(name = "Nombre")
    @JsonProperty("nombre")
    private String nombreCte;

    @Column(name = "Rfc")
    @JsonProperty("rfc")
    private String rfcCte;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Vehiculo_Id")
    private VehiculoEntity vehiculo;

    @Column(name = "CteVehi_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaCteVehi;

    @Column(name = "CteVehi_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaCteVehi;

    @Column(name = "CteVehi_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioCteVehi;

    @Column(name = "CteVehi_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioCteVehi;

    /**
     * @return the clienteVehiculoId
     */
    public Long getClienteVehiculoId() {
        return clienteVehiculoId;
    }

    /**
     * @param clienteVehiculoId the clienteVehiculoId to set
     */
    public void setClienteVehiculoId(Long clienteVehiculoId) {
        this.clienteVehiculoId = clienteVehiculoId;
    }

    /**
     * @return the apellidoPaterno
     */
    public String getApellidoPaternoCte() {
        return apellidoPaternoCte;
    }

    /**
     * @param apellidoPaterno the apellidoPaterno to set
     */
    public void setApellidoPaternoCte(String apellidoPaterno) {
        this.apellidoPaternoCte = apellidoPaterno;
    }

    /**
     * @return the apellidoMaterno
     */
    public String getApellidoMaternoCte() {
        return apellidoMaternoCte;
    }

    /**
     * @param apellidoMaterno the apellidoMaterno to set
     */
    public void setApellidoMaternoCte(String apellidoMaterno) {
        this.apellidoMaternoCte = apellidoMaterno;
    }

    /**
     * @return the nombre
     */
    public String getNombreCte() {
        return nombreCte;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombreCte(String nombre) {
        this.nombreCte = nombre;
    }

    /**
     * @return the vehiculo
     */
    public VehiculoEntity getVehiculo() {
        return vehiculo;
    }

    /**
     * @param vehiculo the vehiculo to set
     */
    public void setVehiculo(VehiculoEntity vehiculo) {
        this.vehiculo = vehiculo;
    }

    /**
     * @return the fechaAltaCteVehi
     */
    public Date getFechaAltaCteVehi() {
        return fechaAltaCteVehi;
    }

    /**
     * @param fechaAltaCteVehi the fechaAltaCteVehi to set
     */
    public void setFechaAltaCteVehi(Date fechaAltaCteVehi) {
        this.fechaAltaCteVehi = fechaAltaCteVehi;
    }

    /**
     * @return the usuarioAltaCteVehi
     */
    public String getUsuarioAltaCteVehi() {
        return usuarioAltaCteVehi;
    }

    /**
     * @param usuarioAltaCteVehi the usuarioAltaCteVehi to set
     */
    public void setUsuarioAltaCteVehi(String usuarioAltaCteVehi) {
        this.usuarioAltaCteVehi = usuarioAltaCteVehi;
    }

    /**
     * @return the fechaCambioCteVehi
     */
    public Date getFechaCambioCteVehi() {
        return fechaCambioCteVehi;
    }

    /**
     * @param fechaCambioCteVehi the fechaCambioCteVehi to set
     */
    public void setFechaCambioCteVehi(Date fechaCambioCteVehi) {
        this.fechaCambioCteVehi = fechaCambioCteVehi;
    }

    /**
     * @return the usuarioCambioCteVehi
     */
    public String getUsuarioCambioCteVehi() {
        return usuarioCambioCteVehi;
    }

    /**
     * @param usuarioCambioCteVehi the usuarioCambioCteVehi to set
     */
    public void setUsuarioCambioCteVehi(String usuarioCambioCteVehi) {
        this.usuarioCambioCteVehi = usuarioCambioCteVehi;
    }

    /**
     * @return the rfcCte
     */
    public String getRfcCte() {
        return rfcCte;
    }

    /**
     * @param rfcCte the rfcCte to set
     */
    public void setRfcCte(String rfcCte) {
        this.rfcCte = rfcCte;
    }
}
