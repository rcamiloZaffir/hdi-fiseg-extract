package mx.interware.hdi.fiseg.extract.entity.origin;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad que contiene las propiedades de la vista con informacion de movimiento de Vehiculos Turistas
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@IdClass(MovimientoTuristasIdDWH.class)
@Table(name = "Vw_BI_AuttEmisionDetalle", schema = "{datasources.origin.schema}", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "IdOficina", "NumPoliza", "NumCertificado", "NumDocumento", "SubAgente" }) })
public class MovimientoAutoTuristaEntity extends MovimientoDWH implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SubAgente")
    private Integer subAgente;

	/**
	 * @return the subAgente
	 */
	public Integer getSubAgente() {
		return subAgente;
	}

	/**
	 * @param subAgente the subAgente to set
	 */
	public void setSubAgente(Integer subAgente) {
		this.subAgente = subAgente;
	}

}
