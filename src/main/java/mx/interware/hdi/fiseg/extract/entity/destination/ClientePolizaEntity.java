package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entidad que contiene las propiedades de un cliente asociado a poliza de la base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutClientePoliza", schema = "{datasources.destination.schema}")
public class ClientePolizaEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -9106265633794114451L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CtePol_Id")
    @JsonProperty("clientePolizaId")
    private Long clientePolizaId;

    @Column(name = "ApellidoPaterno")
    @JsonProperty("apellidoPaterno")
    private String apellidoPaterno;

    @Column(name = "ApellidoMaterno")
    @JsonProperty("apellidoMaterno")
    private String apellidoMaterno;

    @Column(name = "Nombre")
    @JsonProperty("nombre")
    private String nombre;

    @Column(name = "Rfc")
    @JsonProperty("rfc")
    private String rfc;

    @Column(name = "Curp")
    @JsonProperty("curp")
    private String curp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MovPol_Id")
    private MovimientoPolizaEntity movimientoPoliza;

    @Column(name = "CtePol_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaCtePol;

    @Column(name = "CtePol_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaCtePol;

    @Column(name = "CtePol_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioCtePol;

    @Column(name = "CtePol_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioCtePol;

    /**
     * @return the clientePolizaId
     */
    public Long getClientePolizaId() {
        return clientePolizaId;
    }

    /**
     * @param clientePolizaId the clientePolizaId to set
     */
    public void setClientePolizaId(Long clientePolizaId) {
        this.clientePolizaId = clientePolizaId;
    }

    /**
     * @return the apellidoPaterno
     */
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * @param apellidoPaterno the apellidoPaterno to set
     */
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    /**
     * @return the apellidoMaterno
     */
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * @param apellidoMaterno the apellidoMaterno to set
     */
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the rfc
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * @param rfc the rfc to set
     */
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    /**
     * @return the curp
     */
    public String getCurp() {
        return curp;
    }

    /**
     * @param curp the curp to set
     */
    public void setCurp(String curp) {
        this.curp = curp;
    }

    /**
     * @return the movimientoPoliza
     */
    public MovimientoPolizaEntity getMovimientoPoliza() {
        return movimientoPoliza;
    }

    /**
     * @param movimientoPoliza the movimientoPoliza to set
     */
    public void setMovimientoPoliza(MovimientoPolizaEntity movimientoPoliza) {
        this.movimientoPoliza = movimientoPoliza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientePolizaEntity)) {
            return false;
        }

        Long id = ((ClientePolizaEntity) o).getClientePolizaId();
        return (clientePolizaId != null) && (clientePolizaId.equals(id));
    }

    @Override
    public int hashCode() {
        return 774;
    }

    /**
     * @return the fechaAltaCtePol
     */
    public Date getFechaAltaCtePol() {
        return fechaAltaCtePol;
    }

    /**
     * @param fechaAltaCtePol the fechaAltaCtePol to set
     */
    public void setFechaAltaCtePol(Date fechaAltaCtePol) {
        this.fechaAltaCtePol = fechaAltaCtePol;
    }

    /**
     * @return the usuarioAltaCtePol
     */
    public String getUsuarioAltaCtePol() {
        return usuarioAltaCtePol;
    }

    /**
     * @param usuarioAltaCtePol the usuarioAltaCtePol to set
     */
    public void setUsuarioAltaCtePol(String usuarioAltaCtePol) {
        this.usuarioAltaCtePol = usuarioAltaCtePol;
    }

    /**
     * @return the fechaCambioCtePol
     */
    public Date getFechaCambioCtePol() {
        return fechaCambioCtePol;
    }

    /**
     * @param fechaCambioCtePol the fechaCambioCtePol to set
     */
    public void setFechaCambioCtePol(Date fechaCambioCtePol) {
        this.fechaCambioCtePol = fechaCambioCtePol;
    }

    /**
     * @return the usuarioCambioCtePol
     */
    public String getUsuarioCambioCtePol() {
        return usuarioCambioCtePol;
    }

    /**
     * @param usuarioCambioCtePol the usuarioCambioCtePol to set
     */
    public void setUsuarioCambioCtePol(String usuarioCambioCtePol) {
        this.usuarioCambioCtePol = usuarioCambioCtePol;
    }

}
