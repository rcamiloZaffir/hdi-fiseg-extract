package mx.interware.hdi.fiseg.extract.dto;

import mx.interware.hdi.fiseg.extract.util.StringUtils;
/**
 * Clase abstracta con la definici&oacute;n del toString para todos los
 * elementos que la heredaran
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public abstract class AbstractDTO {

	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return StringUtils.toJson(this);
	}

}
