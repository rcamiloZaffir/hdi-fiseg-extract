package mx.interware.hdi.fiseg.extract.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mx.interware.hdi.fiseg.extract.exception.ExtractException;

/**
 * Utileria para manejo de fechas dentro del microservicio
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class DateUtils {
    private static final Logger LOGGER = LogManager.getLogger(DateUtils.class);

    private static final String PATTERN_YMD_HMS = "yyyy-MM-dd HH:mm:ss.SSS";
    private static final String PATTERN_YMD_HMS_SHORT = "yyyyMMddhhmmssSSS";
    private static final String PATTERN_YMD_SHORT = "yyyyMMdd";

    /**
     * Covierte un objeto String en formato "yyyy-MM-dd hh:mm:ss.SSS" a un {@link Date}
     * 
     * @param dateStr Objeto a convertir
     * @return Objeto convertido
     */
    public static Date parseYmdhms(String dateStr) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(PATTERN_YMD_HMS);
            return format.parse(dateStr);
        } catch (ParseException ex) {
            LOGGER.error("Error al convertir la fecha", ex);
            throw new ExtractException(ex);
        }
    }

    /**
     * Covierte un objeto {@link Date} a una cadena con formato "yyyyMMddhhmmssSSS"
     * 
     * @param date Objeto a convertir
     * @return Objeto convertido
     */
    public static String formatDateYmdhms(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(PATTERN_YMD_HMS_SHORT);
        String dateStr = formatter.format(date);
        LOGGER.trace("Resultado Conversion => {}", dateStr);
        return dateStr;
    }

    /**
     * Covierte un objeto {@link Date} a una cadena con formato "yyyyMMdd"
     * 
     * @param date Objeto a convertir
     * @return Objeto convertido
     */
    public static String formatDateYmd(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(PATTERN_YMD_SHORT);
        String dateStr = formatter.format(date);
        LOGGER.trace("Resultado conversion => {}", dateStr);
        return dateStr;
    }

    /**
     * Covierte un objeto {@link Timestamp} a una cadena con formato "yyyyMMdd"
     * 
     * @param date Objeto a convertir
     * @return Objeto convertido
     */
    public static String formatTimestamp(Timestamp date) {
        SimpleDateFormat formatter = new SimpleDateFormat(PATTERN_YMD_HMS);
        String dateStr = formatter.format(date);
        LOGGER.trace("Resultado conversion => {}", dateStr);
        return dateStr;
    }

    /**
     * Covierte un objeto String en formato "yyyyMMdd" a un {@link Date}
     * 
     * @param dateStr Objeto a convertir
     * @return Objeto convertido
     */
    public static Date parseDateYmd(String dateStr) {
        try {
            LOGGER.trace("Iniciando parseo");
            SimpleDateFormat formatter = new SimpleDateFormat(PATTERN_YMD_SHORT);
            return formatter.parse(dateStr);
        } catch (ParseException ex) {
            LOGGER.error("Error al convertir la fecha a formato yyyyMMdd", ex);
            throw new ExtractException(ex);
        }
    }

    /**
     * Constructor privado de la clase
     */
    private DateUtils() {

    }

}