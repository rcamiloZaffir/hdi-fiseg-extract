package mx.interware.hdi.fiseg.extract.repository.origin;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.interware.hdi.fiseg.extract.entity.origin.CoberturaAutoResidenteEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoAutoResidenteEntity;
/**
 * Repository con las operaciones necesarias para el manejo de Movimientos de autos residentes
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface MovimientoAutoResidenteDAO extends JpaRepository<MovimientoAutoResidenteEntity, Integer> {

	/**
	 * Obtiene una lista de movimientos de autos residentes a partir de la fecha de ultimo registro
	 * 
	 * @param fechaUltimoRegistro Criterio de b&uacute;squeda
	 * @return Lista de movimientos encontrados
	 */
	@Query(value = "SELECT TOP {datasource.origin.block.size} * FROM {datasources.origin.schema}.Vw_BI_AutrEmisionDetalle WHERE TiempoUltimoCambio > ?1 AND numDocumento >= 0 ORDER BY TiempoUltimoCambio ASC, NumPoliza ASC,NumDocumento ASC", nativeQuery = true)
	public List<MovimientoAutoResidenteEntity> findUnprocessedAUTR(String fechaUltimoRegistro);

	/**
	 * Obtiene una lista de coberturas para movimientos de autos turistas a partir
	 * de determinados par&aacute;metros
	 * 
	 * @param idOficina Identificador de oficinal
	 * @param numPoliza N&uacute;mero de poliza
	 * @param numDocumento N&uacute;mero de documento
	 * @param idTipoDocumento Identificador del tipo de documento
	 * @return Lista de Coberturas encontradas
	 */
	@Query("SELECT c FROM CoberturaAutoResidenteEntity c WHERE idOficina = ?1 AND numPoliza = ?2 AND numDocumento = ?3 AND idTipoDocumento = ?4")
	public List<CoberturaAutoResidenteEntity> findCoberturasAUTR(Short idOficina, Long numPoliza, Short numDocumento, Short idTipoDocumento);

	   /**
     * Obtiene una lista de coberturas para movimientos de autos turistas a partir
     * de determinados par&aacute;metros
     * 
     * @param idOficina Identificador de oficinal
     * @param numPoliza N&uacute;mero de poliza
     * @param numDocumento N&uacute;mero de documento
     * @param idTipoDocumento Identificador del tipo de documento
     * @param numCotizacion Identificador del numero de cotizacion
     * @param numCertificado Identificador del numero de certificado
     * @return Lista de Coberturas encontradas
     */
	@Query("select  distinct c FROM CoberturaAutoResidenteEntity c WHERE idOficina = ?1 AND numPoliza = ?2 AND numDocumento = ?3 AND idTipoDocumento = ?4 AND numCotizacion=?5 AND numCertificado=?6")
	public List<CoberturaAutoResidenteEntity> findCoberturasFlotillaAUTR(Short idOficina, Long numPoliza, Short numDocumento, Short idTipoDocumento, Integer numCotizacion,String numCertificado);

}



