package mx.interware.hdi.fiseg.extract.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.io.ByteSource;

import mx.interware.hdi.fiseg.extract.exception.ExtractException;

/**
 * 
 * Utileria para el manejo de las cadenas "Strings" dentro del MS
 * 
 * @author Interware
 * @since 2020-10-17
 * 
 */
public class StringUtils {

    private static final Logger LOGGER = LogManager.getLogger(StringUtils.class);

    private static ObjectMapper mapper = new ObjectMapper();

    public static String generateRandomString(int size) {
        String randomStr = UUID.randomUUID().toString();
        LOGGER.info("UUID=> {},{}", randomStr,size);
        return randomStr;
    }

    /**
     * Transforma un objeto a un formato JSON
     * 
     * @param obj Objeto a transformar
     * @return JSON generado
     */
    public static String toJson(Object obj) {
        try {
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            mapper.disable(MapperFeature.AUTO_DETECT_CREATORS, MapperFeature.AUTO_DETECT_FIELDS, MapperFeature.AUTO_DETECT_GETTERS,
                    MapperFeature.AUTO_DETECT_IS_GETTERS);
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            String jsonStr = mapper.writeValueAsString(obj);
            LOGGER.trace("{}", jsonStr);
            return jsonStr;
        } catch (JsonProcessingException e) {
            LOGGER.error("Error al parsear el objeto", e);
            throw new ExtractException(e);
        }
    }

    /**
     * Returna un id &uacute;nico
     * 
     * @return Id generado
     */
    public static String generateRandomUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Restorna un String a partir de un flujo de bites
     * 
     * @param inputStream Flujo de donde se obtiene la informaci&oacute;n
     * @return String recuperado
     */
    public static String streamAsString(InputStream inputStream) {
        try {
            ByteSource byteSource = new ByteSource() {
                @Override
                public InputStream openStream() {
                    return inputStream;
                }
            };
            return byteSource.asCharSource(StandardCharsets.UTF_8).read();
        } catch (IOException ex) {
            LOGGER.error("Error al parsear el objeto", ex);
            throw new ExtractException(ex);
        }
    }

    /**
     * Constructor privado de la clase
     */
    private StringUtils() {

    }

}