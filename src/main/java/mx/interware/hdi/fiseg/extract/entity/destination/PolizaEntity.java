package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

import mx.interware.hdi.fiseg.extract.util.AddItem;
import mx.interware.hdi.fiseg.extract.util.RemoveItem;

/**
 * Entidad que contiene las propiedades de una poliza de la base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutPoliza", schema = "{datasources.destination.schema}")
public class PolizaEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -5109849568795099401L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Pol_Id")
    @JsonProperty("polizaId")
    private Long polizaId;

    @Column(name = "CanalVenta")
    @JsonProperty("canalVenta")
    private Short canalVenta;

    @Column(name = "Pol_Str")
    @JsonProperty("polizaStr")
    private String polizaStr;

    @Column(name = "NumeroPoliza")
    @JsonProperty("numeroPoliza")
    private String numeroPoliza;

    @Column(name = "NumeroCotizacion")
    @JsonProperty("numeroCotizacion")
    private String numeroCotizacion;

    @Column(name = "NumeroReferencia")
    @JsonProperty("numeroReferencia")
    private Long numeroReferencia;

    @Column(name = "UbicacionEmision")
    @JsonProperty("ubicacionEmision")
    private Integer ubicacionEmision;

    @Column(name = "LineaNegocio")
    @JsonProperty("lineaNegocio")
    private String lineaNegocio;

    @Column(name = "AgenciaId")
    @JsonProperty("agenciaId")
    private String agenciaId;

    @Column(name = "AgenteId")
    @JsonProperty("agenteId")
    private String agenteId;

    @Column(name = "Poliza")
    @JsonProperty("poliza")
    private Long poliza;

    @Column(name = "Pol_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaPol;

    @Column(name = "Pol_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaPol;

    @Column(name = "Pol_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioPol;

    @Column(name = "Pol_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioPol;

    @Transient
    @JsonProperty("movimientos")
    private List<MovimientoPolizaEntity> movimientos = new ArrayList<>();

    /**
     * @return the polizaId
     */
    public Long getPolizaId() {
        return polizaId;
    }

    /**
     * @param polizaId the polizaId to set
     */
    public void setPolizaId(Long polizaId) {
        this.polizaId = polizaId;
    }

    /**
     * @return the polizaStr
     */
    public String getPolizaStr() {
        return polizaStr;
    }

    /**
     * @param polizaStr the polizaStr to set
     */
    public void setPolizaStr(String polizaStr) {
        this.polizaStr = polizaStr;
    }

    /**
     * @return the numeroPoliza
     */
    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    /**
     * @param numeroPoliza the numeroPoliza to set
     */
    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    /**
     * @return the numeroCotizacion
     */
    public String getNumeroCotizacion() {
        return numeroCotizacion;
    }

    /**
     * @param numeroCotizacion the numeroCotizacion to set
     */
    public void setNumeroCotizacion(String numeroCotizacion) {
        this.numeroCotizacion = numeroCotizacion;
    }

    /**
     * @return the canalVenta
     */
    public Short getCanalVenta() {
        return canalVenta;
    }

    /**
     * @param canalVenta the canalVenta to set
     */
    public void setCanalVenta(Short canalVenta) {
        this.canalVenta = canalVenta;
    }

    /**
     * @return the numeroReferencia
     */
    public Long getNumeroReferencia() {
        return numeroReferencia;
    }

    /**
     * @param numeroReferencia the numeroReferencia to set
     */
    public void setNumeroReferencia(Long numeroReferencia) {
        this.numeroReferencia = numeroReferencia;
    }

    /**
     * @return the ubicacionEmision
     */
    public Integer getUbicacionEmision() {
        return ubicacionEmision;
    }

    /**
     * @param ubicacionEmision the ubicacionEmision to set
     */
    public void setUbicacionEmision(Integer ubicacionEmision) {
        this.ubicacionEmision = ubicacionEmision;
    }

    /**
     * @return the lineaNegocio
     */
    public String getLineaNegocio() {
        return lineaNegocio;
    }

    /**
     * @param lineaNegocio the lineaNegocio to set
     */
    public void setLineaNegocio(String lineaNegocio) {
        this.lineaNegocio = lineaNegocio;
    }

    /**
     * @return the agenciaId
     */
    public String getAgenciaId() {
        return agenciaId;
    }

    /**
     * @param agenciaId the agenciaId to set
     */
    public void setAgenciaId(String agenciaId) {
        this.agenciaId = agenciaId;
    }

    /**
     * @return the agenteId
     */
    public String getAgenteId() {
        return agenteId;
    }

    /**
     * @param agenteId the agenteId to set
     */
    public void setAgenteId(String agenteId) {
        this.agenteId = agenteId;
    }

    /**
     * @return the movimientos
     */
    public List<MovimientoPolizaEntity> getMovimientos() {
        return movimientos;
    }

    /**
     * @param movimientos the movimientos to set
     */
    public void setMovimientos(List<MovimientoPolizaEntity> movimientos) {
        this.movimientos = movimientos;
    }

    @AddItem(entity = MovimientoPolizaEntity.class, collection = "movimientos")
    public void addMovimiento(MovimientoPolizaEntity movimiento) {
        movimientos.add(movimiento);
        movimiento.setPoliza(this);
    }

    @RemoveItem(entity = MovimientoPolizaEntity.class, collection = "movimientos")
    public void removeMovimiento(MovimientoPolizaEntity movimiento) {
        movimientos.remove(movimiento);
        movimiento.setPoliza(null);
    }

    /**
     * @return the fechaAltaPol
     */
    public Date getFechaAltaPol() {
        return fechaAltaPol;
    }

    /**
     * @param fechaAltaPol the fechaAltaPol to set
     */
    public void setFechaAltaPol(Date fechaAltaPol) {
        this.fechaAltaPol = fechaAltaPol;
    }

    /**
     * @return the usuarioAltaPol
     */
    public String getUsuarioAltaPol() {
        return usuarioAltaPol;
    }

    /**
     * @param usuarioAltaPol the usuarioAltaPol to set
     */
    public void setUsuarioAltaPol(String usuarioAltaPol) {
        this.usuarioAltaPol = usuarioAltaPol;
    }

    /**
     * @return the fechaCambioPol
     */
    public Date getFechaCambioPol() {
        return fechaCambioPol;
    }

    /**
     * @param fechaCambioPol the fechaCambioPol to set
     */
    public void setFechaCambioPol(Date fechaCambioPol) {
        this.fechaCambioPol = fechaCambioPol;
    }

    /**
     * @return the usuarioCambioPol
     */
    public String getUsuarioCambioPol() {
        return usuarioCambioPol;
    }

    /**
     * @param usuarioCambioPol the usuarioCambioPol to set
     */
    public void setUsuarioCambioPol(String usuarioCambioPol) {
        this.usuarioCambioPol = usuarioCambioPol;
    }

    /**
     * @return the poliza
     */
    public Long getPoliza() {
        return poliza;
    }

    /**
     * @param poliza the poliza to set
     */
    public void setPoliza(Long poliza) {
        this.poliza = poliza;
    }

}
