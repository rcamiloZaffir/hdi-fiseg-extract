package mx.interware.hdi.fiseg.extract.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * POJO con las propiedades de un Movimiento
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class MovimientoDTO extends AbstractDTO implements Serializable {
	private static final long serialVersionUID = -5160613262244859339L;

	private String lineaNegocio;
	private String agencia;
	private String poliza;
	private String cotizacion;
	private Integer claveEndoso;
	private Short estatus;
	private Date fechaEmision;

	/**
	 * @return the lineaNegocio
	 */
	public String getLineaNegocio() {
		return lineaNegocio;
	}

	/**
	 * @param lineaNegocio the lineaNegocio to set
	 */
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}

	/**
	 * @return the agencia
	 */
	public String getAgencia() {
		return agencia;
	}

	/**
	 * @param agencia the agencia to set
	 */
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	/**
	 * @return the poliza
	 */
	public String getPoliza() {
		return poliza;
	}

	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	/**
	 * @return the cotizacion
	 */
	public String getCotizacion() {
		return cotizacion;
	}

	/**
	 * @param cotizacion the cotizacion to set
	 */
	public void setCotizacion(String cotizacion) {
		this.cotizacion = cotizacion;
	}

	/**
	 * @return the claveEndoso
	 */
	public Integer getClaveEndoso() {
		return claveEndoso;
	}

	/**
	 * @param claveEndoso the claveEndoso to set
	 */
	public void setClaveEndoso(Integer claveEndoso) {
		this.claveEndoso = claveEndoso;
	}

	/**
	 * @return the estatus
	 */
	public Short getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the fechaEmision
	 */
	public Date getFechaEmision() {
		return fechaEmision;
	}

	/**
	 * @param fechaEmision the fechaEmision to set
	 */
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

}