package mx.interware.hdi.fiseg.extract.entity.origin;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad que contiene las propiedades de la vista con informacion de detalle de cobertura Autos residentes
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@IdClass(CoberturaResidentesIdDWH.class)
@Table(name = "Vw_Bi_AutrEmisionDetalleCobertura", schema = "{datasources.origin.schema}", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "IdOficina", "NumPoliza", "NumDocumento", "IdTipoDocumento", "NumCotizacion", "NumCertificado" }) })
public class CoberturaAutoResidenteEntity extends CoberturaDWH implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "IdTipoDocumento")
    private Short idTipoDocumento;

    /**
     * @return the idTipoDocumento
     */
    public Short getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * @param idTipoDocumento the idTipoDocumento to set
     */
    public void setIdTipoDocumento(Short idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }
}
