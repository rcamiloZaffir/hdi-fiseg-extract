package mx.interware.hdi.fiseg.extract.exception;
/**
 * 
 * Excepci&oacute;n que encapsula el manejo de errores del microservicio
 * 
 * @author Interware
 * @since 2020-10-16
 */
public class ExtractException extends RuntimeException {
	private static final long serialVersionUID = -8034440099558628040L;
	/**
	 * Crea una excepci&oacute;n a partir de un mensaje
	 * 
	 * @param message Mensaje del error
	 */
	public ExtractException(String message) {
		super(message);
	}
	/**
	 * Crea una excepci&oacute;n a partir de un mensaje y un objecto {@link Exception}
	 * 
	 * @param message Mensaje de la excepción
	 * @param ex      Objeto {@linkplain Exception}
	 */
	public ExtractException(String message, Exception ex) {
		super(message, ex);
	}
	/**
	 * Crea una excepci&oacute;n a partir de un objecto {@link Exception}
	 * 
	 * @param ex Objeto {@linkplain Exception}
	 */
	public ExtractException(Exception ex) {
		super(ex);
	}

}
