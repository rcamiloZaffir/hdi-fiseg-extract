package mx.interware.hdi.fiseg.extract.config;

import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_BOOTSTRAP_ADDRESS;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTR_TOPIC;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTT_TOPIC;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import mx.interware.hdi.fiseg.extract.util.AppProperties;
/**
 * 
 * Clase que carga la configuraci&oacute;n incial para la conexi&oacute;n a Kafka
 * 
 * @author Interware
 * @since 2020-10-18
 */
@Configuration
public class KafkaConfigurer {

	@Autowired
	AppProperties appProperties;

	/**
	 * REtorna el bean principal para la conexi&oacute;n a Kafka
	 * @return Objeto {@linkplain KafkaAdmin} con la conexi&oacute;n 
	 */
	@Bean
	public KafkaAdmin kafkaAdmin() {
		Map<String, Object> configs = new HashMap<>();
		String bootstrapAddress = appProperties.getProperty(KAFKA_BOOTSTRAP_ADDRESS);
		configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		return new KafkaAdmin(configs);
	}

	/**
	 * Bean con la configuraci&oacute;n para topicos autos Residentes
	 * @return Objeto {@link NewTopic} Autos Residentes
	 */
	@Bean
	public NewTopic autrTopic() {
		String topicName = appProperties.getProperty(KAFKA_EXTRACT_AUTR_TOPIC);
		return new NewTopic(topicName, 1, (short) 1);
	}

	/**
	 * Bean con la configuraci&oacute;n para topicos autos Turistas
	 * @return Objeto {@link NewTopic} Autos Turistas
	 */
	@Bean
	public NewTopic auttTopic() {
		String topicName = appProperties.getProperty(KAFKA_EXTRACT_AUTT_TOPIC);
		return new NewTopic(topicName, 1, (short) 1);
	}
	/**
	 * Devuelve la cofiguraci&oacute;n con los valores necesarios para envio de mensajes al producer
	 * @return Mapa con la configuracion
	 */
	@Bean
	public ProducerFactory<String, String> producerFactory() {
		Map<String, Object> configProps = new HashMap<>();
		String bootstrapAddress = appProperties.getProperty(KAFKA_BOOTSTRAP_ADDRESS);
		configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
		configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		return new DefaultKafkaProducerFactory<>(configProps);
	}

	/**
	 * Retorna la configuraci&oacute;n del template para conexi&oacute;n a Kafka
	 * @return Objeto {@link KafkaTemplate}
	 */
	@Bean
	public KafkaTemplate<String, String> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}
}