package mx.interware.hdi.fiseg.extract.util;

/**
 * Enumeraci&oacute;n con los posibles tipo de eventos para la extracci&oacute;n
 * 
 * @author Interware
 * @since 2020-10-18
 *
 */
public enum TipoEvento {
	EXTRACTION_STARTED((short) 1, "Extracción iniciada"), EXTRACTION_FINISHED((short) 2, "Extracción finalizada");

	private Short id;
	private String description;

	/**
	 * Constructor que incializa el Enum
	 * 
	 * @param id          Identificador
	 * @param description Descripcio&oacute;n
	 */
	TipoEvento(Short id, String description) {
		this.id = id;
		this.description = description;
	}

	/**
	 * Retorna el Identificador
	 * 
	 * @return Identificador
	 */
	public Short getId() {
		return id;
	}

	/**
	 * Retorna la descripci&oacute;n
	 * 
	 * @return Descripci&oacute;n
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Busca el objeto por el id enviado
	 * 
	 * @param id Identificador para la b&uacute;squeda
	 * @return Objeto encontrado
	 */
	public static TipoEvento findById(Short id) {
		for (TipoEvento tipoEvento : values()) {
			if (tipoEvento.getId().equals(id)) {
				return tipoEvento;
			}
		}
		return null;
	}
}