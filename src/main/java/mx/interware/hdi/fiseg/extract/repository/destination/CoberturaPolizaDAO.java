package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.interware.hdi.fiseg.extract.entity.destination.CoberturaPolizaEntity;
/**
 * Repository con las operaciones necesarias para el manejo de Coberturas de Poliza
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface CoberturaPolizaDAO extends JpaRepository<CoberturaPolizaEntity, Integer> {

}
