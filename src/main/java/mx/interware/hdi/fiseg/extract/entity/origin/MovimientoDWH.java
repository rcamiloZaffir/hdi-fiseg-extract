package mx.interware.hdi.fiseg.extract.entity.origin;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Entidad que contiene las propiedades de la vista con informacion de movimiento de Vehiculos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@MappedSuperclass
public class MovimientoDWH implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "IdOficina")
    private Short idOficina;

    @Id
    @Column(name = "NumPoliza")
    private Long numPoliza;

    @Id
    @Column(name = "NumDocumento")
    private Short numDocumento;

    @Id
    @Column(name = "IdTipoDocumento")
    private Short idTipoDocumento;

    @Id
    @Column(name = "NumCertificado")
    private String numCertificado;

    @Id
    @Column(name = "NumCotizacion")
    private Integer numCotizacion;

    @Column(name = "NipPerfilAgente")
    private Integer nipPerfilAgente;

    @Column(name = "FechaTransaccion")
    private Integer fechaTransaccion;

    @Column(name = "NumReferencia")
    private String numReferencia;

    @Column(name = "FechaInicioVigencia")
    private Date fechaInicioVigencia;

    @Column(name = "FechaFinVigencia")
    private Date fechaFinVigencia;

    @Column(name = "NumeroSerie")
    private String numeroSerie;

    @Column(name = "IdMarcaVehiculo")
    private Integer idMarcaVehiculo;

    @Column(name = "ModeloVehiculo")
    private Short modeloVehiculo;

    @Column(name = "TipoPersona")
    private String tipoPersona;

    @Column(name = "Apellido1")
    private String apellido1;

    @Column(name = "Apellido2")
    private String apellido2;

    @Column(name = "Nombre")
    private String nombre;

    @Column(name = "RazonSocial")
    private String razonSocial;

    @Column(name = "Y_O")
    private String yGuionO;

    @Column(name = "IdCanalVenta")
    private Short idCanalVenta;

    @Column(name = "DescCanalVenta")
    private String descCanalVenta;

    @Column(name = "NombrePerfilAgente")
    private String nombrePerfilAgente;

    @Column(name = "DescTipoDocumento")
    private String descTipoDocumento;

    @Column(name = "IdCausaCancelacion")
    private Integer idCausaCancelacion;

    @Column(name = "DescCausaCancelacion")
    private String descCausaCancelacion;

    @Column(name = "FechaEmision")
    private Timestamp fechaEmision;

    @Column(name = "TiempoUltimoCambio")
    private Timestamp tiempoUltimoCambio;

    @Column(name = "CodigoPostal")
    private String codigoPostal;

    @Column(name = "RFC")
    private String rfc;

    @Column(name = "NombrePais")
    private String nombrePais;

    @Column(name = "NombreEstado")
    private String nombreEstado;

    @Column(name = "NombreMunicipio")
    private String nombreMunicipio;

    @Column(name = "NombreCiudad")
    private String nombreCiudad;

    @Column(name = "IdPaquete")
    private Short idPaquete;

    @Column(name = "DescPaquete")
    private String descPaquete;

    @Column(name = "DescGrupoPaquete")
    private String descGrupoPaquete;

    @Column(name = "IdEstadoCliente")
    private Short idEstadoCliente;

    @Column(name = "IdTipoPoliza")
    private Short idTipoPoliza;

    @Column(name = "IdMarcaTipoOcra")
    private Integer idMarcaTipoOcra;

    @Column(name = "IdUsoCNSF")
    private Integer idUsoCNSF;

    @Column(name = "DescUsoCNSF")
    private String descUsoCNSF;

    @Column(name = "CuentaConSeguroObligatorio")
    private Integer cuentaConSeguroObligatorio;

    @Column(name = "IdClienteEntidad")
    private Integer idClienteEntidad;

    @Column(name = "IdClienteMunicipio")
    private Integer idClienteMunicipio;

    @Column(name = "IdTipoSeguro")
    private Integer idTipoSeguro;

    @Column(name = "IdTipoSumaAsegurada")
    private Integer idTipoSumaAsegurada;

    @Column(name = "IdGrupoDocumento")
    private Integer idGrupoDocumento;

    @Column(name = "DescGrupoDocumento")
    private String descGrupoDocumento;

    @Column(name = "AnioPoliza")
    private Integer anioPoliza;

    /**
     * @return the fechaTransaccion
     */
    public Integer getFechaTransaccion() {
        return fechaTransaccion;
    }

    /**
     * @param fechaTransaccion the fechaTransaccion to set
     */
    public void setFechaTransaccion(Integer fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    /**
     * @return the idOficina
     */
    public Short getIdOficina() {
        return idOficina;
    }

    /**
     * @param idOficina the idOficina to set
     */
    public void setIdOficina(Short idOficina) {
        this.idOficina = idOficina;
    }

    /**
     * @return the numPoliza
     */
    public Long getNumPoliza() {
        return numPoliza;
    }

    /**
     * @param numPoliza the numPoliza to set
     */
    public void setNumPoliza(Long numPoliza) {
        this.numPoliza = numPoliza;
    }

    /**
     * @return the numReferencia
     */
    public String getNumReferencia() {
        return numReferencia;
    }

    /**
     * @param numReferencia the numReferencia to set
     */
    public void setNumReferencia(String numReferencia) {
        this.numReferencia = numReferencia;
    }

    /**
     * @return the numCertificado
     */
    public String getNumCertificado() {
        return numCertificado;
    }

    /**
     * @param numCertificado the numCertificado to set
     */
    public void setNumCertificado(String numCertificado) {
        this.numCertificado = numCertificado;
    }

    /**
     * @return the fechaInicioVigencia
     */
    public Date getFechaInicioVigencia() {
        return fechaInicioVigencia;
    }

    /**
     * @param fechaInicioVigencia the fechaInicioVigencia to set
     */
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }

    /**
     * @return the fechaFinVigencia
     */
    public Date getFechaFinVigencia() {
        return fechaFinVigencia;
    }

    /**
     * @param fechaFinVigencia the fechaFinVigencia to set
     */
    public void setFechaFinVigencia(Date fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }

    /**
     * @return the numeroSerie
     */
    public String getNumeroSerie() {
        return numeroSerie;
    }

    /**
     * @param numeroSerie the numeroSerie to set
     */
    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    /**
     * @return the idMarcaVehiculo
     */
    public Integer getIdMarcaVehiculo() {
        return idMarcaVehiculo;
    }

    /**
     * @param idMarcaVehiculo the idMarcaVehiculo to set
     */
    public void setIdMarcaVehiculo(Integer idMarcaVehiculo) {
        this.idMarcaVehiculo = idMarcaVehiculo;
    }

    /**
     * @return the modeloVehiculo
     */
    public Short getModeloVehiculo() {
        return modeloVehiculo;
    }

    /**
     * @param modeloVehiculo the modeloVehiculo to set
     */
    public void setModeloVehiculo(Short modeloVehiculo) {
        this.modeloVehiculo = modeloVehiculo;
    }

    /**
     * @return the tipoPersona
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * @param tipoPersona the tipoPersona to set
     */
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    /**
     * @return the apellido1
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     * @param apellido1 the apellido1 to set
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    /**
     * @return the apellido2
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * @param apellido2 the apellido2 to set
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     * @return the yGuionO
     */
    public String getYGuionO() {
        return yGuionO;
    }

    /**
     * @param yGuionO the yGuionO to set
     */
    public void setYGuionO(String yGuionO) {
        this.yGuionO = yGuionO;
    }

    /**
     * @return the idCanalVenta
     */
    public Short getIdCanalVenta() {
        return idCanalVenta;
    }

    /**
     * @param idCanalVenta the idCanalVenta to set
     */
    public void setIdCanalVenta(Short idCanalVenta) {
        this.idCanalVenta = idCanalVenta;
    }

    /**
     * @return the descCanalVenta
     */
    public String getDescCanalVenta() {
        return descCanalVenta;
    }

    /**
     * @param descCanalVenta the descCanalVenta to set
     */
    public void setDescCanalVenta(String descCanalVenta) {
        this.descCanalVenta = descCanalVenta;
    }

    /**
     * @return the nombrePerfilAgente
     */
    public String getNombrePerfilAgente() {
        return nombrePerfilAgente;
    }

    /**
     * @param nombrePerfilAgente the nombrePerfilAgente to set
     */
    public void setNombrePerfilAgente(String nombrePerfilAgente) {
        this.nombrePerfilAgente = nombrePerfilAgente;
    }

    /**
     * @return the numDocumento
     */
    public Short getNumDocumento() {
        return numDocumento;
    }

    /**
     * @param numDocumento the numDocumento to set
     */
    public void setNumDocumento(Short numDocumento) {
        this.numDocumento = numDocumento;
    }

    /**
     * @return the descTipoDocumento
     */
    public String getDescTipoDocumento() {
        return descTipoDocumento;
    }

    /**
     * @param descTipoDocumento the descTipoDocumento to set
     */
    public void setDescTipoDocumento(String descTipoDocumento) {
        this.descTipoDocumento = descTipoDocumento;
    }

    /**
     * @return the idCausaCancelacion
     */
    public Integer getIdCausaCancelacion() {
        return idCausaCancelacion;
    }

    /**
     * @param idCausaCancelacion the idCausaCancelacion to set
     */
    public void setIdCausaCancelacion(Integer idCausaCancelacion) {
        this.idCausaCancelacion = idCausaCancelacion;
    }

    /**
     * @return the descCausaCancelacion
     */
    public String getDescCausaCancelacion() {
        return descCausaCancelacion;
    }

    /**
     * @param descCausaCancelacion the descCausaCancelacion to set
     */
    public void setDescCausaCancelacion(String descCausaCancelacion) {
        this.descCausaCancelacion = descCausaCancelacion;
    }

    /**
     * @return the fechaEmision
     */
    public Timestamp getFechaEmision() {
        return fechaEmision;
    }

    /**
     * @param fechaEmision the fechaEmision to set
     */
    public void setFechaEmision(Timestamp fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return the rfc
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * @param rfc the rfc to set
     */
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    /**
     * @return the nombrePais
     */
    public String getNombrePais() {
        return nombrePais;
    }

    /**
     * @param nombrePais the nombrePais to set
     */
    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    /**
     * @return the nombreEstado
     */
    public String getNombreEstado() {
        return nombreEstado;
    }

    /**
     * @param nombreEstado the nombreEstado to set
     */
    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    /**
     * @return the nombreMunicipio
     */
    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    /**
     * @param nombreMunicipio the nombreMunicipio to set
     */
    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    /**
     * @return the nombreCiudad
     */
    public String getNombreCiudad() {
        return nombreCiudad;
    }

    /**
     * @param nombreCiudad the nombreCiudad to set
     */
    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    /**
     * @return the idPaquete
     */
    public Short getIdPaquete() {
        return idPaquete;
    }

    /**
     * @param idPaquete the idPaquete to set
     */
    public void setIdPaquete(Short idPaquete) {
        this.idPaquete = idPaquete;
    }

    /**
     * @return the descPaquete
     */
    public String getDescPaquete() {
        return descPaquete;
    }

    /**
     * @param descPaquete the descPaquete to set
     */
    public void setDescPaquete(String descPaquete) {
        this.descPaquete = descPaquete;
    }

    /**
     * @return the descGrupoPaquete
     */
    public String getDescGrupoPaquete() {
        return descGrupoPaquete;
    }

    /**
     * @param descGrupoPaquete the descGrupoPaquete to set
     */
    public void setDescGrupoPaquete(String descGrupoPaquete) {
        this.descGrupoPaquete = descGrupoPaquete;
    }

    /**
     * @return the idEstadoCliente
     */
    public Short getIdEstadoCliente() {
        return idEstadoCliente;
    }

    /**
     * @param idEstadoCliente the idEstadoCliente to set
     */
    public void setIdEstadoCliente(Short idEstadoCliente) {
        this.idEstadoCliente = idEstadoCliente;
    }

    /**
     * @return the numCotizacion
     */
    public Integer getNumCotizacion() {
        return numCotizacion;
    }

    /**
     * @param numCotizacion the numCotizacion to set
     */
    public void setNumCotizacion(Integer numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    /**
     * @return the idTipoPoliza
     */
    public Short getIdTipoPoliza() {
        return idTipoPoliza;
    }

    /**
     * @param idTipoPoliza the idTipoPoliza to set
     */
    public void setIdTipoPoliza(Short idTipoPoliza) {
        this.idTipoPoliza = idTipoPoliza;
    }

    /**
     * @return the idTipoDocumento
     */
    public Short getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * @param idTipoDocumento the idTipoDocumento to set
     */
    public void setIdTipoDocumento(Short idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    /**
     * @return the nipPerfilAgente
     */
    public Integer getNipPerfilAgente() {
        return nipPerfilAgente;
    }

    /**
     * @param nipPerfilAgente the nipPerfilAgente to set
     */
    public void setNipPerfilAgente(Integer nipPerfilAgente) {
        this.nipPerfilAgente = nipPerfilAgente;
    }

    /**
     * @return the idMarcaTipoOcra
     */
    public Integer getIdMarcaTipoOcra() {
        return idMarcaTipoOcra;
    }

    /**
     * @param idMarcaTipoOcra the idMarcaTipoOcra to set
     */
    public void setIdMarcaTipoOcra(Integer idMarcaTipoOcra) {
        this.idMarcaTipoOcra = idMarcaTipoOcra;
    }

    /**
     * @return the idUsoCNSF
     */
    public Integer getIdUsoCNSF() {
        return idUsoCNSF;
    }

    /**
     * @param idUsoCNSF the idUsoCNSF to set
     */
    public void setIdUsoCNSF(Integer idUsoCNSF) {
        this.idUsoCNSF = idUsoCNSF;
    }

    /**
     * @return the descUsoCNSF
     */
    public String getDescUsoCNSF() {
        return descUsoCNSF;
    }

    /**
     * @param descUsoCNSF the descUsoCNSF to set
     */
    public void setDescUsoCNSF(String descUsoCNSF) {
        this.descUsoCNSF = descUsoCNSF;
    }

    /**
     * @return the cuentaConSeguroObligatorio
     */
    public Integer getCuentaConSeguroObligatorio() {
        return cuentaConSeguroObligatorio;
    }

    /**
     * @param cuentaConSeguroObligatorio the cuentaConSeguroObligatorio to set
     */
    public void setCuentaConSeguroObligatorio(Integer cuentaConSeguroObligatorio) {
        this.cuentaConSeguroObligatorio = cuentaConSeguroObligatorio;
    }

    /**
     * @return the idClienteEntidad
     */
    public Integer getIdClienteEntidad() {
        return idClienteEntidad;
    }

    /**
     * @param idClienteEntidad the idClienteEntidad to set
     */
    public void setIdClienteEntidad(Integer idClienteEntidad) {
        this.idClienteEntidad = idClienteEntidad;
    }

    /**
     * @return the idClienteMunicipio
     */
    public Integer getIdClienteMunicipio() {
        return idClienteMunicipio;
    }

    /**
     * @param idClienteMunicipio the idClienteMunicipio to set
     */
    public void setIdClienteMunicipio(Integer idClienteMunicipio) {
        this.idClienteMunicipio = idClienteMunicipio;
    }

    /**
     * @return the idTipoSeguro
     */
    public Integer getIdTipoSeguro() {
        return idTipoSeguro;
    }

    /**
     * @param idTipoSeguro the idTipoSeguro to set
     */
    public void setIdTipoSeguro(Integer idTipoSeguro) {
        this.idTipoSeguro = idTipoSeguro;
    }

    /**
     * @return the idTipoSumaAsegurada
     */
    public Integer getIdTipoSumaAsegurada() {
        return idTipoSumaAsegurada;
    }

    /**
     * @param idTipoSumaAsegurada the idTipoSumaAsegurada to set
     */
    public void setIdTipoSumaAsegurada(Integer idTipoSumaAsegurada) {
        this.idTipoSumaAsegurada = idTipoSumaAsegurada;
    }

    /**
     * @return the idGrupoDocumento
     */
    public Integer getIdGrupoDocumento() {
        return idGrupoDocumento;
    }

    /**
     * @param idGrupoDocumento the idGrupoDocumento to set
     */
    public void setIdGrupoDocumento(Integer idGrupoDocumento) {
        this.idGrupoDocumento = idGrupoDocumento;
    }

    /**
     * @return the descGrupoDocumento
     */
    public String getDescGrupoDocumento() {
        return descGrupoDocumento;
    }

    /**
     * @param descGrupoDocumento the descGrupoDocumento to set
     */
    public void setDescGrupoDocumento(String descGrupoDocumento) {
        this.descGrupoDocumento = descGrupoDocumento;
    }

    /**
     * @return the yGuionO
     */
    public String getyGuionO() {
        return yGuionO;
    }

    /**
     * @param yGuionO the yGuionO to set
     */
    public void setyGuionO(String yGuionO) {
        this.yGuionO = yGuionO;
    }

    /**
     * @return the tiempoUltimoCambio
     */
    public Timestamp getTiempoUltimoCambio() {
        return tiempoUltimoCambio;
    }

    /**
     * @param tiempoUltimoCambio the tiempoUltimoCambio to set
     */
    public void setTiempoUltimoCambio(Timestamp tiempoUltimoCambio) {
        this.tiempoUltimoCambio = tiempoUltimoCambio;
    }

    /**
     * @return the anioPoliza
     */
    public Integer getAnioPoliza() {
        return anioPoliza;
    }

    /**
     * @param anioPoliza the anioPoliza to set
     */
    public void setAnioPoliza(Integer anioPoliza) {
        this.anioPoliza = anioPoliza;
    }
}
