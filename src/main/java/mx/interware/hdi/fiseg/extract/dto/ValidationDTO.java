package mx.interware.hdi.fiseg.extract.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO con las propiedades de resultado de validaci&oacute;n
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class ValidationDTO extends AbstractDTO implements Serializable {

	private static final long serialVersionUID = -2755695496241454345L;

	@JsonProperty("success")
	private boolean success;

	@JsonProperty("message")
	private String message;

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}