package mx.interware.hdi.fiseg.extract.config;

import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_DDL_AUTO;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_DIALECT;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_DRIVER;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_ENTITY_PACKAGE;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_SCHEMA;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_URL;

import java.util.HashMap;

import javax.sql.DataSource;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import mx.interware.hdi.fiseg.extract.util.AppProperties;

/**
 * 
 * Clase que carga la configuraci&oacute;n incial para la base de datos destino
 * 
 * @author Interware
 * @since 2020-10-17
 */
@Configuration
@EnableJpaRepositories(basePackages = "mx.interware.hdi.fiseg.extract.repository.destination", entityManagerFactoryRef = "destinationEntityManager", transactionManagerRef = "destinationTransactionManager")
public class DestinationDBConfigurer {
    private static final Logger LOGGER = LogManager.getLogger(DestinationDBConfigurer.class);

    @Autowired
    private AppProperties appProperties;

    /**
     * Configuraci&oacute;n del entity Manager de la base de datos destino a partir de la variables de entorno o propiedades default
     * 
     * @return {@link LocalContainerEntityManagerFactoryBean} configuracion
     */
    @Bean
    @DependsOn("destinationSqlInterceptor")
    public LocalContainerEntityManagerFactoryBean destinationEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(destinationDataSource());
        em.setPackagesToScan(appProperties.getProperty(DS_DESTINATION_ENTITY_PACKAGE));
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", appProperties.getProperty(DS_DESTINATION_DDL_AUTO));
        properties.put("hibernate.dialect", appProperties.getProperty(DS_DESTINATION_DIALECT));
        properties.put("hibernate.enable_lazy_load_no_trans", true);
        properties.put("hibernate.ejb.interceptor", destinationSqlInterceptor());
        em.setJpaPropertyMap(properties);

        return em;
    }

    /**
     * Obtiene la configuraci&oacute;n de la base de datos destino y crea el datasource
     * 
     * @return Datasource
     */
    @Bean
    public DataSource destinationDataSource() {
        String url = appProperties.getProperty(DS_DESTINATION_URL);
        boolean isBase64 = Base64.isBase64(url.getBytes());

        DataSource dataSource = null;
        if (isBase64) {
            LOGGER.info("Creating datasource destino from encrypted url : {} ...", url);
            hdi.framework.spring.extensions.jdbc.datasource.DriverManagerDataSource securedDataSource = new hdi.framework.spring.extensions.jdbc.datasource.DriverManagerDataSource();
            securedDataSource.setDriverClassName(appProperties.getProperty(DS_DESTINATION_DRIVER));
            securedDataSource.setUrl(url);
            dataSource = securedDataSource;
        } else {
            LOGGER.info("Creating datasource destino from url : {} ...", url);
            DriverManagerDataSource plainDataSource = new DriverManagerDataSource();
            plainDataSource.setUrl(url);
            plainDataSource.setDriverClassName(appProperties.getProperty(DS_DESTINATION_DRIVER));
            dataSource = plainDataSource;
        }
        return dataSource;
    }

    /**
     * Crea el Bean para la administracion de transacciones para la base de datos destino
     * 
     * @return Transaction Manager configurado
     */
    @Bean
    public PlatformTransactionManager destinationTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(destinationEntityManager().getObject());
        return transactionManager;
    }

    /**
     * Retorna el Bean para la intercepci&oacute;n y reemplazo de los schemas en los querys, para que tome el valor de las variables de entorno.
     * 
     * @return String con el nuevo schema al que se debe acceder
     */
    @Bean
    public EmptyInterceptor destinationSqlInterceptor() {

        return new EmptyInterceptor() {
            @Override
            public String onPrepareStatement(String sql) {
                LOGGER.trace(">> sql     :{}", sql);
                String fixedSql = super.onPrepareStatement(sql);
                fixedSql = fixedSql.replaceAll("\\{datasources.destination.schema\\}", appProperties.getProperty(DS_DESTINATION_SCHEMA));
                LOGGER.trace(">> fixedSql:{}", fixedSql);
                return fixedSql;
            }
        };
    }

}
