package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonProperty;

import mx.interware.hdi.fiseg.extract.util.StringUtils;

/**
 * Clase abstracta con la definici&oacute;n del toString para todos los entities que la heredaran
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
@MappedSuperclass
public class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "Tab_Estatus")
    @JsonProperty("tabEstatus")
    private Integer tabEstatus;

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return StringUtils.toJson(this);

    }

    /**
     * @return the estatus
     */
    public int getTabEstatus() {
        return this.tabEstatus;
    }

    /**
     * @param tabEstatus the estatus to set
     */
    public void setTabEstatus(int tabEstatus) {
        this.tabEstatus = tabEstatus;
    }

}
