package mx.interware.hdi.fiseg.extract.config;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * 
 * Clase que permite la configuraci&oacute;n y redirecci&oacute;n de los
 * elementos de Swagger
 * 
 * @author Interware
 * @since 2020-10-17
 */
public class SwaggerUiWebMvcConfigurer implements WebMvcConfigurer {
	private final String baseUrl;
	/**
	 * Constructor a partir de la URL base
	 * 
	 * @param baseUrl URL base de la aplicaci&oacute;n
	 */
	public SwaggerUiWebMvcConfigurer(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		String baseUrlTmp = StringUtils.trimTrailingCharacter(this.baseUrl, '/');
		registry.addResourceHandler(baseUrlTmp + "/swagger-ui/**")
				.addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/")
				.resourceChain(false);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController(baseUrl + "/swagger-ui/").setViewName("forward:" + baseUrl + "/swagger-ui/index.html");
	}
}
