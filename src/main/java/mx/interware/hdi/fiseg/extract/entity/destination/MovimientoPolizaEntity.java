package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

import mx.interware.hdi.fiseg.extract.util.AddItem;
import mx.interware.hdi.fiseg.extract.util.RemoveItem;

/**
 * Entidad que contiene las propiedades de un movimiento de poliza de la base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutMovimientoPoliza", schema = "{datasources.destination.schema}")
public class MovimientoPolizaEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -5952884672631633949L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MovPol_Id")
    @JsonProperty("movimientoPolizaId")
    private Long movimientoPolizaId;

    @Column(name = "MovPol_Str")
    @JsonProperty("movimientoPolizaStr")
    private String movimientoPolizaStr;

    @Column(name = "FechaMovimiento")
    @JsonProperty("fechaMovimiento")
    private Date fechaMovimiento;

    @Column(name = "NumeroDocumento")
    @JsonProperty("numeroDocumento")
    private Integer numeroDocumento;

    @Column(name = "TipoMovimiento")
    @JsonProperty("tipoMovimiento")
    private Short tipoMovimiento;

    @Column(name = "DescGrupoMovimiento")
    @JsonProperty("descGrupoMovimiento")
    private String descGrupoMovimiento;

    @Column(name = "CausaCancelacion")
    @JsonProperty("causaCancelacion")
    private String causaCancelacion;

    @Column(name = "IdCausaCancelacion")
    @JsonProperty("IdCausaCancelacion")
    private Integer idCausaCancelacion;

    @Column(name = "TipoPoliza")
    @JsonProperty("tipoPoliza")
    private Short tipoPoliza;

    @Column(name = "NumeroVehiculo")
    @JsonProperty("numeroVehiculo")
    private Integer numeroVehiculo;

    @Column(name = "NumCertificado")
    @JsonProperty("numCertificado")
    private String numCertificado;

    @Column(name = "EstatusProcesamiento")
    @JsonProperty("estatusProcesamiento")
    private Short estatusProcesamiento;

    @Column(name = "MensajeProcesamiento")
    @JsonProperty("mensajeProcesamiento")
    private String mensajeProcesamiento;

    @Column(name = "LineaNegocio")
    @JsonProperty("lineaNegocio")
    private String lineaNegocio;

    @Column(name = "Clasificacion")
    @JsonProperty("clasificacion")
    private Short clasificacion;

    @Column(name = "FechaCaptura")
    @JsonProperty("fechacaptura")
    private Date fechacaptura;

    @Column(name = "FechaEmision")
    @JsonProperty("fechaEmision")
    private Date fechaEmision;

    @Column(name = "FechaCancelacion")
    @JsonProperty("fechaCancelacion")
    private Date fechaCancelacion;

    @Column(name = "FechaInicioVigencia")
    @JsonProperty("fechaInicioVigencia")
    private Date fechaInicioVigencia;

    @Column(name = "FechaFinalVigencia")
    @JsonProperty("fechaFinalVigencia")
    private Date fechaFinalVigencia;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Pol_Id")
    private PolizaEntity poliza;

    @Column(name = "MovPol_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaMovPol;

    @Column(name = "MovPol_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaMovPol;

    @Column(name = "MovPol_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioMovPol;

    @Column(name = "MovPol_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioMovPol;

    @Transient
    @JsonProperty("vehiculos")
    private List<VehiculoEntity> vehiculos = new ArrayList<>();

    @Transient
    @JsonProperty("clientes")
    private List<ClientePolizaEntity> clientes = new ArrayList<>();

    @Transient
    @JsonProperty("coberturas")
    private List<CoberturaPolizaEntity> coberturas = new ArrayList<>();

    /**
     * @return the movimientoPolizaId
     */
    public Long getMovimientoPolizaId() {
        return movimientoPolizaId;
    }

    /**
     * @param movimientoPolizaId the movimientoPolizaId to set
     */
    public void setMovimientoPolizaId(Long movimientoPolizaId) {
        this.movimientoPolizaId = movimientoPolizaId;
    }

    /**
     * @return the movimientoPolizaStr
     */
    public String getMovimientoPolizaStr() {
        return movimientoPolizaStr;
    }

    /**
     * @param movimientoPolizaStr the movimientoPolizaStr to set
     */
    public void setMovimientoPolizaStr(String movimientoPolizaStr) {
        this.movimientoPolizaStr = movimientoPolizaStr;
    }

    /**
     * @return the fechaMovimiento
     */
    public Date getFechaMovimiento() {
        return fechaMovimiento;
    }

    /**
     * @param fechaMovimiento the fechaMovimiento to set
     */
    public void setFechaMovimiento(Date fechaMovimiento) {
        this.fechaMovimiento = fechaMovimiento;
    }

    /**
     * @return the numeroDocumento
     */
    public Integer getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * @param numeroDocumento the numeroDocumento to set
     */
    public void setNumeroDocumento(Integer numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    /**
     * @return the tipoMovimiento
     */
    public Short getTipoMovimiento() {
        return tipoMovimiento;
    }

    /**
     * @param tipoMovimiento the tipoMovimiento to set
     */
    public void setTipoMovimiento(Short tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    /**
     * @return the tipoPoliza
     */
    public Short getTipoPoliza() {
        return tipoPoliza;
    }

    /**
     * @param tipoPoliza the tipoPoliza to set
     */
    public void setTipoPoliza(Short tipoPoliza) {
        this.tipoPoliza = tipoPoliza;
    }

    /**
     * @return the numeroVehiculo
     */
    public Integer getNumeroVehiculo() {
        return numeroVehiculo;
    }

    /**
     * @param numeroVehiculo the numeroVehiculo to set
     */
    public void setNumeroVehiculo(Integer numeroVehiculo) {
        this.numeroVehiculo = numeroVehiculo;
    }

    /**
     * @return the estatusProcesamiento
     */
    public Short getEstatusProcesamiento() {
        return estatusProcesamiento;
    }

    /**
     * @param estatusProcesamiento the estatusProcesamiento to set
     */
    public void setEstatusProcesamiento(Short estatusProcesamiento) {
        this.estatusProcesamiento = estatusProcesamiento;
    }

    /**
     * @return the mensajeProcesamiento
     */
    public String getMensajeProcesamiento() {
        return mensajeProcesamiento;
    }

    /**
     * @param mensajeProcesamiento the mensajeProcesamiento to set
     */
    public void setMensajeProcesamiento(String mensajeProcesamiento) {
        this.mensajeProcesamiento = mensajeProcesamiento;
    }

    /**
     * @return the fechacaptura
     */
    public Date getFechacaptura() {
        return fechacaptura;
    }

    /**
     * @param fechacaptura the fechacaptura to set
     */
    public void setFechacaptura(Date fechacaptura) {
        this.fechacaptura = fechacaptura;
    }

    /**
     * @return the fechaEmision
     */
    public Date getFechaEmision() {
        return fechaEmision;
    }

    /**
     * @param fechaEmision the fechaEmision to set
     */
    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    /**
     * @return the fechaCancelacion
     */
    public Date getFechaCancelacion() {
        return fechaCancelacion;
    }

    /**
     * @param fechaCancelacion the fechaCancelacion to set
     */
    public void setFechaCancelacion(Date fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    /**
     * @return the fechaInicioVigencia
     */
    public Date getFechaInicioVigencia() {
        return fechaInicioVigencia;
    }

    /**
     * @param fechaInicioVigencia the fechaInicioVigencia to set
     */
    public void setFechaInicioVigencia(Date fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }

    /**
     * @return the fechaFinalVigencia
     */
    public Date getFechaFinalVigencia() {
        return fechaFinalVigencia;
    }

    /**
     * @param fechaFinalVigencia the fechaFinalVigencia to set
     */
    public void setFechaFinalVigencia(Date fechaFinalVigencia) {
        this.fechaFinalVigencia = fechaFinalVigencia;
    }

    /**
     * @return the poliza
     */
    public PolizaEntity getPoliza() {
        return poliza;
    }

    /**
     * @param poliza the poliza to set
     */
    public void setPoliza(PolizaEntity poliza) {
        this.poliza = poliza;
    }

    /**
     * @return the vehiculos
     */
    public List<VehiculoEntity> getVehiculos() {
        return vehiculos;
    }

    /**
     * @param vehiculos the vehiculos to set
     */
    public void setVehiculos(List<VehiculoEntity> vehiculos) {
        this.vehiculos = vehiculos;
    }

    /**
     * @return the clientes
     */
    public List<ClientePolizaEntity> getClientes() {
        return clientes;
    }

    /**
     * @param clientes the clientes to set
     */
    public void setClientes(List<ClientePolizaEntity> clientes) {
        this.clientes = clientes;
    }

    /**
     * @return the coberturas
     */
    public List<CoberturaPolizaEntity> getCoberturas() {
        return coberturas;
    }

    /**
     * @param coberturas the coberturas to set
     */
    public void setCoberturas(List<CoberturaPolizaEntity> coberturas) {
        this.coberturas = coberturas;
    }

    @AddItem(entity = VehiculoEntity.class, collection = "vehiculos")
    public void addVehiculo(VehiculoEntity vehiculo) {
        vehiculos.add(vehiculo);
        vehiculo.setMovimientoPoliza(this);
    }

    @RemoveItem(entity = VehiculoEntity.class, collection = "vehiculos")
    public void removeVehiculo(VehiculoEntity vehiculo) {
        vehiculos.remove(vehiculo);
        vehiculo.setMovimientoPoliza(null);
    }

    @AddItem(entity = ClientePolizaEntity.class, collection = "clientes")
    public void addClientePoliza(ClientePolizaEntity clientePoliza) {
        clientes.add(clientePoliza);
        clientePoliza.setMovimientoPoliza(this);
    }

    @RemoveItem(entity = ClientePolizaEntity.class, collection = "clientes")
    public void removeClientePoliza(ClientePolizaEntity clientePoliza) {
        clientes.remove(clientePoliza);
        clientePoliza.setMovimientoPoliza(null);
    }

    @AddItem(entity = CoberturaPolizaEntity.class, collection = "coberturas")
    public void addCoberturaPoliza(CoberturaPolizaEntity coberturaPoliza) {
        coberturas.add(coberturaPoliza);
        coberturaPoliza.setMovimientoPoliza(this);
    }

    @RemoveItem(entity = CoberturaPolizaEntity.class, collection = "coberturas")
    public void removeCoberturaPoliza(CoberturaPolizaEntity coberturaPoliza) {
        coberturas.remove(coberturaPoliza);
        coberturaPoliza.setMovimientoPoliza(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MovimientoPolizaEntity)) {
            return false;
        }

        Long id = ((MovimientoPolizaEntity) o).getMovimientoPolizaId();
        return (movimientoPolizaId != null) && (movimientoPolizaId.equals(id));
    }

    @Override
    public int hashCode() {
        return 771;
    }

    /**
     * @return the fechaAltaMovPol
     */
    public Date getFechaAltaMovPol() {
        return fechaAltaMovPol;
    }

    /**
     * @param fechaAltaMovPol the fechaAltaMovPol to set
     */
    public void setFechaAltaMovPol(Date fechaAltaMovPol) {
        this.fechaAltaMovPol = fechaAltaMovPol;
    }

    /**
     * @return the usuarioAltaMovPol
     */
    public String getUsuarioAltaMovPol() {
        return usuarioAltaMovPol;
    }

    /**
     * @param usuarioAltaMovPol the usuarioAltaMovPol to set
     */
    public void setUsuarioAltaMovPol(String usuarioAltaMovPol) {
        this.usuarioAltaMovPol = usuarioAltaMovPol;
    }

    /**
     * @return the fechaCambioMovPol
     */
    public Date getFechaCambioMovPol() {
        return fechaCambioMovPol;
    }

    /**
     * @param fechaCambioMovPol the fechaCambioMovPol to set
     */
    public void setFechaCambioMovPol(Date fechaCambioMovPol) {
        this.fechaCambioMovPol = fechaCambioMovPol;
    }

    /**
     * @return the usuarioCambioMovPol
     */
    public String getUsuarioCambioMovPol() {
        return usuarioCambioMovPol;
    }

    /**
     * @param usuarioCambioMovPol the usuarioCambioMovPol to set
     */
    public void setUsuarioCambioMovPol(String usuarioCambioMovPol) {
        this.usuarioCambioMovPol = usuarioCambioMovPol;
    }

    /**
     * @return the descGrupoMovimiento
     */
    public String getDescGrupoMovimiento() {
        return descGrupoMovimiento;
    }

    /**
     * @param descGrupoMovimiento the descGrupoMovimiento to set
     */
    public void setDescGrupoMovimiento(String descGrupoMovimiento) {
        this.descGrupoMovimiento = descGrupoMovimiento;
    }

    /**
     * @return the causaCancelacion
     */
    public String getCausaCancelacion() {
        return causaCancelacion;
    }

    /**
     * @param causaCancelacion the causaCancelacion to set
     */
    public void setCausaCancelacion(String causaCancelacion) {
        this.causaCancelacion = causaCancelacion;
    }

    /**
     * @return the idCausaCancelacion
     */
    public Integer getIdCausaCancelacion() {
        return idCausaCancelacion;
    }

    /**
     * @param idCausaCancelacion the idCausaCancelacion to set
     */
    public void setIdCausaCancelacion(Integer idCausaCancelacion) {
        this.idCausaCancelacion = idCausaCancelacion;
    }

    /**
     * @return the numCertificado
     */
    public String getNumCertificado() {
        return numCertificado;
    }

    /**
     * @param numCertificado the numCertificado to set
     */
    public void setNumCertificado(String numCertificado) {
        this.numCertificado = numCertificado;
    }

    /**
     * @return the lineaNegocio
     */
    public String getLineaNegocio() {
        return lineaNegocio;
    }

    /**
     * @param lineaNegocio the lineaNegocio to set
     */
    public void setLineaNegocio(String lineaNegocio) {
        this.lineaNegocio = lineaNegocio;
    }

    /**
     * @return the clasificacion
     */
    public Short getClasificacion() {
        return clasificacion;
    }

    /**
     * @param clasificacion the clasificacion to set
     */
    public void setClasificacion(Short clasificacion) {
        this.clasificacion = clasificacion;
    }

}
