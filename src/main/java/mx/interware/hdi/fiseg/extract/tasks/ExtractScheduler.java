package mx.interware.hdi.fiseg.extract.tasks;

import static mx.interware.hdi.fiseg.extract.util.Constants.RESIDENTES_CRON_EXPRESSION;
import static mx.interware.hdi.fiseg.extract.util.Constants.TIMEZONE;
import static mx.interware.hdi.fiseg.extract.util.Constants.TURISTAS_CRON_EXPRESSION;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import mx.interware.hdi.fiseg.extract.service.ProcessAutosService;
import mx.interware.hdi.fiseg.extract.util.AppProperties;
import mx.interware.hdi.fiseg.extract.util.LineaNegocio;

/**
 * Clase que encapsula las operaciones para revisi&oacute;n de estatus de los servicios
 * 
 * @author Interware
 * @since 2020-10-17
 */
@Component
public class ExtractScheduler {
    private static final Logger LOGGER = LogManager.getLogger(ExtractScheduler.class);

    @Autowired
    private ProcessAutosService residentesService;

    @Autowired
    private ProcessAutosService turistasService;

    @Autowired
    AppProperties appProperties;

    @Autowired
    ThreadPoolTaskScheduler scheduler;

    Map<String, ScheduledFuture<?>> jobsMap = new HashMap<>();

    /**
     * Crea una tarea de acuerdo a una expresi&oacute;n CRON
     * 
     * @param id         Identificador de la tarea
     * @param task       Tarea a ejecutar
     * @param expression Expresi&oacute;n CRON
     */
    public void addTaskToScheduler(String id, Runnable task, String expression) {
        LOGGER.info("Adding task [{}:{}] to scheduler ...", id, task);

        TimeZone zone = TimeZone.getTimeZone(appProperties.getProperty(TIMEZONE));
        ScheduledFuture<?> scheduledTask = scheduler.schedule(task, new CronTrigger(expression, zone));
        jobsMap.put(id, scheduledTask);
    }

    /**
     * Inicializa las propiedades a utilizar en la clase
     */
    @PostConstruct
    public void init() {
        String residentesExpr = appProperties.getProperty(RESIDENTES_CRON_EXPRESSION);
        String turistasExpr = appProperties.getProperty(TURISTAS_CRON_EXPRESSION);

        residentesService.setLineaNegocio(LineaNegocio.AUTR);
        turistasService.setLineaNegocio(LineaNegocio.AUTT);

        addTaskToScheduler(residentesService.getLineaNegocio().getId(), residentesService, residentesExpr);
        addTaskToScheduler(turistasService.getLineaNegocio().getId(), turistasService, turistasExpr);
    }
}