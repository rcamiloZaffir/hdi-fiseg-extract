package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.interware.hdi.fiseg.extract.entity.destination.MovimientoPolizaEntity;
/**
 * Repository con las operaciones necesarias para el manejo de los movimientos poliza
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface MovimientoPolizaDAO extends JpaRepository<MovimientoPolizaEntity, Long> {
	/**
	 * Obtiene el movimiento de poliza a partir del identificador 
	 * 
	 * @param movimientoPolizaStr Identificador para la b&uacute;squeda
	 * @return Entidad encontrada
	 */
	@Query("SELECT m FROM MovimientoPolizaEntity m WHERE movimientoPolizaStr = ?1")
	public MovimientoPolizaEntity findByMovimientoStr(String movimientoPolizaStr);
}
