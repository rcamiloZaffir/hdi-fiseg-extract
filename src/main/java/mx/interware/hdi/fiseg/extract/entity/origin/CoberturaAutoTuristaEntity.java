package mx.interware.hdi.fiseg.extract.entity.origin;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Entidad que contiene las propiedades de la vista con informacion de detalle
 * de cobertura Autos turistas
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Vw_Bi_AuttEmisionDetalleCobertura", schema = "{datasources.origin.schema}", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "IdOficina", "IdOficina", "NumPoliza", "NumDocumento", "SubAgente" }) })
public class CoberturaAutoTuristaEntity extends CoberturaDWH implements Serializable {

    private static final long serialVersionUID = 1L;

	@Column(name = "SubAgente")
	private Integer subAgente;

	/**
	 * @return the subAgente
	 */
	public Integer getSubAgente() {
		return subAgente;
	}

	/**
	 * @param subAgente the subAgente to set
	 */
	public void setSubAgente(Integer subAgente) {
		this.subAgente = subAgente;
	}

}
