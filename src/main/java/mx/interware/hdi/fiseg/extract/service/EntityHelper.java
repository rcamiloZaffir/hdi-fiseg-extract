package mx.interware.hdi.fiseg.extract.service;

import static mx.interware.hdi.fiseg.extract.util.Constants.ESTADO_ACTIVO;
import static mx.interware.hdi.fiseg.extract.util.Constants.FLOTILLA;
import static mx.interware.hdi.fiseg.extract.util.Constants.INDIVIDUAL;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_DEFAULT_LOG_USER;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_DEFAULT_PREFIX;
import static mx.interware.hdi.fiseg.extract.util.Constants.TURISTA;
import static mx.interware.hdi.fiseg.extract.util.Constants.USUARIO_CAMBIO_DEFAULT;

import java.time.LocalDateTime;
import java.util.Date;

import org.apache.kafka.common.cache.LRUCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mx.interware.hdi.fiseg.extract.entity.destination.ClientePolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.ClienteVehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.CoberturaVehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.HistoriaProcesamientoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.MovimientoPolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.PolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.VehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.CoberturaDWH;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoAutoResidenteEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoAutoTuristaEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoDWH;
import mx.interware.hdi.fiseg.extract.util.AppProperties;
import mx.interware.hdi.fiseg.extract.util.DateUtils;
import mx.interware.hdi.fiseg.extract.util.LineaNegocio;
import mx.interware.hdi.fiseg.extract.util.TipoEvento;

/**
 * Clase auxiliar que permite la trasnformaci&oacute;n de los elementos origen a la estructura destino
 * 
 * @author Interware
 * @since 2020-10-18
 */
@Component
public class EntityHelper {

    @Autowired
    private AppProperties appProperties;

    private static final Logger LOGGER = LogManager.getLogger(EntityHelper.class);

    private LRUCache<String, PolizaEntity> polizaCache = new LRUCache<>(10000);
    private LRUCache<String, MovimientoPolizaEntity> movimientoPolizaCache = new LRUCache<>(10000);

    /**
     * Almacena en cach&eacute; un elemento del tipo {@link PolizaEntity}
     * 
     * @param key    Identificador
     * @param poliza Objeto
     */
    public void putPolizaIntoCache(String key, PolizaEntity poliza) {
        polizaCache.put(key, poliza);
    }

    /**
     * Almacena en cach&eacute; un elemento del tipo {@link MovimientoPolizaEntity}
     * 
     * @param key              Identificador
     * @param movimientoPoliza Objeto
     */
    public void putMovimientoIntoCache(String key, MovimientoPolizaEntity movimientoPoliza) {
        movimientoPolizaCache.put(key, movimientoPoliza);
    }

    /**
     * Recupera de cach&eacute; un elemento del tipo {@link PolizaEntity}
     * 
     * @param key Identificador
     * @return Entidad recuperada
     */
    public PolizaEntity getPolizaFromCache(String key) {
        LOGGER.debug("Getting poliza from cache for [{}] ...", key);
        return polizaCache.get(key);
    }

    /**
     * Recupera de cach&eacute; un elemento del tipo {@link MovimientoPolizaEntity}
     * 
     * @param key Identificador
     * @return Entidad recuperada
     */
    public MovimientoPolizaEntity getMovimientoFromCache(String key) {
        LOGGER.debug("Getting movimientoPoliza from cache for [{}] ...", key);
        return movimientoPolizaCache.get(key);
    }

    /**
     * Crea un identificador de Poliza v&aacute;lido de acuerdo a los campos y linea de negocio
     * 
     * @param item Elemento con la informaci&oacute;n necesaria
     * @return Identificador creado
     */
    public String buildPolizaStr(MovimientoDWH item) {

        StringBuilder polizaStr = new StringBuilder();

        if (item instanceof MovimientoAutoResidenteEntity) {

            LOGGER.debug("Tipo poliza=> {}\tCotizacion => {}\tCertificado => {}", item.getIdTipoPoliza(), item.getNumCotizacion(), item.getNumCertificado());

            polizaStr.append(LineaNegocio.AUTR.getId());
            polizaStr.append("_");
            polizaStr.append(item.getIdOficina());
            polizaStr.append("_");
            polizaStr.append(item.getNumPoliza());
            polizaStr.append("_");
            polizaStr.append(item.getNumCertificado());
            if (item.getAnioPoliza() != null && item.getAnioPoliza().intValue() != 0) {
                polizaStr.append("_");
                polizaStr.append(item.getAnioPoliza());
            }
        } else {

            polizaStr.append(LineaNegocio.AUTT.getId());
            polizaStr.append("_");
            polizaStr.append( ((MovimientoAutoTuristaEntity)item).getSubAgente() );
            polizaStr.append("_");
            polizaStr.append(item.getNumPoliza());
			if (item.getAnioPoliza() != null && item.getAnioPoliza().intValue() != 0) {
                polizaStr.append("_");
                polizaStr.append(item.getAnioPoliza());
            }
        }

        return polizaStr.toString();
    }

    /**
     * Crea un identificador de movimiento de poliza v&aacute;lido de acuerdo a los campos y linea de negocio
     * 
     * @param item Elemento con la informaci&oacute;n necesaria
     * @return Identificador creado
     */
    public String buildMovimientoPolizaStr(MovimientoDWH item) {
        StringBuilder polizaStr = new StringBuilder();

        if (item instanceof MovimientoAutoResidenteEntity) {

            polizaStr.append(LineaNegocio.AUTR.getId());
            polizaStr.append("_");
            polizaStr.append(item.getIdOficina());
            polizaStr.append("_");
            polizaStr.append(item.getNumPoliza());
            polizaStr.append("_");
            polizaStr.append(item.getNumDocumento());
            polizaStr.append("_");
            polizaStr.append(item.getIdTipoDocumento());

            polizaStr.append("_");
            polizaStr.append(item.getNumCotizacion());
            polizaStr.append("_");
            polizaStr.append(item.getNumCertificado());
            polizaStr.append("_");
            polizaStr.append(item.getAnioPoliza());

        } else {
            polizaStr.append(LineaNegocio.AUTT.getId());
            polizaStr.append("_");
            polizaStr.append(item.getIdOficina());

            polizaStr.append("_");
            polizaStr.append( (( MovimientoAutoTuristaEntity)item).getSubAgente());

            polizaStr.append("_");
            polizaStr.append(item.getNumPoliza());
            polizaStr.append("_");
            polizaStr.append(item.getNumCotizacion());
            polizaStr.append("_");
            polizaStr.append(item.getNumDocumento());
 
        }

        return polizaStr.toString();
    }

    /**
     * Crea el n&uacute;mero de poliza a partir del id Oficina y el numero de la misma
     * 
     * @param movimiento Elemento de la base de datos origen con los campos utilizados
     * @return Objeto convertido
     */
    public String buildNumeroPoliza(MovimientoDWH movimiento) {
        StringBuilder numeroPoliza = new StringBuilder(appProperties.getProperty(SERVICE_DEFAULT_PREFIX));

        if (movimiento instanceof MovimientoAutoResidenteEntity) {
    
            numeroPoliza.append(movimiento.getIdOficina());
            numeroPoliza.append("-");
            numeroPoliza.append(movimiento.getNumPoliza());
            numeroPoliza.append("-");
            numeroPoliza.append(movimiento.getNumCertificado());
            if (movimiento.getAnioPoliza() != null && movimiento.getAnioPoliza().intValue() != 0) {
                numeroPoliza.append("-");
                numeroPoliza.append(movimiento.getAnioPoliza());
            }
        } else {
            numeroPoliza.append( ((MovimientoAutoTuristaEntity)movimiento).getSubAgente());
            numeroPoliza.append("-");
            numeroPoliza.append(movimiento.getNumPoliza());
        }
    
        LOGGER.debug("Se obtuvo numero poliza {}", numeroPoliza);
        return numeroPoliza.toString();
    }

    /**
     * Crea una entidad de tupo {@link MovimientoPolizaEntity} para su almacenamiento en la base destino
     * 
     * @param item Elemento de la base de datos origen
     * @param mov  Entidad de la base de datos
     * @return Objeto convertido
     */
    public MovimientoPolizaEntity buildMovimientoPoliza(MovimientoDWH item, MovimientoPolizaEntity mov) {

        String movimientoPolizaStr = buildMovimientoPolizaStr(item);

        MovimientoPolizaEntity movimientoPoliza = new MovimientoPolizaEntity();

        if (mov != null) {
            movimientoPoliza = mov;
        }
        movimientoPoliza.setMovimientoPolizaStr(movimientoPolizaStr);
        movimientoPoliza.setFechaMovimiento(item.getFechaEmision());
        if (item.getNumDocumento() != null) {
            movimientoPoliza.setNumeroDocumento(item.getNumDocumento().intValue());
        }
        if (item.getIdGrupoDocumento() != null) {
            movimientoPoliza.setTipoMovimiento(item.getIdGrupoDocumento().shortValue());
        }
        movimientoPoliza.setTipoPoliza(item.getIdTipoPoliza());
        movimientoPoliza.setNumeroVehiculo(Integer.valueOf(item.getNumCertificado()));

        movimientoPoliza.setFechacaptura(DateUtils.parseDateYmd("" + item.getFechaTransaccion()));
        movimientoPoliza.setFechaEmision(item.getFechaEmision());
        movimientoPoliza.setFechaInicioVigencia(item.getFechaInicioVigencia());
        movimientoPoliza.setFechaFinalVigencia(item.getFechaFinVigencia());

        movimientoPoliza.setTipoPoliza(item.getIdTipoPoliza());
        movimientoPoliza.setDescGrupoMovimiento(item.getDescGrupoDocumento());
        movimientoPoliza.setCausaCancelacion(item.getDescCausaCancelacion());
        movimientoPoliza.setIdCausaCancelacion(item.getIdCausaCancelacion());
        if (item.getIdCausaCancelacion() != null && item.getIdCausaCancelacion().intValue() != 0) {
            movimientoPoliza.setFechaCancelacion(item.getFechaEmision());
        }
        movimientoPoliza.setNumCertificado(item.getNumCertificado());

        movimientoPoliza.setUsuarioAltaMovPol(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
        movimientoPoliza.setUsuarioCambioMovPol(USUARIO_CAMBIO_DEFAULT);
        movimientoPoliza.setFechaCambioMovPol(getDefaultValeDate());
        movimientoPoliza.setFechaAltaMovPol(new Date());
        movimientoPoliza.setTabEstatus(ESTADO_ACTIVO);

        if (item instanceof MovimientoAutoResidenteEntity) {
            movimientoPoliza.setLineaNegocio(LineaNegocio.AUTR.getId());
            if (item.getIdTipoPoliza().intValue() == 4013) {
                movimientoPoliza.setClasificacion(INDIVIDUAL);
            } else if (item.getIdTipoPoliza().intValue() == 4014) {
                movimientoPoliza.setClasificacion(FLOTILLA);
            }
        } else {
            movimientoPoliza.setLineaNegocio(LineaNegocio.AUTT.getId());
            movimientoPoliza.setClasificacion(TURISTA);
        }
        return movimientoPoliza;
    }

    /**
     * Crea una entidad de tupo {@link PolizaEntity} para su almacenamiento en la base destino
     * 
     * @param item Elemento de la base de datos origen
     * @return Objeto convertido
     */
    public PolizaEntity buildPoliza(MovimientoDWH item) {
        LineaNegocio lineaNegocio = (item instanceof MovimientoAutoResidenteEntity) ? LineaNegocio.AUTR : LineaNegocio.AUTT;

        String numeroPoliza = buildNumeroPoliza(item);
        String polizaStr = buildPolizaStr(item);

        PolizaEntity poliza = new PolizaEntity();
        poliza.setPolizaStr(polizaStr);
        poliza.setNumeroPoliza(numeroPoliza);
        poliza.setPoliza(item.getNumPoliza());
        poliza.setAgenciaId(item.getIdOficina() + "");
        poliza.setNumeroCotizacion(item.getNumCotizacion() + "");
        poliza.setCanalVenta(item.getIdCanalVenta());
        poliza.setLineaNegocio(lineaNegocio.getId());

        if (item.getNipPerfilAgente() != null) {
            poliza.setAgenteId("" + item.getNipPerfilAgente());
        }

        // Cambiar por SubAgente para Turista 27-04-2021
        if( item instanceof MovimientoAutoTuristaEntity ){	
        	final MovimientoAutoTuristaEntity movimientoAutoTurista = (MovimientoAutoTuristaEntity) item;
        	poliza.setAgenteId("" + movimientoAutoTurista.getSubAgente());
        }

        poliza.setPolizaStr(polizaStr);
        poliza.setTabEstatus(ESTADO_ACTIVO);
        poliza.setUsuarioAltaPol(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
        poliza.setUsuarioCambioPol(USUARIO_CAMBIO_DEFAULT);
        poliza.setFechaCambioPol(getDefaultValeDate());
        poliza.setFechaAltaPol(new Date());
        return poliza;
    }

    /**
     * Crea una entidad de tupo {@link VehiculoEntity} para su almacenamiento en la base destino
     * 
     * @param item Elemento de la base de datos origen
     * @param vehi Vehiculo existente en la BD
     * @return Objeto convertido
     */
    public VehiculoEntity buildVehiculo(MovimientoDWH item, VehiculoEntity vehi) {

        VehiculoEntity vehiculo = new VehiculoEntity();
        if (vehi != null) {
            vehiculo = vehi;
        }
        vehiculo.setVin(item.getNumeroSerie());
        vehiculo.setInciso(item.getNumCertificado());
        if (item.getModeloVehiculo() != null) {
            vehiculo.setModelo(Integer.valueOf(item.getModeloVehiculo()));
        }
        vehiculo.setMarcaTipo(item.getIdMarcaTipoOcra());
        vehiculo.setUso(item.getIdUsoCNSF());
        vehiculo.setDescripcionUso(item.getDescUsoCNSF());
        if (item.getIdPaquete() != null) {
            vehiculo.setTipoSeguro(item.getIdPaquete().intValue());
        }
        if (item instanceof MovimientoAutoResidenteEntity) {
            MovimientoAutoResidenteEntity me = (MovimientoAutoResidenteEntity) item;
            vehiculo.setPlaca(me.getPlacas());
        }
        vehiculo.setUsuarioAltaVehiculo(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
        vehiculo.setFechaAltaVehiculo(new Date());
        vehiculo.setTabEstatus(ESTADO_ACTIVO);
        vehiculo.setFechaCambioVehiculo(getDefaultValeDate());
        vehiculo.setUsuarioCambioVehiculo(USUARIO_CAMBIO_DEFAULT);
        vehiculo.setObligatorio(item.getCuentaConSeguroObligatorio());
        vehiculo.setTipoSeguro(item.getIdTipoSeguro());
        vehiculo.setUbicacionAsegurado(item.getIdClienteMunicipio());
        // Revisar campo *IdClienteEntidad*
        return vehiculo;
    }

    /**
     * Crea una entidad de tupo {@link ClientePolizaEntity} para su almacenamiento en la base destino
     * 
     * @param item Elemento de la base de datos origen
     * @return Objeto convertido
     */
    public ClientePolizaEntity buildClientePoliza(MovimientoDWH item) {
        ClientePolizaEntity clientePoliza = new ClientePolizaEntity();
        String nombre = (item.getNombre() != null) ? item.getNombre() : item.getRazonSocial();
        clientePoliza.setNombre(nombre);
        clientePoliza.setApellidoPaterno(item.getApellido1());
        clientePoliza.setApellidoMaterno(item.getApellido2());
        clientePoliza.setRfc(item.getRfc());

        clientePoliza.setUsuarioAltaCtePol(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
        clientePoliza.setFechaAltaCtePol(new Date());
        clientePoliza.setTabEstatus(ESTADO_ACTIVO);
        clientePoliza.setFechaCambioCtePol(getDefaultValeDate());
        clientePoliza.setUsuarioCambioCtePol(USUARIO_CAMBIO_DEFAULT);

        return clientePoliza;
    }

    /**
     * Verifica que el dato sea diferente de vacio o Nulo
     * 
     * @param data Dato a verificar
     * @return True si es un dato valido
     */
    private Boolean existData(String data) {
        return data != null && !data.trim().isEmpty();
    }

    /**
     * Crea una entidad de tupo {@link ClienteVehiculoEntity} para su almacenamiento en la base destino
     * 
     * @param item Elemento de la base de datos origen
     * @param cte  Cliente existente en la BD
     * @return Objeto convertido
     */
    public ClienteVehiculoEntity buildClienteVehiculo(MovimientoDWH item, ClienteVehiculoEntity cte) {
        ClienteVehiculoEntity clienteVehiculo = new ClienteVehiculoEntity();
        if (cte != null) {
            clienteVehiculo = cte;
        }

        String nombre = Boolean.TRUE.equals(existData(item.getNombre())) ? item.getNombre() : item.getRazonSocial();

        clienteVehiculo.setNombreCte(nombre);

        if (Boolean.TRUE.equals(existData(item.getApellido1()))) {
            clienteVehiculo.setApellidoPaternoCte(item.getApellido1());
        }
        if (Boolean.TRUE.equals(existData(item.getApellido2()))) {
            clienteVehiculo.setApellidoMaternoCte(item.getApellido2());
        }
        if (Boolean.TRUE.equals(existData(item.getRfc()))) {
            clienteVehiculo.setRfcCte(item.getRfc());
        }

        clienteVehiculo.setUsuarioAltaCteVehi(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
        clienteVehiculo.setFechaAltaCteVehi(new Date());
        clienteVehiculo.setTabEstatus(ESTADO_ACTIVO);
        clienteVehiculo.setFechaCambioCteVehi(getDefaultValeDate());
        clienteVehiculo.setUsuarioCambioCteVehi(USUARIO_CAMBIO_DEFAULT);

        return clienteVehiculo;
    }

    /**
     * Crea una entidad de tupo {@link CoberturaVehiculoEntity} para su almacenamiento en la base destino
     * 
     * @param coberturaItem Elemento de la base de datos origen
     * @return Objeto convertido
     */
    public CoberturaVehiculoEntity buildCoberturaVehiculo(CoberturaDWH coberturaItem) {
        CoberturaVehiculoEntity coberturaVehiculo = new CoberturaVehiculoEntity();
        coberturaVehiculo.setCoberturaCveCobVehi(coberturaItem.getIdCoberturaRR8());
        coberturaVehiculo.setEstatusCobVehi(coberturaItem.getIdEstatusCobertura());
        coberturaVehiculo.setSumaAseguradaCobVehi(coberturaItem.getSumaAsegurada());
        coberturaVehiculo.setSumaAseguradaCveCobVehi(coberturaItem.getIdCobertura());
        coberturaVehiculo.setUsuarioAltaCobVehi(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
        coberturaVehiculo.setFechaAltaCobVehi(new Date());
        coberturaVehiculo.setTabEstatus(ESTADO_ACTIVO);
        coberturaVehiculo.setFechaCambioCobVehi(getDefaultValeDate());
        coberturaVehiculo.setUsuarioCambioCobVehi(USUARIO_CAMBIO_DEFAULT);
        return coberturaVehiculo;
    }

    /**
     * Crea un objeto de tipo {@link HistoriaProcesamientoEntity}
     * 
     * @param tipoEvento   Tipo de evento
     * @param lineaNegocio Residentes o turistas
     * @param uuid         Identificador generado
     * @param count        Conteo de lementos
     * @param isNew        Es un elemento nuevo en la base de datos
     * @return Elemento construido
     */
    public HistoriaProcesamientoEntity buildHistoryEntity(TipoEvento tipoEvento, LineaNegocio lineaNegocio, String uuid, int count, boolean isNew) {
        HistoriaProcesamientoEntity history = new HistoriaProcesamientoEntity();
        history.setProcesoExtraccionId(lineaNegocio.getKey().intValue());
        history.setProcesoExtraccionUuid(uuid);
        history.setTipoEvento(tipoEvento.getId());
        history.setFechaEvento(LocalDateTime.now());
        history.setCantidadMovimiento(count);
        history.setTabEstatus(ESTADO_ACTIVO);
        if (isNew) {
            history.setUsuarioAltaHistProc(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
            history.setFechaAltaHistProc(new Date());
            history.setUsuarioCambioHistProc(USUARIO_CAMBIO_DEFAULT);
            history.setFechaCambioHistProc(getDefaultValeDate());

        } else {
            history.setUsuarioAltaHistProc(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
            history.setFechaAltaHistProc(new Date());
            history.setUsuarioCambioHistProc(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
            history.setFechaCambioHistProc(new Date());
        }
        return history;
    }

    private Date getDefaultValeDate() {
        return new Date(0);
    }

}
