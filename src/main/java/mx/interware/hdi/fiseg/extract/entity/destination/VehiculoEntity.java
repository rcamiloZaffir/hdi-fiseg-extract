package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

import mx.interware.hdi.fiseg.extract.util.AddItem;
import mx.interware.hdi.fiseg.extract.util.RemoveItem;

/**
 * Entidad que contiene las propiedades de un vehiculo de la base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutVehiculo", schema = "{datasources.destination.schema}")
public class VehiculoEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -8638504650610766287L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Vehiculo_Id")
    @JsonProperty("vehiculoId")
    private Long vehiculoId;

    @Column(name = "Uso")
    @JsonProperty("uso")
    private Integer uso;

    @Column(name = "DescripcionUso")
    @JsonProperty("descripcionUso")
    private String descripcionUso;

    @Column(name = "Vin")
    @JsonProperty("vin")
    private String vin;

    @Column(name = "Obligatorio")
    @JsonProperty("obligatorio")
    private Integer obligatorio;

    @Column(name = "Modelo")
    @JsonProperty("modelo")
    private Integer modelo;

    @Column(name = "Placa")
    @JsonProperty("placa")
    private String placa;

    @Column(name = "Inciso")
    @JsonProperty("inciso")
    private String inciso;

    @Column(name = "MarcaTipo")
    @JsonProperty("marcaTipo")
    private Integer marcaTipo;

    @Column(name = "UbicacionAsegurado")
    @JsonProperty("ubicacionAsegurado")
    private Integer ubicacionAsegurado;

    @Column(name = "TipoSeguro")
    @JsonProperty("tipoSeguro")
    private Integer tipoSeguro;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MovPol_Id")
    private MovimientoPolizaEntity movimientoPoliza;

    @Column(name = "Vehi_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaVehiculo;

    @Column(name = "Vehi_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaVehiculo;

    @Column(name = "Vehi_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioVehiculo;

    @Column(name = "Vehi_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioVehiculo;

    @Transient
    @JsonProperty("coberturas")
    private List<CoberturaVehiculoEntity> coberturas = new ArrayList<>();

    @Transient
    @JsonProperty("clientes")
    private List<ClienteVehiculoEntity> clientes = new ArrayList<>();

    @Transient
    @JsonProperty("beneficiarios")
    private List<BeneficiarioEntity> beneficiarios = new ArrayList<>();

    /**
     * @return the vehiculoId
     */
    public Long getVehiculoId() {
        return vehiculoId;
    }

    /**
     * @param vehiculoId the vehiculoId to set
     */
    public void setVehiculoId(Long vehiculoId) {
        this.vehiculoId = vehiculoId;
    }

    /**
     * @return the uso
     */
    public Integer getUso() {
        return uso;
    }

    /**
     * @param uso the uso to set
     */
    public void setUso(Integer uso) {
        this.uso = uso;
    }

    /**
     * @return the vin
     */
    public String getVin() {
        return vin;
    }

    /**
     * @param vin the vin to set
     */
    public void setVin(String vin) {
        this.vin = vin;
    }

    /**
     * @return the obligatorio
     */
    public Integer getObligatorio() {
        return obligatorio;
    }

    /**
     * @param obligatorio the obligatorio to set
     */
    public void setObligatorio(Integer obligatorio) {
        this.obligatorio = obligatorio;
    }

    /**
     * @return the modelo
     */
    public Integer getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(Integer modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the inciso
     */
    public String getInciso() {
        return inciso;
    }

    /**
     * @param inciso the inciso to set
     */
    public void setInciso(String inciso) {
        this.inciso = inciso;
    }

    /**
     * @return the marcaTipo
     */
    public Integer getMarcaTipo() {
        return marcaTipo;
    }

    /**
     * @param marcaTipo the marcaTipo to set
     */
    public void setMarcaTipo(Integer marcaTipo) {
        this.marcaTipo = marcaTipo;
    }

    /**
     * @return the ubicacionAsegurado
     */
    public Integer getUbicacionAsegurado() {
        return ubicacionAsegurado;
    }

    /**
     * @param ubicacionAsegurado the ubicacionAsegurado to set
     */
    public void setUbicacionAsegurado(Integer ubicacionAsegurado) {
        this.ubicacionAsegurado = ubicacionAsegurado;
    }

    /**
     * @return the tipoSeguro
     */
    public Integer getTipoSeguro() {
        return tipoSeguro;
    }

    /**
     * @param tipoSeguro the tipoSeguro to set
     */
    public void setTipoSeguro(Integer tipoSeguro) {
        this.tipoSeguro = tipoSeguro;
    }

    /**
     * @return the movimientoPoliza
     */
    public MovimientoPolizaEntity getMovimientoPoliza() {
        return movimientoPoliza;
    }

    /**
     * @param movimientoPoliza the movimientoPoliza to set
     */
    public void setMovimientoPoliza(MovimientoPolizaEntity movimientoPoliza) {
        this.movimientoPoliza = movimientoPoliza;
    }

    /**
     * @return the coberturas
     */
    public List<CoberturaVehiculoEntity> getCoberturas() {
        return coberturas;
    }

    /**
     * @param coberturas the coberturas to set
     */
    public void setCoberturas(List<CoberturaVehiculoEntity> coberturas) {
        this.coberturas = coberturas;
    }

    /**
     * @return the clientes
     */
    public List<ClienteVehiculoEntity> getClientes() {
        return clientes;
    }

    /**
     * @param clientes the clientes to set
     */
    public void setClientes(List<ClienteVehiculoEntity> clientes) {
        this.clientes = clientes;
    }

    /**
     * @return the beneficiarios
     */
    public List<BeneficiarioEntity> getBeneficiarios() {
        return beneficiarios;
    }

    /**
     * @param beneficiarios the beneficiarios to set
     */
    public void setBeneficiarios(List<BeneficiarioEntity> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

    @AddItem(entity = VehiculoEntity.class, collection = "coberturas")
    public void addCobertura(CoberturaVehiculoEntity cobertura) {
        coberturas.add(cobertura);
        cobertura.setVehiculo(this);
    }

    @RemoveItem(entity = VehiculoEntity.class, collection = "coberturas")
    public void removeCobertura(CoberturaVehiculoEntity cobertura) {
        coberturas.remove(cobertura);
        cobertura.setVehiculo(null);
    }

    @AddItem(entity = VehiculoEntity.class, collection = "clientes")
    public void addCliente(ClienteVehiculoEntity cliente) {
        clientes.add(cliente);
        cliente.setVehiculo(this);
    }

    @RemoveItem(entity = VehiculoEntity.class, collection = "clientes")
    public void removeCliente(ClienteVehiculoEntity cliente) {
        clientes.remove(cliente);
        cliente.setVehiculo(null);
    }

    @AddItem(entity = BeneficiarioEntity.class, collection = "beneficiarios")
    public void addBeneficiario(BeneficiarioEntity beneficiario) {
        beneficiarios.add(beneficiario);
        beneficiario.setVehiculo(this);
    }

    @RemoveItem(entity = BeneficiarioEntity.class, collection = "beneficiarios")
    public void removeBeneficiario(BeneficiarioEntity beneficiario) {
        beneficiarios.remove(beneficiario);
        beneficiario.setVehiculo(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VehiculoEntity)) {
            return false;
        }

        Long id = ((VehiculoEntity) o).getVehiculoId();
        return (vehiculoId != null) && (vehiculoId.equals(id));
    }

    @Override
    public int hashCode() {
        return 772;
    }

    /**
     * @return the descripcionUso
     */
    public String getDescripcionUso() {
        return descripcionUso;
    }

    /**
     * @param descripcionUso the descripcionUso to set
     */
    public void setDescripcionUso(String descripcionUso) {
        this.descripcionUso = descripcionUso;
    }

    /**
     * @return the fechaAltaVehiculo
     */
    public Date getFechaAltaVehiculo() {
        return fechaAltaVehiculo;
    }

    /**
     * @param fechaAltaVehiculo the fechaAltaVehiculo to set
     */
    public void setFechaAltaVehiculo(Date fechaAltaVehiculo) {
        this.fechaAltaVehiculo = fechaAltaVehiculo;
    }

    /**
     * @return the usuarioAltaVehiculo
     */
    public String getUsuarioAltaVehiculo() {
        return usuarioAltaVehiculo;
    }

    /**
     * @param usuarioAltaVehiculo the usuarioAltaVehiculo to set
     */
    public void setUsuarioAltaVehiculo(String usuarioAltaVehiculo) {
        this.usuarioAltaVehiculo = usuarioAltaVehiculo;
    }

    /**
     * @return the fechaCambioVehiculo
     */
    public Date getFechaCambioVehiculo() {
        return fechaCambioVehiculo;
    }

    /**
     * @param fechaCambioVehiculo the fechaCambioVehiculo to set
     */
    public void setFechaCambioVehiculo(Date fechaCambioVehiculo) {
        this.fechaCambioVehiculo = fechaCambioVehiculo;
    }

    /**
     * @return the usuarioCambioVehiculo
     */
    public String getUsuarioCambioVehiculo() {
        return usuarioCambioVehiculo;
    }

    /**
     * @param usuarioCambioVehiculo the usuarioCambioVehiculo to set
     */
    public void setUsuarioCambioVehiculo(String usuarioCambioVehiculo) {
        this.usuarioCambioVehiculo = usuarioCambioVehiculo;
    }

}
