package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.interware.hdi.fiseg.extract.entity.destination.ClientePolizaEntity;
/**
 * Repository con las operaciones necesarias para el manejo de los clientes asociados a la Poliza
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface ClientePolizaDAO extends JpaRepository<ClientePolizaEntity, Integer> {

}
