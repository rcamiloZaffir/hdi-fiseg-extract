package mx.interware.hdi.fiseg.extract.service;

import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_BLOCK_SIZE;
import static mx.interware.hdi.fiseg.extract.util.Constants.ERROR;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTR_TOPIC;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTT_TOPIC;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_DEFAULT_LOG_USER;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_LIMIT_MAX_RECORDS_AUTR;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_LIMIT_MAX_RECORDS_AUTT;
import static mx.interware.hdi.fiseg.extract.util.Constants.SUCCESS;
import static mx.interware.hdi.fiseg.extract.util.Constants.WAITING;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import mx.interware.hdi.fiseg.extract.dto.MovimientoEventDTO;
import mx.interware.hdi.fiseg.extract.entity.destination.ClienteVehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.CoberturaVehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.HistoriaProcesamientoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.MovimientoPolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.PolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.ProcesoExtraccionEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.VehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.CoberturaDWH;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoAutoTuristaEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoDWH;
import mx.interware.hdi.fiseg.extract.repository.destination.ClienteVehiculoDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.CoberturaVehiculoDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.HistoriaProcesamientoDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.MovimientoPolizaDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.PolizaDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.ProcesoExtraccionDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.VehiculoDAO;
import mx.interware.hdi.fiseg.extract.repository.origin.MovimientoAutoResidenteDAO;
import mx.interware.hdi.fiseg.extract.repository.origin.MovimientoAutoTuristaDAO;
import mx.interware.hdi.fiseg.extract.util.AppProperties;
import mx.interware.hdi.fiseg.extract.util.DateUtils;
import mx.interware.hdi.fiseg.extract.util.LineaNegocio;
import mx.interware.hdi.fiseg.extract.util.ProcessStatus;
import mx.interware.hdi.fiseg.extract.util.StringUtils;
import mx.interware.hdi.fiseg.extract.util.TipoEvento;

/**
 * Ejecuta las operaciones para transformaci&oacute;n de autos Residentes
 * 
 * @author Interware
 * @since 2020-10-17
 */
@Service
@Scope("prototype")
public class ProcessAutosService implements AbstractService, Runnable {
    private static final Logger LOGGER = LogManager.getLogger(ProcessAutosService.class);

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private KafkaService kafkaService;

    @Autowired
    private EntityHelper entityHelper;

    @Autowired
    private PolizaDAO polizaDAO;

    @Autowired
    private MovimientoAutoTuristaDAO movimientoAutoTuristaDAO;

    @Autowired
    private MovimientoAutoResidenteDAO movimientoAutoResidenteDAO;

    @Autowired
    private MovimientoPolizaDAO movimientoPolizaDAO;

    @Autowired
    private VehiculoDAO vehiculoDAO;

    @Autowired
    private ClienteVehiculoDAO clienteVehiculoDAO;

    @Autowired
    private CoberturaVehiculoDAO coberturaVehiculoDAO;

    @Autowired
    private ProcesoExtraccionDAO procesoExtraccionDAO;

    @Autowired
    private HistoriaProcesamientoDAO historiaProcesamientoDAO;

    private String fechaExtraccion;

    private int conteo;

    private LineaNegocio lineaNegocio;

    /**
     * Convirte un objeto JSON a una entidad Poliza
     * 
     * @param polizaStr Objeto a convertir
     * @return Poliza obtenida
     */
    private PolizaEntity getPoliza(String polizaStr) {
        PolizaEntity poliza = entityHelper.getPolizaFromCache(polizaStr);
        if (poliza == null) {
            poliza = polizaDAO.findByPolizaStr(polizaStr);
        }
        return poliza;
    }

    /**
     * Convirte un objeto JSON a una entidad {@link MovimientoPolizaEntity}
     * 
     * @param polizaStr Objeto a convertir
     * @return Entidad obtenida
     */
	private MovimientoPolizaEntity getMovimientoPoliza(String movimientoPolizaStr) {
//       MovimientoPolizaEntity movimientoPoliza = entityHelper.getMovimientoFromCache(movimientoPolizaStr)
//       SI (movimientoPoliza == null) 
//            movimientoPoliza = movimientoPolizaDAO.findByMovimientoStr(movimientoPolizaStr)
		return movimientoPolizaDAO.findByMovimientoStr(movimientoPolizaStr);
	}

    /**
     * Realiza la trasnformaci&oacute; de un objeto con informacion de autos residentes origen para almacenarlo en una entidad destino, y devuelve el evento
     * para su posterior uso
     * 
     * @param item       Elemento a convertir
     * @param proceso    Proceso de extracci&oacute;n al que pertenece
     * @return Resultado
     */
    public MovimientoEventDTO storeMovimientoSnapshot(MovimientoDWH item, ProcesoExtraccionEntity proceso) {
        MovimientoEventDTO movimientoEvent = new MovimientoEventDTO();
        LOGGER.info("*** storeMovimientoSnapshot Status=START");
        LOGGER.debug("Movimiento {}-{}-{}-{}-{}-{} ", item.getIdOficina(), item.getNumPoliza(), item.getNumDocumento(),
        		item.getIdTipoDocumento(), item.getNumCotizacion(), item.getNumCertificado());

        List<? extends CoberturaDWH> coberturaItems = getCoberturasVehiculo(item);

        //info
        LOGGER.debug("Total Coberturas en el movimiento={}", coberturaItems.size());

        String polizaStr = entityHelper.buildPolizaStr(item);

        PolizaEntity poliza = getPoliza(polizaStr);

        if (poliza == null) {
            poliza = entityHelper.buildPoliza(item);
            // info
            LOGGER.debug("Storing Poliza record [{}] ...", polizaStr);
            poliza = polizaDAO.save(poliza);
            entityHelper.putPolizaIntoCache(polizaStr, poliza);
        } else {
        	//info
            LOGGER.debug("Poliza record [{}] has already been stored.", polizaStr);
        }
        Long polizaId = poliza.getPolizaId();

        //debug
        LOGGER.info("PolizaId={} , PolizaStr={}", polizaId, polizaStr);

        String movimientoPolizaStr = entityHelper.buildMovimientoPolizaStr(item);

        LOGGER.info("MovimientoPolizaStr=[{}]", movimientoPolizaStr);

        MovimientoPolizaEntity movimientoPoliza = getMovimientoPoliza(movimientoPolizaStr);

        Boolean movimientoExist = movimientoPoliza != null;

        if (movimientoPoliza == null || movimientoPoliza.getEstatusProcesamiento().shortValue() != 4 ) {
            LOGGER.info("Storing MovimientoPoliza record [{}] numero poliza[{}] ...", 
            		movimientoPolizaStr,poliza.getNumeroPoliza() );
            // MovimientoPoliza
            movimientoPoliza = entityHelper.buildMovimientoPoliza(item, movimientoPoliza);
            //info
            LOGGER.debug("Se agrega la poliza =>{} al movimiento ", polizaId);
            movimientoPoliza.setPoliza(poliza);
            movimientoPoliza.setEstatusProcesamiento(ProcessStatus.EXTRACTED.getStatus());
            entityHelper.putMovimientoIntoCache(polizaStr, movimientoPoliza);

            movimientoEvent.setMustProcess(true);

            LOGGER.debug("Poliza asociada al movimiento ={}", polizaId);
            movimientoPoliza = movimientoPolizaDAO.save(movimientoPoliza);

            Long movimientoPolizaId = movimientoPoliza.getMovimientoPolizaId();
            LOGGER.debug("MovimientoPolizaId= {}", movimientoPolizaId);
            poliza.setMovimientos(new ArrayList<>());
            poliza.addMovimiento(movimientoPoliza);

            // Vehículo
            VehiculoEntity vehiculo = null;
            if (movimientoExist) {
                vehiculo = vehiculoDAO.findByIdMovimientoPoliza(movimientoPoliza.getMovimientoPolizaId());
            }
            Boolean vehiculoExist = vehiculo != null;

            vehiculo = entityHelper.buildVehiculo(item, vehiculo);
            vehiculo.setMovimientoPoliza(movimientoPoliza);
            vehiculo = vehiculoDAO.save(vehiculo);
            movimientoPoliza.addVehiculo(vehiculo);

            // ClienteVehiculo
            ClienteVehiculoEntity clienteVehiculo = null;
            if (vehiculoExist) {
                clienteVehiculo = clienteVehiculoDAO.findByIdVehiculo(vehiculo.getVehiculoId());
                coberturaVehiculoDAO.deleteByIdVehiculo(vehiculo.getVehiculoId());
            }
            clienteVehiculo = entityHelper.buildClienteVehiculo(item, clienteVehiculo);

            clienteVehiculo.setVehiculo(vehiculo);
            clienteVehiculo = clienteVehiculoDAO.save(clienteVehiculo);
            vehiculo.addCliente(clienteVehiculo);

            for (CoberturaDWH coberturaItem : coberturaItems) {
                // CoberturaVehiculo
                CoberturaVehiculoEntity coberturaVehiculo = entityHelper.buildCoberturaVehiculo(coberturaItem);
                coberturaVehiculo.setVehiculo(vehiculo);
                coberturaVehiculo = coberturaVehiculoDAO.save(coberturaVehiculo);
                vehiculo.addCobertura(coberturaVehiculo);
            }

            movimientoEvent.setEventId("" + movimientoPolizaId);
            movimientoEvent.setPoliza(poliza);
        } else {
            LOGGER.info("MovimientoPoliza record [{}] has already been stored and is SUCCESS", movimientoPolizaStr);
            movimientoEvent.setMustProcess(false);
        }

        proceso.setFechaExtraccion(LocalDateTime.now());
        proceso.setFechaUltimoRegistro(item.getTiempoUltimoCambio());
        proceso.setUltimoRegistroExtraido(movimientoPolizaStr);
        proceso.setFechaCambioProcExtr(new Date());
        proceso.setUsuarioCambioProcExtr(appProperties.getProperty(SERVICE_DEFAULT_LOG_USER));
        proceso = procesoExtraccionDAO.save(proceso);
        LOGGER.debug("*** Se actualizó proceso => {}", proceso.getProcesoExtraccionId());

        LOGGER.info("PolizaStr=[{}] MovimientoPolizaStr=[{}]", polizaStr ,movimientoPolizaStr);
        LOGGER.info("*** storeMovimientoSnapshot Status=END");
        return movimientoEvent;
    }

    /**
     * Realiza el procesamiento de un elemento de autos residentes encontrados en la vista origen
     * 
     * @param movimiento Movimiento encontrado
     * @param proceso    Proceso de extracci&oacute;n en donde se encuentra agrupado
     * @return Resultado del proceso
     */
    public String processMovimiento(MovimientoDWH movimiento, ProcesoExtraccionEntity proceso) {
        LOGGER.info("--- processMovimiento Status=START");
        
        LOGGER.trace("Processing {} movement: [{}]", lineaNegocio, movimiento);

        String extracResult = SUCCESS;
        try {

            String topicName;

            if (LineaNegocio.AUTR.equals(lineaNegocio)) {
                topicName = appProperties.getProperty(KAFKA_EXTRACT_AUTR_TOPIC);
            } else {
                topicName = appProperties.getProperty(KAFKA_EXTRACT_AUTT_TOPIC);
            }

            Short tipoDocumento = movimiento.getIdTipoDocumento();
            String numeroPoliza = entityHelper.buildNumeroPoliza(movimiento);

            LOGGER.debug("TopicName=[{}]\tProcessing document [{}, {}]  ...", topicName, tipoDocumento, numeroPoliza);
            MovimientoEventDTO movimientoEvent = storeMovimientoSnapshot(movimiento, proceso);
            if (movimientoEvent.isMustProcess()) {
                String kafkaEvent = movimientoEvent.toString();
                String eventId = movimientoEvent.getEventId();
                LOGGER.debug("***************** Event id => {}", eventId);
                LOGGER.debug("***************** Topic Name => {}", topicName);
                LOGGER.debug("Sending event KafkaTopic {} , Movimiento {} ",topicName, kafkaEvent);
                kafkaService.sendEvent(topicName, kafkaEvent, eventId);
            } else {
                LOGGER.info("No need for event creation. Skipping ...");
            }

        } catch (Exception e) {
            LOGGER.error("Error al ejecutar el procesamiento", e);
            extracResult = ERROR;
        }

        LOGGER.info("--- processMovimiento Status=END");
        return extracResult;
    }

    /**
     * Busca los movimientos en la vista de autos residentes, de acuerdo a la ultima fecha de ejecuci&oacute;n
     * 
     * @param fechaUltimoRegistro fecha de &uacute;ltima ejecuci&oacute;n
     * @return Elementos obtenidos
     */
    @SuppressWarnings("all")
    public List<? extends MovimientoDWH> findUnprocessedMovements(Timestamp fechaUltimoRegistro) {
    	LOGGER.info("Searching for unprocessed movements :::: Status=START");
    	LOGGER.debug("Searching for unprocessed movements \t[ Block size={} ]", appProperties.getProperty(DS_ORIGIN_BLOCK_SIZE));
        String fechaFormateada = DateUtils.formatTimestamp(fechaUltimoRegistro);

        LOGGER.debug("Fecha ultimo registro extraido=[{}]", fechaFormateada);

        List<? extends MovimientoDWH> movimiento;

        if (LineaNegocio.AUTR.equals(lineaNegocio)) {
            movimiento = movimientoAutoResidenteDAO.findUnprocessedAUTR(fechaFormateada);
        } else {
            movimiento = movimientoAutoTuristaDAO.findUnprocessedAUTT(fechaFormateada);
        }

        LOGGER.debug("Found [{}] records   ........", movimiento.size());
        LOGGER.info("Searching for unprocessed movements :::: Status=END ... Found [{}] records ", movimiento.size());
        return movimiento;
    }

    private int getLimiteRegistros(LineaNegocio lineaNegocio) {
        if (LineaNegocio.AUTR.equals(lineaNegocio)) {
            return Integer.parseInt(appProperties.getProperty(SERVICE_LIMIT_MAX_RECORDS_AUTR));
        } else {
            return Integer.parseInt(appProperties.getProperty(SERVICE_LIMIT_MAX_RECORDS_AUTT));
        }

    }

    /**
     * Realiza una busqueda en la BD de aquellos elementos de autors residentes no procesados y en caso de encontrar elmentos los envia a procesamiento
     * 
     * @param processType Tipo de proceso
     * @return Resultado
     */
    public String executeExtraction(String processType) {
        LOGGER.info("Executing {} extraction Status=START ................", lineaNegocio);

        String extracResult = WAITING;

        try {
            LOGGER.debug("Tipo de procesamiento: {} ..............", processType);

            ProcesoExtraccionEntity proceso = procesoExtraccionDAO.findByTipoProcesamiento(processType);

            int limiteRegistros = getLimiteRegistros(lineaNegocio);

            int tamanioBloque = Integer.parseInt(appProperties.getProperty(DS_ORIGIN_BLOCK_SIZE));

            executeCompareFechaExtraccion();

            LOGGER.info("[ Límite de registros diarios={} ]\t[ Procesados hoy={} ]", limiteRegistros, conteo);

            Timestamp fechaUltimoRegistro = proceso.getFechaUltimoRegistro();

            String processUuid = StringUtils.generateRandomUUID();

            List<? extends MovimientoDWH> movimientos = null;

            int count = 0;
            boolean firstBlock = true;
            do {

                if (maximoDiario(limiteRegistros, conteo, tamanioBloque)) {
                    LOGGER.warn("***[ Se ha alcanzado el maximo numero de registros ]***");
                    extracResult = SUCCESS;
                    return extracResult;
                }

                movimientos = findUnprocessedMovements(fechaUltimoRegistro);
                if (movimientos != null && !movimientos.isEmpty()) {

                    count += movimientos.size();
                    conteo += movimientos.size();

                    if (firstBlock) {
                        HistoriaProcesamientoEntity startedHistory = entityHelper.buildHistoryEntity(TipoEvento.EXTRACTION_STARTED, lineaNegocio, processUuid,
                                0, Boolean.TRUE);
                        historiaProcesamientoDAO.save(startedHistory);
                        firstBlock = false;
                    }

                    LOGGER.info("--------------[ {} BLOCK START ]--------------", lineaNegocio);
                    for (MovimientoDWH movimiento : movimientos) {
                        LOGGER.info("Process Movimiento: {}-{}-{}-{}-{}-{} ", movimiento.getIdOficina(), movimiento.getNumPoliza(), movimiento.getNumDocumento(),
                                movimiento.getIdTipoDocumento(), movimiento.getNumCotizacion(), movimiento.getNumCertificado());
                        processMovimiento(movimiento, proceso);
                    }

                    LOGGER.info("--------------[ {} BLOCK END   ]--------------", lineaNegocio);
                    proceso = procesoExtraccionDAO.findByTipoProcesamiento(processType);
                    fechaUltimoRegistro = proceso.getFechaUltimoRegistro();

                }

            } while (movimientos != null && !movimientos.isEmpty());

            if (count > 0) {
                HistoriaProcesamientoEntity finishedHistory = entityHelper.buildHistoryEntity(TipoEvento.EXTRACTION_FINISHED, lineaNegocio, processUuid, count,
                        Boolean.FALSE);
                finishedHistory = historiaProcesamientoDAO.save(finishedHistory);
                LOGGER.debug("Se creó {} ", finishedHistory.getHistoriaProcesamientoId());
            }

            extracResult = SUCCESS;

        } catch (Exception e) {
            LOGGER.error("Error al realizar la extraccion", e);
            extracResult = ERROR;
        }
        LOGGER.info("Executing {} extraction Status=END ................", lineaNegocio);
        return extracResult;
    }

    private boolean maximoDiario(int limiteRegistros, int conteo, int tamanioBloque) {
        return limiteRegistros != 0 && (conteo + tamanioBloque) > limiteRegistros;
    }

    /**
     * Setea las propiedades de configuraci&oacute;n
     * 
     * @param appProperties Datos de variables
     */
    public void setAppProperties(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

    private void executeCompareFechaExtraccion() {
        Date fechaActual = new Date();
        String fechaExtraccionTemp = DateUtils.formatDateYmd(fechaActual);
        if (fechaExtraccion == null) {
            fechaExtraccion = fechaExtraccionTemp;
        } else {
            if (!fechaExtraccion.equals(fechaExtraccionTemp)) {
                fechaExtraccion = fechaExtraccionTemp;
                conteo = 0;
            }
        }
    }

    @SuppressWarnings("all")
    protected List<? extends CoberturaDWH> getCoberturasVehiculo(MovimientoDWH item) {
        List<? extends CoberturaDWH> coberturaItems;

        if (LineaNegocio.AUTR.equals(lineaNegocio)) {
            LOGGER.debug(
                    "Buscando Residentes [IdOficina={} AND NumPoliza={} AND NumDocumento={} AND IdTipoDocumento={} AND NumCotizacion={} AND NumCertificado={}]",
                    item.getIdOficina(), item.getNumPoliza(), item.getNumDocumento(), item.getIdTipoDocumento(), item.getNumCotizacion(),
                    item.getNumCertificado());
            coberturaItems = movimientoAutoResidenteDAO.findCoberturasFlotillaAUTR(item.getIdOficina(), item.getNumPoliza(), item.getNumDocumento(),
                    item.getIdTipoDocumento(), item.getNumCotizacion(), item.getNumCertificado());
        } else {

        	final Integer subAgente = ((MovimientoAutoTuristaEntity) item).getSubAgente();

            LOGGER.debug("Buscando Turistas [IdOficina={} AND NumPoliza={} AND NumDocumento={} AND SubAgente={} AND NumCotizacion={}]",
                    item.getIdOficina(), item.getNumPoliza(), item.getNumDocumento(), subAgente, item.getNumCotizacion());

            coberturaItems = movimientoAutoTuristaDAO.findCoberturasAUTT(item.getIdOficina(), item.getNumPoliza(), item.getNumDocumento(),
            		subAgente, item.getNumCotizacion());
        }
        return coberturaItems;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        LOGGER.info(">>>>>>> Iniciando proceso de extracción >>>>>>>");
        String processType = this.lineaNegocio.getId();
        executeExtraction(processType);
        LOGGER.info("<<<<<<< Terminando proceso de extracción <<<<<<");
    }

    public void setLineaNegocio(LineaNegocio lineaNegocio) {
        this.lineaNegocio = lineaNegocio;
    }

    public LineaNegocio getLineaNegocio() {
        return lineaNegocio;
    }

}
