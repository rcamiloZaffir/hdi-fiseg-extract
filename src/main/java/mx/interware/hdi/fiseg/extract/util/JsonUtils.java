package mx.interware.hdi.fiseg.extract.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import mx.interware.hdi.fiseg.extract.dto.MovimientoEventDTO;
import mx.interware.hdi.fiseg.extract.exception.ExtractException;

/**
 * Utileria para el manejo y conversion de JSONs
 * 
 * @author Interware
 * @since 2020-10-17
 * 
 */
public class JsonUtils {
    private static final Logger LOGGER = LogManager.getLogger(JsonUtils.class);

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.disable(MapperFeature.AUTO_DETECT_CREATORS, MapperFeature.AUTO_DETECT_FIELDS, MapperFeature.AUTO_DETECT_GETTERS,
                MapperFeature.AUTO_DETECT_IS_GETTERS);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    /**
     * Constructor privado
     */
    private JsonUtils() {
    }

    public static MovimientoEventDTO deserialize(String json) throws JsonProcessingException {
        LOGGER.debug("json: {}", json);
        MovimientoEventDTO movimientoEvent = mapper.readValue(json, MovimientoEventDTO.class);
        LOGGER.debug("movimientoEvent: {}", movimientoEvent);

        return movimientoEvent;
    }

    /**
     * Convierte un objeto a un JSON Gen&eacute;rico V&aacute;lido
     * 
     * @param obj Objeto a covertir
     * @return Json obtenido
     */
    public static String toJson(Object obj) {
        try {
            LOGGER.debug("Conversión finalizada");
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            LOGGER.error("Error al parsear el objeto", ex);
            throw new ExtractException(ex);
        }
    }

}