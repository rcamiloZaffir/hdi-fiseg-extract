package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.interware.hdi.fiseg.extract.entity.destination.ProcesoExtraccionEntity;

/**
 * Repository con las operaciones necesarias para el manejo de las entidades
 * para el proceso de extracci&oacute;n
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface ProcesoExtraccionDAO extends JpaRepository<ProcesoExtraccionEntity, Integer> {

	/**
	 * Obtiene los datos del proceso de extraci&oacute;n a partir del tipo de
	 * procesamiento
	 * 
	 * @param tipoProcesamiento Tipo procesamiento para la b&uacute;squeda
	 * @return Entidad encontrada
	 */
	@Query("SELECT p FROM ProcesoExtraccionEntity p WHERE tipoProcesamiento = ?1")
	public ProcesoExtraccionEntity findByTipoProcesamiento(String tipoProcesamiento);

}
