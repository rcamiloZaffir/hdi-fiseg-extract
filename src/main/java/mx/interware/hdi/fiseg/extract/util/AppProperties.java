package mx.interware.hdi.fiseg.extract.util;

import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_DDL_AUTO;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_DIALECT;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_DRIVER;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_ENTITY_PACKAGE;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_SCHEMA;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_URL;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_BLOCK_SIZE;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_DDL_AUTO;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_DIALECT;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_DRIVER;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_ENTITY_PACKAGE;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_SCHEMA;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_URL;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_BOOTSTRAP_ADDRESS;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTR_TOPIC;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTT_TOPIC;
import static mx.interware.hdi.fiseg.extract.util.Constants.RESIDENTES_CRON_EXPRESSION;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_DEFAULT_LOG_USER;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_DEFAULT_PREFIX;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_LIMIT_MAX_RECORDS_AUTR;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_LIMIT_MAX_RECORDS_AUTT;
import static mx.interware.hdi.fiseg.extract.util.Constants.TIMEZONE;
import static mx.interware.hdi.fiseg.extract.util.Constants.TURISTAS_CRON_EXPRESSION;

import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Componente para acceder a las variables de configuraci&oacute;n del microservicio
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
@Component
public class AppProperties {
    private static final Logger LOGGER = LogManager.getLogger(AppProperties.class);

    private static Map<String, String> propertiesMap = new TreeMap<>();
    private static Map<String, String> env = System.getenv();

    @Value("${scheduler.tasks.autos_residentes.cron_expression}")
    private String residentesCronExpression;

    @Value("${scheduler.tasks.autos_turistas.cron_expression}")
    private String turistasCronExpression;

    @Value("${application.timezone}")
    private String timeZone;

    @Value("${kafka.bootstrap_address}")
    private String bootstrapAddress;

    @Value("${kafka.topics.extract_autr}")
    private String extractAUTRTopic;

    @Value("${kafka.topics.extract_autt}")
    private String extractAUTTTopic;

    @Value("${datasources.origin.url}")
    private String dsOriginUrl;

    @Value("${datasources.origin.driver}")
    private String dsOriginDriver;

    @Value("${datasources.origin.schema}")
    private String dsOriginSchema;

    @Value("${datasources.origin.dialect}")
    private String dsOriginDialect;

    @Value("${datasources.origin.ddl_auto}")
    private String dsOriginDdlAuto;

    @Value("${datasources.origin.entity_package}")
    private String dsOriginEntityPackage;

    @Value("${datasources.origin.block.size}")
    private String dsOriginBlockSize;

    @Value("${datasources.destination.url}")
    private String dsDestinationUrl;

    @Value("${datasources.destination.driver}")
    private String dsDestinationDriver;

    @Value("${datasources.destination.schema}")
    private String dsDestinationSchema;

    @Value("${datasources.destination.dialect}")
    private String dsDestinationDialect;

    @Value("${datasources.destination.ddl_auto}")
    private String dsDestinationDdlAuto;

    @Value("${datasources.destination.entity_package}")
    private String dsDestinationEntityPackage;

    @Value("${service.limit.max_records_autr}")
    private String serviceLimitMaxRecordsAutr;

    @Value("${service.limit.max_records_autt}")
    private String serviceLimitMaxRecordsAutt;

    @Value("${service.default.log_user}")
    private String defaultLogUser;

    @Value("${service.default.prefix}")
    private String defaultPrefix;

    /**
     * Agrega una propiedad a la configurac&oacute;n
     * 
     * @param name         Nombre de la propiedad
     * @param defaultValue Valor de la propriedad
     */
    private void addProperty(String name, String defaultValue) {
        String value = env.get(name);
        if (value != null) {
            LOGGER.trace("Adding property from environment :{} = \"{}\"", name, value);
            propertiesMap.put(name, value);
        } else {
            LOGGER.trace("Adding property from default     :{} = \"{}\"", name, defaultValue);
            propertiesMap.put(name, defaultValue);
        }
    }

    /**
     * Inicializa las propiedades de la aplicaci&oacute;n
     */
    @PostConstruct
    private void init() {
        addProperty(RESIDENTES_CRON_EXPRESSION, residentesCronExpression);
        addProperty(TURISTAS_CRON_EXPRESSION, turistasCronExpression);
        addProperty(TIMEZONE, timeZone);
        addProperty(KAFKA_BOOTSTRAP_ADDRESS, bootstrapAddress);
        addProperty(KAFKA_EXTRACT_AUTR_TOPIC, extractAUTRTopic);
        addProperty(KAFKA_EXTRACT_AUTT_TOPIC, extractAUTTTopic);

        addProperty(DS_ORIGIN_URL, dsOriginUrl);
        addProperty(DS_ORIGIN_DRIVER, dsOriginDriver);
        addProperty(DS_ORIGIN_SCHEMA, dsOriginSchema);
        addProperty(DS_ORIGIN_DIALECT, dsOriginDialect);
        addProperty(DS_ORIGIN_DDL_AUTO, dsOriginDdlAuto);
        addProperty(DS_ORIGIN_ENTITY_PACKAGE, dsOriginEntityPackage);
        addProperty(DS_ORIGIN_BLOCK_SIZE, dsOriginBlockSize);

        addProperty(DS_DESTINATION_URL, dsDestinationUrl);
        addProperty(DS_DESTINATION_DRIVER, dsDestinationDriver);
        addProperty(DS_DESTINATION_SCHEMA, dsDestinationSchema);
        addProperty(DS_DESTINATION_DIALECT, dsDestinationDialect);
        addProperty(DS_DESTINATION_DDL_AUTO, dsDestinationDdlAuto);
        addProperty(DS_DESTINATION_ENTITY_PACKAGE, dsDestinationEntityPackage);
        addProperty(SERVICE_LIMIT_MAX_RECORDS_AUTR, serviceLimitMaxRecordsAutr);
        addProperty(SERVICE_LIMIT_MAX_RECORDS_AUTT, serviceLimitMaxRecordsAutt);
        addProperty(SERVICE_DEFAULT_LOG_USER, defaultLogUser);
        addProperty(SERVICE_DEFAULT_PREFIX, defaultPrefix);
    }

    /**
     * Obtiene el valor de una propiedad
     * 
     * @param name Nombre de la propiedad
     * @return Valor obtenido
     */
    public String getProperty(String name) {
        String property = propertiesMap.get(name);
        LOGGER.trace("Getting property:[{}=\"{}\"] ...", name, property);
        return property;
    }

}