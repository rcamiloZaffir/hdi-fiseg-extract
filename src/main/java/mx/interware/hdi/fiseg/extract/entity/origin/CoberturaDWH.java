package mx.interware.hdi.fiseg.extract.entity.origin;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Entidad que contiene las propiedades de la vista con informacion de detalle de cobertura
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@MappedSuperclass
public class CoberturaDWH implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "IdOficina")
    private Short idOficina;

    @Id
    @Column(name = "NumPoliza")
    private Long numPoliza;

    @Id
    @Column(name = "NumDocumento")
    private Short numDocumento;

    @Id
    @Column(name = "NumCertificado")
    private String numCertificado;

    @Id
    @Column(name = "NumCotizacion")
    private Integer numCotizacion;

    @Id
    @Column(name = "IdCobertura")
    private Short idCobertura;

    @Column(name = "FechaTransaccion")
    private Integer fechaTransaccion;

    @Column(name = "IdReglaCobertura")
    private Short idReglaCobertura;

    @Column(name = "IdCoberturaRR8")
    private Short idCoberturaRR8;

    @Column(name = "DescCobertura")
    private String descCobertura;

    @Column(name = "SumaAsegurada")
    private BigDecimal sumaAsegurada;

    @Column(name = "IdEstatusCobertura")
    private Short idEstatusCobertura;

    /**
     * @return the numCotizacion
     */
    public Integer getNumCotizacion() {
        return numCotizacion;
    }

    /**
     * @param numCotizacion the numCotizacion to set
     */
    public void setNumCotizacion(Integer numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    /**
     * @return the fechaTransaccion
     */
    public Integer getFechaTransaccion() {
        return fechaTransaccion;
    }

    /**
     * @param fechaTransaccion the fechaTransaccion to set
     */
    public void setFechaTransaccion(Integer fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    /**
     * @return the numDocumento
     */
    public Short getNumDocumento() {
        return numDocumento;
    }

    /**
     * @param numDocumento the numDocumento to set
     */
    public void setNumDocumento(Short numDocumento) {
        this.numDocumento = numDocumento;
    }

    /**
     * @return the idOficina
     */
    public Short getIdOficina() {
        return idOficina;
    }

    /**
     * @param idOficina the idOficina to set
     */
    public void setIdOficina(Short idOficina) {
        this.idOficina = idOficina;
    }

    /**
     * @return the numPoliza
     */
    public Long getNumPoliza() {
        return numPoliza;
    }

    /**
     * @param numPoliza the numPoliza to set
     */
    public void setNumPoliza(Long numPoliza) {
        this.numPoliza = numPoliza;
    }

    /**
     * @return the numCertificado
     */
    public String getNumCertificado() {
        return numCertificado;
    }

    /**
     * @param numCertificado the numCertificado to set
     */
    public void setNumCertificado(String numCertificado) {
        this.numCertificado = numCertificado;
    }

    /**
     * @return the idReglaCobertura
     */
    public Short getIdReglaCobertura() {
        return idReglaCobertura;
    }

    /**
     * @param idReglaCobertura the idReglaCobertura to set
     */
    public void setIdReglaCobertura(Short idReglaCobertura) {
        this.idReglaCobertura = idReglaCobertura;
    }

    /**
     * @return the idCobertura
     */
    public Short getIdCobertura() {
        return idCobertura;
    }

    /**
     * @param idCobertura the idCobertura to set
     */
    public void setIdCobertura(Short idCobertura) {
        this.idCobertura = idCobertura;
    }

    /**
     * @return the idCoberturaRR8
     */
    public Short getIdCoberturaRR8() {
        return idCoberturaRR8;
    }

    /**
     * @param idCoberturaRR8 the idCoberturaRR8 to set
     */
    public void setIdCoberturaRR8(Short idCoberturaRR8) {
        this.idCoberturaRR8 = idCoberturaRR8;
    }

    /**
     * @return the descCobertura
     */
    public String getDescCobertura() {
        return descCobertura;
    }

    /**
     * @param descCobertura the descCobertura to set
     */
    public void setDescCobertura(String descCobertura) {
        this.descCobertura = descCobertura;
    }

    /**
     * @return the sumaAsegurada
     */
    public BigDecimal getSumaAsegurada() {
        return sumaAsegurada;
    }

    /**
     * @param sumaAsegurada the sumaAsegurada to set
     */
    public void setSumaAsegurada(BigDecimal sumaAsegurada) {
        this.sumaAsegurada = sumaAsegurada;
    }

    /**
     * @return the idEstatusCobertura
     */
    public Short getIdEstatusCobertura() {
        return idEstatusCobertura;
    }

    /**
     * @param idEstatusCobertura the idEstatusCobertura to set
     */
    public void setIdEstatusCobertura(Short idEstatusCobertura) {
        this.idEstatusCobertura = idEstatusCobertura;
    }

}
