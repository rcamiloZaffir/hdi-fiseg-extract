package mx.interware.hdi.fiseg.extract.repository.origin;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.interware.hdi.fiseg.extract.entity.origin.CoberturaAutoTuristaEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoAutoTuristaEntity;

/**
 * Repository con las operaciones necesarias para el manejo de Movimientos de autos turistas
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface MovimientoAutoTuristaDAO extends JpaRepository<MovimientoAutoTuristaEntity, Integer> {

    /**
     * Obtiene una lista de movimientos de autos turistas a partir de la fecha de ultimo registro
     * 
     * @param fechaUltimoRegistro Criterio de b&uacute;squeda
     * @return Lista de movimientos encontrados
     */
    @Query(value = "SELECT TOP {datasource.origin.block.size} * FROM {datasources.origin.schema}.Vw_BI_AuttEmisionDetalle WHERE TiempoUltimoCambio > ?1 AND numDocumento >= 0 ORDER BY TiempoUltimoCambio ASC,NumPoliza ASC,NumDocumento ASC", nativeQuery = true)
    public List<MovimientoAutoTuristaEntity> findUnprocessedAUTT(String fechaUltimoRegistro);

    /**
     * Obtiene una lista de coberturas para movimientos de autos turistas a partir de determinados par&aacute;metros
     * 
     * @param idOficina       Identificador de oficinal
     * @param numPoliza       N&uacute;mero de poliza
     * @param numDocumento    N&uacute;mero de documento
     * @param nipPerfilAgente Nip Perfil agente
     * @param numCotizacion Numero de cotización
     * @return Lista de Coberturas encontradas
     */
    @Query("SELECT c FROM CoberturaAutoTuristaEntity c WHERE idOficina = ?1 AND numPoliza = ?2 AND numDocumento = ?3 AND subAgente = ?4 AND numCotizacion = ?5")
    public List<CoberturaAutoTuristaEntity> findCoberturasAUTT(Short idOficina, Long numPoliza, Short numDocumento, Integer subAgente,Integer numCotizacion);

}
