package mx.interware.hdi.fiseg.extract.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Clase que encapsula las operaciones para revisi&oacute;n de estatus de los servicios
 * 
 * @author Interware
 * @since 2020-10-17
 */
@RestController
public class HealthController {
    private static final Logger LOGGER = LogManager.getLogger(HealthController.class);

    /**
     * Retorna la cadena "Pong" en respuesta al ping
     * 
     * @return String pong
     */
    @GetMapping("/api/ping")
    @ApiOperation(value = "Consulta Ping", response = String.class, notes = "Devuelve el valor pong, para pruebas de iniciación del MS")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Ok") })
    public String ping() {
        LOGGER.info("Pinging server ...");
        return "pong";
    }

    /**
     * Devuelve el valor compuesto de una KEY, para pruebas de iniciación del MS
     * 
     * @param key Objeto para generar response
     * @return Respuesta
     */

    @GetMapping("/api/health/{key}")
    @ApiOperation(value = "Consulta KEY", response = String.class, notes = "Devuelve el valor compuesto de una KEY, para pruebas de iniciación del MS")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Ok") })
    public ResponseEntity<String> health(@PathVariable(value = "key") String key) {
        LOGGER.info("Verifying health ...");

        String result = "{'action':'verifyHealth', 'key':'" + key + "', 'status':'OK'}";
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Devuelve el valor compuesto de una KEY, para pruebas de iniciación del MS
     * 
     * @param name Objeto para generar response
     * @return Respuesta
     */
    @GetMapping("/api/sayHi/{name}")
    @ApiOperation(value = "Consulta Saludo", response = String.class, notes = "Devuelve una cadena en donde contiene el saludo que se le envia como parametro")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Ok") })
    public ResponseEntity<String> sayHi(@PathVariable(value = "name") String name) {
        LOGGER.info("Saying Hi! ...");

        String result = "Hello there " + name + "!";
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
