package mx.interware.hdi.fiseg.extract;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * Clase principal para iniciar el microservicio de Extraccion de datos
 * 
 * @author Interware
 * @since 2020-10-17
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, SpringDataWebAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class,
		HibernateJpaAutoConfiguration.class, KafkaAutoConfiguration.class })
@EnableSwagger2
@EnableOpenApi
public class ExtractApplication {
	private static final Logger LOGGER = LogManager.getLogger(ExtractApplication.class);

	@Value("${application.name}")
	private String applicationName;

	@Value("${application.version}")
	private String applicationVersion;

	/**
	 * <b>Main</b> Punto de inicio de la aplicaci&oacute;n
	 *
	 * @param args argumentos de l&iacute;nea de comandos
	 */
	public static void main(String[] args) {
		SpringApplication.run(ExtractApplication.class, args);
	}

	/**
	 * Retorna un bean con la informaci&oacute;n del API
	 * 
	 * @return Bean con informaci&oacute;n del api
	 */
	private ApiInfo getApiInfo() {

		StringBuilder titulo = new StringBuilder("HDI ");
		titulo.append(applicationName);
		titulo.append("\tAPI Info");

		LOGGER.debug("Titulo ={}", titulo);

		StringBuilder msj = new StringBuilder("HDI ");
		msj.append(applicationName);
		msj.append(" API reference for developers");

		LOGGER.debug("Descripcion ={}", msj);

		return new ApiInfoBuilder().title(titulo.toString()).description(msj.toString()).version(applicationVersion).build();
	}

	/**
	 * Retorna un bean para la generaci&oacute;n de los endpoints swagger2
	 * 
	 * @return Swagger configuration bean
	 */
	@Bean
	public Docket api() {
		LOGGER.info("Adding swagger support ..");
		return new Docket(DocumentationType.OAS_30).groupName("public-api").apiInfo(getApiInfo()).select().paths(PathSelectors.any()).build();
	}

	/**
	 * Retorna un bean con la configuraci&oacute;n del los hilos de ejecuci&oacute;n
	 * 
	 * @return Bean con configuracion&oacute;n de threads
	 */
	@Bean
	public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
		ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
		threadPoolTaskScheduler.setPoolSize(5);
		threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
		return threadPoolTaskScheduler;
	}
}
