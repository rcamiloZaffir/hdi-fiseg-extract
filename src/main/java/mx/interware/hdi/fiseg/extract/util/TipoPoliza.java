package mx.interware.hdi.fiseg.extract.util;

/**
 * Enumeraci&oacute;n con los posibles tipo de movimientos de poliza Flotilla o Individual
 * 
 * @author Interware
 * @since 2020-10-21
 *
 */
public enum TipoPoliza {
    FLOTILLA((short) 4014, "Flotilla"), INDIVIDUAL((short) 4013, "Individual");

    private Short idTipoPoliza;
    private String description;

    /**
     * Constructor que incializa el Enum
     * 
     * @param idTipoPoliza Identificador
     * @param description  Descripcio&oacute;n
     */
    TipoPoliza(Short idTipoPoliza, String description) {
        this.idTipoPoliza = idTipoPoliza;
        this.description = description;
    }

    /**
     * Retorna el Identificador
     * 
     * @return Identificador
     */
    public Short getIdTipoPoliza() {
        return idTipoPoliza;
    }

    /**
     * Retorna la descripci&oacute;n
     * 
     * @return Descripci&oacute;n
     */
    public String getDescription() {
        return description;
    }

    /**
     * Busca el objeto por el id enviado
     * 
     * @param id Identificador para la b&uacute;squeda
     * @return Objeto encontrado
     */
    public static TipoPoliza findById(Short id) {
        for (TipoPoliza tipoEvento : values()) {
            if (tipoEvento.getIdTipoPoliza().equals(id)) {
                return tipoEvento;
            }
        }
        return null;
    }
}