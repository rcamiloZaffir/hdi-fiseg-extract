package mx.interware.hdi.fiseg.extract.entity.origin;

import java.io.Serializable;

/**
 * Entidad que permite identificar los multiples campos que componen el id en movimientos de cobertura
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
public class CoberturaResidentesIdDWH implements Serializable {

    private static final long serialVersionUID = 1L;

    Short idOficina;

    Long numPoliza;

    Short numDocumento;

    Short idTipoDocumento;

    String numCertificado;

    Integer numCotizacion;

    Short idCobertura;

}
