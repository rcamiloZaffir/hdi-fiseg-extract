package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entidad que contiene las propiedades de un beneficiario de la base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutBeneficiario", schema = "{datasources.destination.schema}")
public class BeneficiarioEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -4583856665185669433L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Beneficiario_Id")
    @JsonProperty("beneficiarioId")
    private Long beneficiarioId;

    @Column(name = "ApellidoPaterno")
    @JsonProperty("apellidoPaterno")
    private String apellidoPaternoBenef;

    @Column(name = "ApellidoMaterno")
    @JsonProperty("apellidoMaterno")
    private String apellidoMaternoBenef;

    @Column(name = "Nombre")
    @JsonProperty("nombre")
    private String nombreBenef;

    @Column(name = "Rfc")
    @JsonProperty("rfc")
    private String rfcBenef;

    @Column(name = "Curp")
    @JsonProperty("curp")
    private String curpBenef;

    @Column(name = "TipoPersona")
    @JsonProperty("tipoPersona")
    private String tipoPersonaBenef;


    @Column(name = "Benef_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaBenef;

    @Column(name = "Benef_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaBenef;

    @Column(name = "Benef_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioBenef;

    @Column(name = "Benef_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioBenef;

    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Vehiculo_Id")
    private VehiculoEntity vehiculo;

    /**
     * @return the beneficiarioId
     */
    public Long getBeneficiarioId() {
        return beneficiarioId;
    }

    /**
     * @param beneficiarioId the beneficiarioId to set
     */
    public void setBeneficiarioId(Long beneficiarioId) {
        this.beneficiarioId = beneficiarioId;
    }

    /**
     * @return the apellidoPaterno
     */
    public String getApellidoPaternoBenef() {
        return apellidoPaternoBenef;
    }

    /**
     * @param apellidoPaterno the apellidoPaterno to set
     */
    public void setApellidoPaternoBenef(String apellidoPaterno) {
        this.apellidoPaternoBenef = apellidoPaterno;
    }

    /**
     * @return the apellidoMaterno
     */
    public String getApellidoMaternoBenef() {
        return apellidoMaternoBenef;
    }

    /**
     * @param apellidoMaterno the apellidoMaterno to set
     */
    public void setApellidoMaternoBenef(String apellidoMaterno) {
        this.apellidoMaternoBenef = apellidoMaterno;
    }

    /**
     * @return the nombre
     */
    public String getNombreBenef() {
        return nombreBenef;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombreBenef(String nombre) {
        this.nombreBenef = nombre;
    }

    /**
     * @return the rfc
     */
    public String getRfcBenef() {
        return rfcBenef;
    }

    /**
     * @param rfc the rfc to set
     */
    public void setRfcBenef(String rfc) {
        this.rfcBenef = rfc;
    }

    /**
     * @return the curp
     */
    public String getCurpBenef() {
        return curpBenef;
    }

    /**
     * @param curp the curp to set
     */
    public void setCurpBenef(String curp) {
        this.curpBenef = curp;
    }

    /**
     * @return the tipoPersona
     */
    public String getTipoPersonaBenef() {
        return tipoPersonaBenef;
    }

    /**
     * @param tipoPersona the tipoPersona to set
     */
    public void setTipoPersonaBenef(String tipoPersona) {
        this.tipoPersonaBenef = tipoPersona;
    }

    /**
     * @return the vehiculo
     */
    public VehiculoEntity getVehiculo() {
        return vehiculo;
    }

    /**
     * @param vehiculo the vehiculo to set
     */
    public void setVehiculo(VehiculoEntity vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BeneficiarioEntity)) {
            return false;
        }

        Long id = ((BeneficiarioEntity) o).getBeneficiarioId();
        return (beneficiarioId != null) && (beneficiarioId.equals(id));
    }

    @Override
    public int hashCode() {
        return 776;
    }

    /**
     * @return the fechaAltaBenef
     */
    public Date getFechaAltaBenef() {
        return fechaAltaBenef;
    }

    /**
     * @param fechaAltaBenef the fechaAltaBenef to set
     */
    public void setFechaAltaBenef(Date fechaAltaBenef) {
        this.fechaAltaBenef = fechaAltaBenef;
    }

    /**
     * @return the usuarioAltaBenef
     */
    public String getUsuarioAltaBenef() {
        return usuarioAltaBenef;
    }

    /**
     * @param usuarioAltaBenef the usuarioAltaBenef to set
     */
    public void setUsuarioAltaBenef(String usuarioAltaBenef) {
        this.usuarioAltaBenef = usuarioAltaBenef;
    }

    /**
     * @return the fechaCambioBenef
     */
    public Date getFechaCambioBenef() {
        return fechaCambioBenef;
    }

    /**
     * @param fechaCambioBenef the fechaCambioBenef to set
     */
    public void setFechaCambioBenef(Date fechaCambioBenef) {
        this.fechaCambioBenef = fechaCambioBenef;
    }

    /**
     * @return the usuarioCambioBenef
     */
    public String getUsuarioCambioBenef() {
        return usuarioCambioBenef;
    }

    /**
     * @param usuarioCambioBenef the usuarioCambioBenef to set
     */
    public void setUsuarioCambioBenef(String usuarioCambioBenef) {
        this.usuarioCambioBenef = usuarioCambioBenef;
    }
    

}
