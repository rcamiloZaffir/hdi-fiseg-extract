package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entidad que contiene las propiedades de un proceso de extracci&oacute;n de la base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutProcesoExtraccion", schema = "{datasources.destination.schema}")
public class ProcesoExtraccionEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -6054086180780497623L;

    @Id
    @Column(name = "ProcExtrac_Id")
    private Integer procesoExtraccionId;

    @Column(name = "TipoProcesamiento")
    private String tipoProcesamiento;

    @Column(name = "Descripcion")
    private String descripcion;

    @Column(name = "FechaExtraccion")
    private LocalDateTime fechaExtraccion;

    @Column(name = "UltimoRegistroExtraido")
    private String ultimoRegistroExtraido;

    @Column(name = "FechaUltimoRegistro")
    private Timestamp fechaUltimoRegistro;
    
    @Column(name = "ProcExtr_FechaAlta")
    @JsonIgnore
    private Date fechaAltaProcExtr;

    @Column(name = "ProcExtr_UsuarioAlta")
    @JsonIgnore
    private String usuarioAltaProcExtr;

    @Column(name = "ProcExtr_FechaCambio", nullable = false)
    @JsonIgnore
    private Date fechaCambioProcExtr;

    @Column(name = "ProcExtr_UsuarioCambio", nullable = false)
    @JsonIgnore
    private String usuarioCambioProcExtr;

    /**
     * @return the procesoExtraccionId
     */
    public Integer getProcesoExtraccionId() {
        return procesoExtraccionId;
    }

    /**
     * @param procesoExtraccionId the procesoExtraccionId to set
     */
    public void setProcesoExtraccionId(Integer procesoExtraccionId) {
        this.procesoExtraccionId = procesoExtraccionId;
    }

    /**
     * @return the tipoProcesamiento
     */
    public String getTipoProcesamiento() {
        return tipoProcesamiento;
    }

    /**
     * @param tipoProcesamiento the tipoProcesamiento to set
     */
    public void setTipoProcesamiento(String tipoProcesamiento) {
        this.tipoProcesamiento = tipoProcesamiento;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the fechaExtraccion
     */
    public LocalDateTime getFechaExtraccion() {
        return fechaExtraccion;
    }

    /**
     * @param fechaExtraccion the fechaExtraccion to set
     */
    public void setFechaExtraccion(LocalDateTime fechaExtraccion) {
        this.fechaExtraccion = fechaExtraccion;
    }

    /**
     * @return the ultimoRegistroExtraido
     */
    public String getUltimoRegistroExtraido() {
        return ultimoRegistroExtraido;
    }

    /**
     * @param ultimoRegistroExtraido the ultimoRegistroExtraido to set
     */
    public void setUltimoRegistroExtraido(String ultimoRegistroExtraido) {
        this.ultimoRegistroExtraido = ultimoRegistroExtraido;
    }

    /**
     * @return the fechaUltimoRegistro
     */
    public Timestamp getFechaUltimoRegistro() {
        return fechaUltimoRegistro;
    }

    /**
     * @param fechaUltimoRegistro the fechaUltimoRegistro to set
     */
    public void setFechaUltimoRegistro(Timestamp fechaUltimoRegistro) {
        this.fechaUltimoRegistro = fechaUltimoRegistro;
    }

    /**
     * @return the fechaAltaProcExtr
     */
    public Date getFechaAltaProcExtr() {
        return fechaAltaProcExtr;
    }

    /**
     * @param fechaAltaProcExtr the fechaAltaProcExtr to set
     */
    public void setFechaAltaProcExtr(Date fechaAltaProcExtr) {
        this.fechaAltaProcExtr = fechaAltaProcExtr;
    }

    /**
     * @return the usuarioAltaProcExtr
     */
    public String getUsuarioAltaProcExtr() {
        return usuarioAltaProcExtr;
    }

    /**
     * @param usuarioAltaProcExtr the usuarioAltaProcExtr to set
     */
    public void setUsuarioAltaProcExtr(String usuarioAltaProcExtr) {
        this.usuarioAltaProcExtr = usuarioAltaProcExtr;
    }

    /**
     * @return the fechaCambioProcExtr
     */
    public Date getFechaCambioProcExtr() {
        return fechaCambioProcExtr;
    }

    /**
     * @param fechaCambioProcExtr the fechaCambioProcExtr to set
     */
    public void setFechaCambioProcExtr(Date fechaCambioProcExtr) {
        this.fechaCambioProcExtr = fechaCambioProcExtr;
    }

    /**
     * @return the usuarioCambioProcExtr
     */
    public String getUsuarioCambioProcExtr() {
        return usuarioCambioProcExtr;
    }

    /**
     * @param usuarioCambioProcExtr the usuarioCambioProcExtr to set
     */
    public void setUsuarioCambioProcExtr(String usuarioCambioProcExtr) {
        this.usuarioCambioProcExtr = usuarioCambioProcExtr;
    }

}
