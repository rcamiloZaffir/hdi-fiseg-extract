package mx.interware.hdi.fiseg.extract.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import mx.interware.hdi.fiseg.extract.entity.destination.PolizaEntity;

/**
 * POJO con las propiedades de un Movimiento asociado a un evento
 * 
 * @author Interware
 * @since 2020-10-17
 *
 */
public class MovimientoEventDTO extends AbstractDTO implements Serializable {
	private static final long serialVersionUID = -5527298349199916434L;

	@JsonProperty("eventId")
	private String eventId;

	@JsonProperty("poliza")
	private PolizaEntity poliza;

	@JsonProperty("mustProcess")
	private boolean mustProcess;

	/**
	 * @return the eventId
	 */
	public String getEventId() {
		return eventId;
	}

	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	/**
	 * @return the poliza
	 */
	public PolizaEntity getPoliza() {
		return poliza;
	}

	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(PolizaEntity poliza) {
		this.poliza = poliza;
	}

	/**
	 * @return the mustProcess
	 */
	public boolean isMustProcess() {
		return mustProcess;
	}

	/**
	 * @param mustProcess the mustProcess to set
	 */
	public void setMustProcess(boolean mustProcess) {
		this.mustProcess = mustProcess;
	}

}