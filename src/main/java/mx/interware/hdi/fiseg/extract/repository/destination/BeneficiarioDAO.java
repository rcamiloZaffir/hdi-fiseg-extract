package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.interware.hdi.fiseg.extract.entity.destination.BeneficiarioEntity;
/**
 * Repository con las operaciones necesarias para el manejo de los beneficiarios
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface BeneficiarioDAO extends JpaRepository<BeneficiarioEntity, Integer> {

}
