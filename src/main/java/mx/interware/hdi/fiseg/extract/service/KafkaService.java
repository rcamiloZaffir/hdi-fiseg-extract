package mx.interware.hdi.fiseg.extract.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 * Clase que realiza toda la configuraci&oacute;n para el envio de datos a Kafka
 * 
 * @author Interware
 * @since 2020-10-17
 */
@Component
public class KafkaService {
    private static Logger log = LogManager.getLogger(KafkaService.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * Envia un evento a un determinado topico de Kafka
     * 
     * @param topicName Nombre del topico
     * @param event     Evento a enviar
     * @param eventId   Identificador del evento
     */
    public void sendEvent(String topicName, String event, String eventId) {
        log.info("Sending event [{}] to topic [{}] ...", eventId, topicName);
        kafkaTemplate.send(topicName, event);
    }

    /**
     * Envia un evento de manera asincrona a un determinado topico de Kafka
     * 
     * @param topicName Nombre del topico
     * @param event     Evento a enviar
     * @param eventId   Identificador del evento
     */

    public void sendEventAsynchronously(String topicName, String event, String eventId) {
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topicName, event);

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("Event has been successfully sent. Event: [{}], Offset: [{}]", event, result.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(Throwable ex) {
                log.error("Unable to send the event. Event: [{}], Error: {}", event, ex.getMessage());
            }
        });
    }
}