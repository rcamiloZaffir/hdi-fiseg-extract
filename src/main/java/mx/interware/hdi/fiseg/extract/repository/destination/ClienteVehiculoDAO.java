package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.interware.hdi.fiseg.extract.entity.destination.ClienteVehiculoEntity;

/**
 * Repository con las operaciones necesarias para el manejo de los clientes asociados al vehiculo
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface ClienteVehiculoDAO extends JpaRepository<ClienteVehiculoEntity, Integer> {

    @Query("SELECT cv FROM ClienteVehiculoEntity cv WHERE cv.vehiculo.vehiculoId = ?1")
    ClienteVehiculoEntity findByIdVehiculo(Long idVehiculo);

}
