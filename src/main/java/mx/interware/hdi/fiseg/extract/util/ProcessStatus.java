package mx.interware.hdi.fiseg.extract.util;

/**
 * Enumeraci&oacute;n con los posibles estados de procesamiento
 * 
 * @author Interware
 * @since 2020-10-18
 *
 */
public enum ProcessStatus {
	EXTRACTED((short) 1, "Datos extraidos"), IN_PROGRESS((short) 2, "En proceso"), SENT((short) 3, "Enviado"),
	SUCCESS((short) 4, "Exito"), ERROR_EXTRACT((short) 5, "Error de extracción"),
	ERROR_TRANSFORM((short) 6, "Error de transformación"), ERROR_SEND((short) 7, "Error de envío");

	private Short status;
	private String description;

	/**
	 * Constructor que incializa el Enum
	 * 
	 * @param status      Estatus del Enum
	 * @param description Descripcio&oacute;n
	 */
	ProcessStatus(Short status, String description) {
		this.status = status;
		this.description = description;
	}

	/**
	 * Retorna el status
	 * 
	 * @return Estatos del enum
	 */
	public Short getStatus() {
		return status;
	}

	/**
	 * Retorna la descripcio&oacute;n
	 * 
	 * @return Descripcio&oacute;n del enum
	 */
	public String getDescription() {
		return description;
	}

}