package mx.interware.hdi.fiseg.extract.util;

/**
 * Define todas las constantes utilizadas en el MS
 * 
 * @author Interware
 * @since 2020-10-17
 */
public class Constants {
    public static final Integer ESTADO_ACTIVO = 171;
    public static final String USUARIO_CAMBIO_DEFAULT = "";
    public static final String RESIDENTES_CRON_EXPRESSION = "FISEG_RESIDENTES_CRON_EXPRESSION";
    public static final String TURISTAS_CRON_EXPRESSION = "FISEG_TURISTAS_CRON_EXPRESSION";
    public static final String TIMEZONE = "FISEG_TIMEZONE";

    public static final String DS_ORIGIN_URL = "SQL_HDI_DHW_J1";
    public static final String DS_ORIGIN_DRIVER = "FISEG_DS_ORIGIN_DRIVER";
    public static final String DS_ORIGIN_SCHEMA = "FISEG_DS_ORIGIN_SCHEMA";
    public static final String DS_ORIGIN_DIALECT = "FISEG_DS_ORIGIN_DIALECT";
    public static final String DS_ORIGIN_DDL_AUTO = "FISEG_DS_ORIGIN_DDL_AUTO";
    public static final String DS_ORIGIN_ENTITY_PACKAGE = "FISEG_DS_ORIGIN_ENTITY_PACKAGE";
    public static final String DS_ORIGIN_BLOCK_SIZE = "FISEG_DS_ORIGIN_BLOCK_SIZE";

    public static final String DS_DESTINATION_URL = "SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1";
    public static final String DS_DESTINATION_DRIVER = "FISEG_DS_DESTINATION_DRIVER";
    public static final String DS_DESTINATION_SCHEMA = "FISEG_DS_DESTINATION_SCHEMA";
    public static final String DS_DESTINATION_DIALECT = "FISEG_DS_DESTINATION_DIALECT";
    public static final String DS_DESTINATION_DDL_AUTO = "FISEG_DS_DESTINATION_DDL_AUTO";
    public static final String DS_DESTINATION_ENTITY_PACKAGE = "FISEG_DS_DESTINATION_ENTITY_PACKAGE";

    public static final String KAFKA_BOOTSTRAP_ADDRESS = "FISEG_KAFKA_BOOTSTRAP_ADDRESS";
    public static final String KAFKA_EXTRACT_AUTR_TOPIC = "FISEG_KAFKA_EXTRACT_AUTR_TOPIC";
    public static final String KAFKA_EXTRACT_AUTT_TOPIC = "FISEG_KAFKA_EXTRACT_AUTT_TOPIC";
    public static final String SERVICE_LIMIT_MAX_RECORDS_AUTR = "FISEG_SERVICE_LIMIT_MAX_RECORDS_AUTR";
    public static final String SERVICE_LIMIT_MAX_RECORDS_AUTT = "FISEG_SERVICE_LIMIT_MAX_RECORDS_AUTT";
    public static final String SERVICE_DEFAULT_LOG_USER = "FISEG_DEFAULT_LOG_USER";
    public static final String SERVICE_DEFAULT_PREFIX = "SERVICE_DEFAULT_PREFIX";

    public static final short INDIVIDUAL = 1;
    public static final short FLOTILLA = 2;
    public static final short TURISTA = 3;
    
    public static final String SUCCESS = "success";
    public static final String WAITING = "waiting";
    public static final String ERROR = "error";

    /**
     * Constructor privado
     */
    private Constants() {
    }
}