package mx.interware.hdi.fiseg.extract.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * Anotaci&oacute;n usada en las entidades de persistencia para agregar elementos de una lista 
 * 
 * @author Interware
 * @since 2020-10-18
 *
 */
@Documented
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@SuppressWarnings("all")
public @interface AddItem {
	Class entity();

	String collection();
}