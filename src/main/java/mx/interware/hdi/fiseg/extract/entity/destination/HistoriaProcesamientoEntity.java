package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entidad que contiene las propiedades de un historial de procesamiento de la
 * base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutHistoriaProcesamiento", schema = "{datasources.destination.schema}")
public class HistoriaProcesamientoEntity extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = -4799317689729634546L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "HistProcesamiento_Id")
	@JsonProperty("historiaProcesamientoId")
	private Long historiaProcesamientoId;

	@Column(name = "ProcExtrac_Id")
	@JsonProperty("procesoExtraccionId")
	private Integer procesoExtraccionId;

	@Column(name = "ProcExtrac_Uuid")
	@JsonProperty("procesoExtraccionUuid")
	private String procesoExtraccionUuid;

	@Column(name = "TipoEvento")
	@JsonProperty("tipoEvento")
	private Short tipoEvento;

	@Column(name = "FechaEvento")
	@JsonProperty("fechaEvento")
	private LocalDateTime fechaEvento;

	@Column(name = "CantidadMovimientos")
	@JsonProperty("cantidadMovimiento")
	private Integer cantidadMovimiento;
	
    @Column(name = "HistProc_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaHistProc;

    @Column(name = "HistProc_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaHistProc;

    @Column(name = "HistProc_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioHistProc;

    @Column(name = "HistProc_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioHistProc;


	/**
	 * @return the historiaProcesamientoId
	 */
	public Long getHistoriaProcesamientoId() {
		return historiaProcesamientoId;
	}

	/**
	 * @param historiaProcesamientoId the historiaProcesamientoId to set
	 */
	public void setHistoriaProcesamientoId(Long historiaProcesamientoId) {
		this.historiaProcesamientoId = historiaProcesamientoId;
	}

	/**
	 * @return the procesoExtraccionId
	 */
	public Integer getProcesoExtraccionId() {
		return procesoExtraccionId;
	}

	/**
	 * @param procesoExtraccionId the procesoExtraccionId to set
	 */
	public void setProcesoExtraccionId(Integer procesoExtraccionId) {
		this.procesoExtraccionId = procesoExtraccionId;
	}

	/**
	 * @return the procesoExtraccionUuid
	 */
	public String getProcesoExtraccionUuid() {
		return procesoExtraccionUuid;
	}

	/**
	 * @param procesoExtraccionUuid the procesoExtraccionUuid to set
	 */
	public void setProcesoExtraccionUuid(String procesoExtraccionUuid) {
		this.procesoExtraccionUuid = procesoExtraccionUuid;
	}

	/**
	 * @return the tipoEvento
	 */
	public Short getTipoEvento() {
		return tipoEvento;
	}

	/**
	 * @param tipoEvento the tipoEvento to set
	 */
	public void setTipoEvento(Short tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	/**
	 * @return the fechaEvento
	 */
	public LocalDateTime getFechaEvento() {
		return fechaEvento;
	}

	/**
	 * @param fechaEvento the fechaEvento to set
	 */
	public void setFechaEvento(LocalDateTime fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	/**
	 * @return the cantidadMovimiento
	 */
	public Integer getCantidadMovimiento() {
		return cantidadMovimiento;
	}

	/**
	 * @param cantidadMovimiento the cantidadMovimiento to set
	 */
	public void setCantidadMovimiento(Integer cantidadMovimiento) {
		this.cantidadMovimiento = cantidadMovimiento;
	}

    /**
     * @return the fechaAltaHistProc
     */
    public Date getFechaAltaHistProc() {
        return fechaAltaHistProc;
    }

    /**
     * @param fechaAltaHistProc the fechaAltaHistProc to set
     */
    public void setFechaAltaHistProc(Date fechaAltaHistProc) {
        this.fechaAltaHistProc = fechaAltaHistProc;
    }

    /**
     * @return the usuarioAltaHistProc
     */
    public String getUsuarioAltaHistProc() {
        return usuarioAltaHistProc;
    }

    /**
     * @param usuarioAltaHistProc the usuarioAltaHistProc to set
     */
    public void setUsuarioAltaHistProc(String usuarioAltaHistProc) {
        this.usuarioAltaHistProc = usuarioAltaHistProc;
    }

    /**
     * @return the fechaCambioHistProc
     */
    public Date getFechaCambioHistProc() {
        return fechaCambioHistProc;
    }

    /**
     * @param fechaCambioHistProc the fechaCambioHistProc to set
     */
    public void setFechaCambioHistProc(Date fechaCambioHistProc) {
        this.fechaCambioHistProc = fechaCambioHistProc;
    }

    /**
     * @return the usuarioCambioHistProc
     */
    public String getUsuarioCambioHistProc() {
        return usuarioCambioHistProc;
    }

    /**
     * @param usuarioCambioHistProc the usuarioCambioHistProc to set
     */
    public void setUsuarioCambioHistProc(String usuarioCambioHistProc) {
        this.usuarioCambioHistProc = usuarioCambioHistProc;
    }
	

}
