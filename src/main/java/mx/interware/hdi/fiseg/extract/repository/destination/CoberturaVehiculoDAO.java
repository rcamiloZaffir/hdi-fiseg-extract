package mx.interware.hdi.fiseg.extract.repository.destination;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.interware.hdi.fiseg.extract.entity.destination.CoberturaVehiculoEntity;

/**
 * Repository con las operaciones necesarias para el manejo de las coberturas de los vehiculos
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface CoberturaVehiculoDAO extends JpaRepository<CoberturaVehiculoEntity, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE CoberturaVehiculoEntity cve SET cve.tabEstatus =173   WHERE cve.vehiculo.vehiculoId=:idVehiculo")
    int deleteByIdVehiculo(@Param("idVehiculo") Long idVehiculo);
}
