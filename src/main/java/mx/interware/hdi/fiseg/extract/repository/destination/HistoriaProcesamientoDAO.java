package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.interware.hdi.fiseg.extract.entity.destination.HistoriaProcesamientoEntity;

/**
 * Repository con las operaciones necesarias para el manejo de las entidades
 * para el hist&oacute;rico de procesamiento
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface HistoriaProcesamientoDAO extends JpaRepository<HistoriaProcesamientoEntity, Long> {

	/**
	 * Obtiene el hist&oacute;rico del procesamiento a partir del tipo evento
	 * 
	 * @param tipoEvento Tipo evento para la b&uacute;squeda
	 * @return Entidad encontrada
	 */
	@Query("SELECT h FROM HistoriaProcesamientoEntity h WHERE tipoEvento = ?1")
	public HistoriaProcesamientoEntity findByTipoEvento(Short tipoEvento);

}
