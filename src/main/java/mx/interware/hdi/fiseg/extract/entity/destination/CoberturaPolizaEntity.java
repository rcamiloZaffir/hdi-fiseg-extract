package mx.interware.hdi.fiseg.extract.entity.destination;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Entidad que contiene las propiedades de un cobertura asociada a una poliza de la base de datos
 * 
 * @author Interware
 * @since 2020-10-19
 *
 */
@Entity
@Table(name = "Tb_Mov_AutCoberturaPoliza", schema = "{datasources.destination.schema}")
public class CoberturaPolizaEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 2837411485272025554L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CobPol_Id")
    @JsonProperty("coberturaPolizaId")
    private Long coberturaPolizaId;

    @Column(name = "Cob_Cve")
    @JsonProperty("coberturaCve")
    private Short coberturaCve;

    @Column(name = "SumaAseguradaCve")
    @JsonProperty("sumaAseguradaCve")
    private Short sumaAseguradaCve;

    @Column(name = "SumaAsegurada")
    @JsonProperty("sumaAsegurada")
    private BigDecimal sumaAsegurada;

    @Column(name = "Estatus")
    @JsonProperty("estatus")
    private Short estatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MovPol_Id")
    private MovimientoPolizaEntity movimientoPoliza;

    @Column(name = "CobPol_FechaAlta")
    @JsonProperty("fechaAlta")
    private Date fechaAltaCobPol;

    @Column(name = "CobPol_UsuarioAlta")
    @JsonProperty("usuarioAlta")
    private String usuarioAltaCobPol;

    @Column(name = "CobPol_FechaCambio", nullable = false)
    @JsonProperty("fechaCambio")
    private Date fechaCambioCobPol;

    @Column(name = "CobPol_UsuarioCambio", nullable = false)
    @JsonProperty("usuarioCambio")
    private String usuarioCambioCobPol;

    /**
     * @return the coberturaPolizaId
     */
    public Long getCoberturaPolizaId() {
        return coberturaPolizaId;
    }

    /**
     * @param coberturaPolizaId the coberturaPolizaId to set
     */
    public void setCoberturaPolizaId(Long coberturaPolizaId) {
        this.coberturaPolizaId = coberturaPolizaId;
    }

    /**
     * @return the coberturaCve
     */
    public Short getCoberturaCve() {
        return coberturaCve;
    }

    /**
     * @param coberturaCve the coberturaCve to set
     */
    public void setCoberturaCve(Short coberturaCve) {
        this.coberturaCve = coberturaCve;
    }

    /**
     * @return the sumaAseguradaCve
     */
    public Short getSumaAseguradaCve() {
        return sumaAseguradaCve;
    }

    /**
     * @param sumaAseguradaCve the sumaAseguradaCve to set
     */
    public void setSumaAseguradaCve(Short sumaAseguradaCve) {
        this.sumaAseguradaCve = sumaAseguradaCve;
    }

    /**
     * @return the sumaAsegurada
     */
    public BigDecimal getSumaAsegurada() {
        return sumaAsegurada;
    }

    /**
     * @param sumaAsegurada the sumaAsegurada to set
     */
    public void setSumaAsegurada(BigDecimal sumaAsegurada) {
        this.sumaAsegurada = sumaAsegurada;
    }

    /**
     * @return the estatus
     */
    public Short getEstatus() {
        return estatus;
    }

    /**
     * @param estatus the estatus to set
     */
    public void setEstatus(Short estatus) {
        this.estatus = estatus;
    }

    /**
     * @return the movimientoPoliza
     */
    public MovimientoPolizaEntity getMovimientoPoliza() {
        return movimientoPoliza;
    }

    /**
     * @param movimientoPoliza the movimientoPoliza to set
     */
    public void setMovimientoPoliza(MovimientoPolizaEntity movimientoPoliza) {
        this.movimientoPoliza = movimientoPoliza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CoberturaPolizaEntity)) {
            return false;
        }

        Long id = ((CoberturaPolizaEntity) o).getCoberturaPolizaId();
        return (coberturaPolizaId != null) && (coberturaPolizaId.equals(id));
    }

    @Override
    public int hashCode() {
        return 775;
    }

    /**
     * @return the fechaAltaCobPol
     */
    public Date getFechaAltaCobPol() {
        return fechaAltaCobPol;
    }

    /**
     * @param fechaAltaCobPol the fechaAltaCobPol to set
     */
    public void setFechaAltaCobPol(Date fechaAltaCobPol) {
        this.fechaAltaCobPol = fechaAltaCobPol;
    }

    /**
     * @return the usuarioAltaCobPol
     */
    public String getUsuarioAltaCobPol() {
        return usuarioAltaCobPol;
    }

    /**
     * @param usuarioAltaCobPol the usuarioAltaCobPol to set
     */
    public void setUsuarioAltaCobPol(String usuarioAltaCobPol) {
        this.usuarioAltaCobPol = usuarioAltaCobPol;
    }

    /**
     * @return the fechaCambioCobPol
     */
    public Date getFechaCambioCobPol() {
        return fechaCambioCobPol;
    }

    /**
     * @param fechaCambioCobPol the fechaCambioCobPol to set
     */
    public void setFechaCambioCobPol(Date fechaCambioCobPol) {
        this.fechaCambioCobPol = fechaCambioCobPol;
    }

    /**
     * @return the usuarioCambioCobPol
     */
    public String getUsuarioCambioCobPol() {
        return usuarioCambioCobPol;
    }

    /**
     * @param usuarioCambioCobPol the usuarioCambioCobPol to set
     */
    public void setUsuarioCambioCobPol(String usuarioCambioCobPol) {
        this.usuarioCambioCobPol = usuarioCambioCobPol;
    }

}
