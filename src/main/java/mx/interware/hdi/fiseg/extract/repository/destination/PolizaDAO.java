package mx.interware.hdi.fiseg.extract.repository.destination;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.interware.hdi.fiseg.extract.entity.destination.PolizaEntity;

/**
 * Repository con las operaciones necesarias para el manejo de las polizas
 * 
 * @author Interware
 * @since 2020-10-18
 */
public interface PolizaDAO extends JpaRepository<PolizaEntity, Long> {
	/**
	 * Obtiene una poliza a partir del identificador compuesto
	 * 
	 * @param polizaStr Identificador para la b&uacute;squeda
	 * @return Poliza encontrada
	 */
	@Query("SELECT p FROM PolizaEntity p WHERE polizaStr = ?1")
	public PolizaEntity findByPolizaStr(String polizaStr);
}
