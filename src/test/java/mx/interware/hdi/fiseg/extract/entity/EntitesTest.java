package mx.interware.hdi.fiseg.extract.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import mx.interware.hdi.fiseg.extract.entity.destination.BeneficiarioEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.ClientePolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.ClienteVehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.CoberturaPolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.CoberturaVehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.HistoriaProcesamientoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.MovimientoPolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.PolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.ProcesoExtraccionEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.VehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.CoberturaDWH;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoDWH;

public class EntitesTest {

    private final BigDecimal NEW_BIG_DECIMAL = new BigDecimal(100L);
    private final Short SHORT_NUMBER = 1;
    private final Long LONG_NUMBER = 1L;
    private final Integer INTEGER_NUMBER = 1;
    private final Integer INTEGER_NUMBER_02 = 2;
    private final Integer INTEGER_NUMBER_03 = 3;
    private final Integer INTEGER_NUMBER_04 = 4;
    private final Integer INTEGER_NUMBER_05 = 5;
    private final Date DATE_ONE = new Date();
    private final Date DATE_TWO = new Date();
    private final Date DATE_THREE = new Date();
    private final Date DATE_FOUR = new Date();
    private final Timestamp TIMESTAMP_ONE = new Timestamp(10000);
    private final String STRING_ONE = "XYZ";
    private final String STRING_TWO = "abc01";
    private final String STRING_THREE = "uoiea01";
    private final String STRING_FOUR = "9ijn8uhb";

    @Test
    public void testCoberturaVehiculoEntity() {
        VehiculoEntity ve = new VehiculoEntity();

        CoberturaVehiculoEntity cbe = new CoberturaVehiculoEntity();
        cbe.setCoberturaVehiculoId(LONG_NUMBER);
        cbe.setCoberturaCveCobVehi(SHORT_NUMBER);
        cbe.setSumaAseguradaCveCobVehi(SHORT_NUMBER);
        cbe.setSumaAseguradaCobVehi(NEW_BIG_DECIMAL);
        cbe.setEstatusCobVehi(SHORT_NUMBER);
        cbe.setVehiculo(ve);

        Assert.assertEquals(cbe.getCoberturaVehiculoId(), LONG_NUMBER);
        Assert.assertEquals(cbe.getCoberturaCveCobVehi(), SHORT_NUMBER);
        Assert.assertEquals(cbe.getSumaAseguradaCveCobVehi(), SHORT_NUMBER);
        Assert.assertEquals(cbe.getSumaAseguradaCobVehi(), NEW_BIG_DECIMAL);
        Assert.assertEquals(cbe.getEstatusCobVehi(), SHORT_NUMBER);
        Assert.assertEquals(cbe.getVehiculo(), ve);
        Assert.assertNotEquals(cbe, null);
        Assert.assertNotEquals(cbe, new CoberturaVehiculoEntity());
        Assert.assertEquals(cbe, cbe);

    }

    @Test
    public void testClienteVehiculoEntity() {
        VehiculoEntity ve = new VehiculoEntity();

        ClienteVehiculoEntity cve = new ClienteVehiculoEntity();
        cve.setClienteVehiculoId(LONG_NUMBER);
        cve.setApellidoPaternoCte("");
        cve.setApellidoMaternoCte("");
        cve.setNombreCte("");
        cve.setVehiculo(ve);

        Assert.assertEquals(cve.getClienteVehiculoId(), LONG_NUMBER);
        Assert.assertEquals(cve.getApellidoPaternoCte(), "");
        Assert.assertEquals(cve.getApellidoMaternoCte(), "");
        Assert.assertEquals(cve.getNombreCte(), "");
        Assert.assertEquals(cve.getVehiculo(), ve);
        Assert.assertNotEquals(cve, null);
        Assert.assertNotEquals(cve, new ClienteVehiculoEntity());
        Assert.assertEquals(cve, cve);

    }

    @Test
    public void testBeneficiarioEntity() {
        VehiculoEntity ve = new VehiculoEntity();

        BeneficiarioEntity be = new BeneficiarioEntity();
        be.setBeneficiarioId(LONG_NUMBER);
        be.setApellidoPaternoBenef("");
        be.setApellidoMaternoBenef("");
        be.setNombreBenef("");
        be.setRfcBenef("");
        be.setCurpBenef("");
        be.setTipoPersonaBenef("");
        be.setVehiculo(ve);

        Assert.assertEquals(be.getBeneficiarioId(), LONG_NUMBER);
        Assert.assertEquals(be.getApellidoPaternoBenef(), "");
        Assert.assertEquals(be.getApellidoMaternoBenef(), "");
        Assert.assertEquals(be.getNombreBenef(), "");
        Assert.assertEquals(be.getRfcBenef(), "");
        Assert.assertEquals(be.getCurpBenef(), "");
        Assert.assertEquals(be.getTipoPersonaBenef(), "");
        Assert.assertEquals(be.getVehiculo(), ve);
        Assert.assertNotEquals(be, null);
        Assert.assertNotEquals(be, new BeneficiarioEntity());
        Assert.assertEquals(be, be);
    }

    @Test
    public void testVehiculoEntity() {

        BeneficiarioEntity be = new BeneficiarioEntity();
        be.setBeneficiarioId(1L);
        ClienteVehiculoEntity ce = new ClienteVehiculoEntity();
        ce.setClienteVehiculoId(1L);
        CoberturaVehiculoEntity cb = new CoberturaVehiculoEntity();
        cb.setCoberturaVehiculoId(1L);
        VehiculoEntity ve = new VehiculoEntity();
        MovimientoPolizaEntity me = new MovimientoPolizaEntity();
        ve.setVehiculoId(LONG_NUMBER);
        ve.setUso(INTEGER_NUMBER);
        ve.setVin("");
        ve.setObligatorio(INTEGER_NUMBER);
        ve.setModelo(INTEGER_NUMBER);
        ve.setPlaca("");
        ve.setInciso("");
        ve.setMarcaTipo(INTEGER_NUMBER);
        ve.setUbicacionAsegurado(INTEGER_NUMBER);
        ve.setTipoSeguro(INTEGER_NUMBER);
        ve.setMovimientoPoliza(me);

        Assert.assertEquals(ve.getVehiculoId(), LONG_NUMBER);
        Assert.assertEquals(ve.getUso(), INTEGER_NUMBER);
        Assert.assertEquals(ve.getVin(), "");
        Assert.assertEquals(ve.getObligatorio(), INTEGER_NUMBER);
        Assert.assertEquals(ve.getModelo(), INTEGER_NUMBER);
        Assert.assertEquals(ve.getPlaca(), "");
        Assert.assertEquals(ve.getInciso(), "");
        Assert.assertEquals(ve.getMarcaTipo(), INTEGER_NUMBER);
        Assert.assertEquals(ve.getUbicacionAsegurado(), INTEGER_NUMBER);
        Assert.assertEquals(ve.getMovimientoPoliza(), me);
        ve.addBeneficiario(be);
        Assert.assertEquals(ve.getBeneficiarios().size(), 1);
        ve.removeBeneficiario(be);
        Assert.assertEquals(ve.getBeneficiarios().size(), 0);
        ve.addCliente(ce);
        Assert.assertEquals(ve.getClientes().size(), 1);
        ve.removeCliente(ce);
        Assert.assertEquals(ve.getClientes().size(), 0);
        ve.addCobertura(cb);
        Assert.assertEquals(ve.getCoberturas().size(), 1);
        ve.removeCobertura(cb);
        Assert.assertEquals(ve.getCoberturas().size(), 0);

        Assert.assertNotEquals(ve, null);
        Assert.assertNotEquals(ve, new VehiculoEntity());
        Assert.assertEquals(ve, ve);
    }

    @Test
    public void testClientePolizaEntity() {
        MovimientoPolizaEntity movimientoPoliza = new MovimientoPolizaEntity();
        ClientePolizaEntity cpe = new ClientePolizaEntity();
        cpe.setApellidoMaterno("");
        cpe.setApellidoPaterno("");
        cpe.setClientePolizaId(LONG_NUMBER);
        cpe.setCurp("");
        cpe.setNombre("");
        cpe.setRfc("");
        cpe.setMovimientoPoliza(movimientoPoliza);
        Assert.assertEquals(cpe.getApellidoMaterno(), "");
        Assert.assertEquals(cpe.getApellidoPaterno(), "");
        Assert.assertEquals(cpe.getClientePolizaId(), LONG_NUMBER);
        Assert.assertEquals(cpe.getCurp(), "");
        Assert.assertEquals(cpe.getNombre(), "");
        Assert.assertEquals(cpe.getRfc(), "");
        Assert.assertEquals(cpe.getMovimientoPoliza(), movimientoPoliza);

        Assert.assertNotEquals(cpe, null);
        Assert.assertNotEquals(cpe, new ClientePolizaEntity());
        Assert.assertNotEquals(cpe, new MovimientoPolizaEntity());
        Assert.assertEquals(cpe, cpe);
        Assert.assertNotNull(cpe.toString());

    }

    @Test
    public void testCoberturaPolizaEntity() {
        MovimientoPolizaEntity movimientoPoliza = new MovimientoPolizaEntity();
        CoberturaPolizaEntity cpole = new CoberturaPolizaEntity();

        cpole.setCoberturaCve(SHORT_NUMBER);
        cpole.setCoberturaPolizaId(LONG_NUMBER);
        cpole.setEstatus(SHORT_NUMBER);
        cpole.setSumaAsegurada(NEW_BIG_DECIMAL);
        cpole.setSumaAseguradaCve(SHORT_NUMBER);
        cpole.setMovimientoPoliza(movimientoPoliza);
        Assert.assertEquals(cpole.getCoberturaCve(), SHORT_NUMBER);
        Assert.assertEquals(cpole.getCoberturaPolizaId(), LONG_NUMBER);
        Assert.assertEquals(cpole.getEstatus(), SHORT_NUMBER);
        Assert.assertEquals(cpole.getSumaAsegurada(), NEW_BIG_DECIMAL);
        Assert.assertEquals(cpole.getSumaAseguradaCve(), SHORT_NUMBER);
        Assert.assertEquals(cpole.getMovimientoPoliza(), movimientoPoliza);

        Assert.assertNotEquals(cpole, null);
        Assert.assertNotEquals(cpole, new MovimientoPolizaEntity());
        Assert.assertNotEquals(cpole, new CoberturaVehiculoEntity());
        Assert.assertEquals(cpole, cpole);

    }

    @Test
    public void testMovimientoPolizaEntity() {
        PolizaEntity pe = new PolizaEntity();
        VehiculoEntity ve = new VehiculoEntity();
        ve.setVehiculoId(1L);
        ClientePolizaEntity cp = new ClientePolizaEntity();
        cp.setClientePolizaId(1L);
        CoberturaPolizaEntity cb = new CoberturaPolizaEntity();
        cb.setCoberturaPolizaId(1L);

        MovimientoPolizaEntity movimiento = new MovimientoPolizaEntity();
        movimiento.setMovimientoPolizaId(LONG_NUMBER);
        movimiento.setMovimientoPolizaStr("XAQWSZ");
        movimiento.setFechaMovimiento(DATE_ONE);
        movimiento.setNumeroDocumento(INTEGER_NUMBER);
        movimiento.setTipoMovimiento(SHORT_NUMBER);
        movimiento.setTipoPoliza(SHORT_NUMBER);
        movimiento.setNumeroVehiculo(INTEGER_NUMBER);
        movimiento.setEstatusProcesamiento(SHORT_NUMBER);
        movimiento.setMensajeProcesamiento("");
        movimiento.setFechacaptura(DATE_ONE);
        movimiento.setFechaEmision(DATE_ONE);
        movimiento.setFechaCancelacion(DATE_ONE);
        movimiento.setFechacaptura(DATE_THREE);
        movimiento.setFechaFinalVigencia(DATE_FOUR);
        movimiento.setPoliza(pe);

        Assert.assertEquals(movimiento.getMovimientoPolizaId(), LONG_NUMBER);
        Assert.assertEquals(movimiento.getMovimientoPolizaStr(), "XAQWSZ");
        Assert.assertEquals(movimiento.getFechaMovimiento(), DATE_ONE);
        Assert.assertEquals(movimiento.getNumeroDocumento(), INTEGER_NUMBER);
        Assert.assertEquals(movimiento.getTipoMovimiento(), SHORT_NUMBER);
        Assert.assertEquals(movimiento.getTipoPoliza(), SHORT_NUMBER);
        Assert.assertEquals(movimiento.getNumeroVehiculo(), INTEGER_NUMBER);
        Assert.assertEquals(movimiento.getEstatusProcesamiento(), SHORT_NUMBER);
        Assert.assertEquals(movimiento.getMensajeProcesamiento(), "");
        Assert.assertEquals(movimiento.getFechacaptura(), DATE_ONE);
        Assert.assertEquals(movimiento.getFechaEmision(), DATE_ONE);
        Assert.assertEquals(movimiento.getFechaCancelacion(), DATE_ONE);
        Assert.assertEquals(movimiento.getFechacaptura(), DATE_THREE);
        Assert.assertEquals(movimiento.getFechaFinalVigencia(), DATE_FOUR);
        Assert.assertEquals(movimiento.getPoliza(), pe);

        movimiento.addVehiculo(ve);
        Assert.assertEquals(movimiento.getVehiculos().size(), 1);
        movimiento.removeVehiculo(ve);
        Assert.assertEquals(movimiento.getVehiculos().size(), 0);

        movimiento.addClientePoliza(cp);
        Assert.assertEquals(movimiento.getClientes().size(), 1);
        movimiento.removeClientePoliza(cp);
        Assert.assertEquals(movimiento.getClientes().size(), 0);

        movimiento.addCoberturaPoliza(cb);
        Assert.assertEquals(movimiento.getCoberturas().size(), 1);
        movimiento.removeCoberturaPoliza(cb);
        Assert.assertEquals(movimiento.getCoberturas().size(), 0);

        Assert.assertNotEquals(movimiento, null);
        Assert.assertNotEquals(movimiento, new CoberturaVehiculoEntity());
        Assert.assertNotEquals(movimiento, new MovimientoPolizaEntity());
        Assert.assertEquals(movimiento, movimiento);
    }

    @Test
    public void testPolizaEntity() {
        MovimientoPolizaEntity mpe = new MovimientoPolizaEntity();
        mpe.setMovimientoPolizaId(1L);

        PolizaEntity poliza = new PolizaEntity();
        poliza.setPolizaId(LONG_NUMBER);
        poliza.setAgenciaId("003");
        poliza.setPolizaStr("");
        poliza.setNumeroPoliza("");
        poliza.setNumeroCotizacion("");
        poliza.setCanalVenta(SHORT_NUMBER);
        poliza.setNumeroReferencia(LONG_NUMBER);
        poliza.setUbicacionEmision(INTEGER_NUMBER);
        poliza.setLineaNegocio("");
        poliza.setAgenteId("");

        Assert.assertEquals(poliza.getPolizaId(), LONG_NUMBER);
        Assert.assertEquals(poliza.getAgenciaId(), "003");
        Assert.assertEquals(poliza.getPolizaStr(), "");
        Assert.assertEquals(poliza.getNumeroPoliza(), "");
        Assert.assertEquals(poliza.getNumeroCotizacion(), "");
        Assert.assertEquals(poliza.getCanalVenta(), SHORT_NUMBER);
        Assert.assertEquals(poliza.getNumeroReferencia(), LONG_NUMBER);
        Assert.assertEquals(poliza.getUbicacionEmision(), INTEGER_NUMBER);
        Assert.assertEquals(poliza.getLineaNegocio(), "");
        Assert.assertEquals(poliza.getAgenteId(), "");

        poliza.addMovimiento(mpe);
        Assert.assertEquals(poliza.getMovimientos().size(), 1);
        poliza.removeMovimiento(mpe);
        Assert.assertEquals(poliza.getMovimientos().size(), 0);

        Assert.assertNotEquals(poliza, null);
        Assert.assertNotEquals(poliza, new CoberturaVehiculoEntity());
        Assert.assertNotEquals(poliza, new PolizaEntity());
        Assert.assertEquals(poliza, poliza);
    }

    @Test
    public void testProcesoExtraccionEntity() {
        LocalDateTime ldt = LocalDateTime.now();

        ProcesoExtraccionEntity pee = new ProcesoExtraccionEntity();
        pee.setDescripcion("");
        pee.setFechaExtraccion(ldt);
        pee.setFechaUltimoRegistro(TIMESTAMP_ONE);
        pee.setProcesoExtraccionId(INTEGER_NUMBER);
        pee.setTipoProcesamiento("");
        pee.setUltimoRegistroExtraido("");

        pee.setUsuarioAltaProcExtr(STRING_ONE);
        pee.setUsuarioCambioProcExtr(STRING_TWO);
        pee.setFechaAltaProcExtr(DATE_ONE);
        pee.setFechaCambioProcExtr(DATE_TWO);

        Assert.assertEquals(pee.getDescripcion(), "");
        Assert.assertEquals(pee.getFechaExtraccion(), ldt);
        Assert.assertEquals(pee.getFechaUltimoRegistro(), TIMESTAMP_ONE);
        Assert.assertEquals(pee.getProcesoExtraccionId(), INTEGER_NUMBER);
        Assert.assertEquals(pee.getTipoProcesamiento(), "");
        Assert.assertEquals(pee.getUltimoRegistroExtraido(), "");

        Assert.assertEquals(pee.getUsuarioAltaProcExtr(), STRING_ONE);
        Assert.assertEquals(pee.getUsuarioCambioProcExtr(), STRING_TWO);
        Assert.assertEquals(pee.getFechaAltaProcExtr(), DATE_ONE);
        Assert.assertEquals(pee.getFechaCambioProcExtr(), DATE_TWO);

        Assert.assertNotEquals(pee, null);
        Assert.assertNotEquals(pee, new CoberturaVehiculoEntity());
        Assert.assertNotEquals(pee, new ProcesoExtraccionEntity());
        Assert.assertEquals(pee, pee);

    }

    @Test
    public void testHistoriaProcesamientoEntity() {
        LocalDateTime ldt = LocalDateTime.now();

        HistoriaProcesamientoEntity hpe = new HistoriaProcesamientoEntity();
        hpe.setCantidadMovimiento(INTEGER_NUMBER);
        hpe.setFechaEvento(ldt);
        hpe.setHistoriaProcesamientoId(LONG_NUMBER);
        hpe.setProcesoExtraccionId(INTEGER_NUMBER);
        hpe.setProcesoExtraccionUuid("");
        hpe.setTipoEvento(SHORT_NUMBER);

        hpe.setUsuarioAltaHistProc(STRING_ONE);
        hpe.setUsuarioCambioHistProc(STRING_TWO);
        hpe.setFechaAltaHistProc(DATE_ONE);
        hpe.setFechaCambioHistProc(DATE_TWO);

        Assert.assertEquals(hpe.getCantidadMovimiento(), INTEGER_NUMBER);
        Assert.assertEquals(hpe.getFechaEvento(), ldt);
        Assert.assertEquals(hpe.getHistoriaProcesamientoId(), LONG_NUMBER);
        Assert.assertEquals(hpe.getProcesoExtraccionId(), INTEGER_NUMBER);
        Assert.assertEquals(hpe.getProcesoExtraccionUuid(), "");
        Assert.assertEquals(hpe.getTipoEvento(), SHORT_NUMBER);

        Assert.assertEquals(hpe.getUsuarioAltaHistProc(), STRING_ONE);
        Assert.assertEquals(hpe.getUsuarioCambioHistProc(), STRING_TWO);
        Assert.assertEquals(hpe.getFechaAltaHistProc(), DATE_ONE);
        Assert.assertEquals(hpe.getFechaCambioHistProc(), DATE_TWO);

        Assert.assertNotEquals(hpe, null);
        Assert.assertNotEquals(hpe, new CoberturaVehiculoEntity());
        Assert.assertNotEquals(hpe, new HistoriaProcesamientoEntity());
        Assert.assertEquals(hpe, hpe);
    }

    @Test
    public void testMovimientoDWHEntity() {
        MovimientoDWH entity = new MovimientoDWH();
        entity.setApellido1(STRING_ONE);
        entity.setApellido2(STRING_TWO);
        entity.setCodigoPostal(STRING_THREE);
        entity.setDescCanalVenta(STRING_FOUR);
        entity.setDescCausaCancelacion("Causa");
        entity.setDescGrupoPaquete("GRU");
        entity.setDescPaquete("Paquete");
        entity.setDescTipoDocumento("1");
        entity.setDescUsoCNSF("Privado");
        entity.setFechaEmision(TIMESTAMP_ONE);
        entity.setFechaInicioVigencia(DATE_TWO);
        entity.setFechaFinVigencia(DATE_THREE);
        entity.setIdCanalVenta(SHORT_NUMBER);
//        entity.setTiempoUltimoCambio(TIMESTAMP_ONE);

        entity.setNumReferencia(STRING_ONE);
        entity.setNumeroSerie(STRING_TWO);
        entity.setIdMarcaVehiculo(INTEGER_NUMBER);
        entity.setTipoPersona(STRING_THREE);
        entity.setNombre(STRING_FOUR);
        entity.setRazonSocial(STRING_ONE);
        entity.setYGuionO(STRING_THREE);
        entity.setNombrePerfilAgente(STRING_FOUR);
        entity.setIdCausaCancelacion(INTEGER_NUMBER);
        entity.setRfc(STRING_ONE);
        entity.setNombrePais(STRING_THREE);
        entity.setNombreMunicipio(STRING_FOUR);
        entity.setNombreEstado(STRING_ONE);
        entity.setNombreCiudad(STRING_THREE);
        entity.setIdEstadoCliente(SHORT_NUMBER);
        entity.setIdMarcaTipoOcra(INTEGER_NUMBER);
        entity.setIdUsoCNSF(INTEGER_NUMBER);

        entity.setCuentaConSeguroObligatorio(INTEGER_NUMBER);
        entity.setIdClienteEntidad(INTEGER_NUMBER_02);
        entity.setIdClienteMunicipio(INTEGER_NUMBER_03);
        entity.setIdTipoSeguro(INTEGER_NUMBER_04);
        entity.setIdTipoSumaAsegurada(INTEGER_NUMBER_05);
        entity.setIdGrupoDocumento(INTEGER_NUMBER);
        entity.setDescGrupoDocumento("CANCELACION");

        Assert.assertEquals(entity.getApellido1(), STRING_ONE);
        Assert.assertEquals(entity.getApellido2(), STRING_TWO);
        Assert.assertEquals(entity.getCodigoPostal(), STRING_THREE);
        Assert.assertEquals(entity.getDescCanalVenta(), STRING_FOUR);
        Assert.assertEquals(entity.getDescCausaCancelacion(), "Causa");
        Assert.assertEquals(entity.getDescGrupoPaquete(), "GRU");
        Assert.assertEquals(entity.getDescPaquete(), "Paquete");
        Assert.assertEquals(entity.getDescTipoDocumento(), "1");
        Assert.assertEquals(entity.getDescUsoCNSF(), "Privado");
        Assert.assertEquals(entity.getFechaInicioVigencia(), DATE_TWO);
        Assert.assertEquals(entity.getFechaFinVigencia(), DATE_THREE);
        Assert.assertEquals(entity.getIdCanalVenta(), SHORT_NUMBER);
        Assert.assertEquals(entity.getNumReferencia(), STRING_ONE);
        Assert.assertEquals(entity.getNumeroSerie(), STRING_TWO);
        Assert.assertEquals(entity.getIdMarcaVehiculo(), INTEGER_NUMBER);
        Assert.assertEquals(entity.getTipoPersona(), STRING_THREE);
        Assert.assertEquals(entity.getNombre(), STRING_FOUR);
        Assert.assertEquals(entity.getRazonSocial(), STRING_ONE);
        Assert.assertEquals(entity.getYGuionO(), STRING_THREE);
        Assert.assertEquals(entity.getNombrePerfilAgente(), STRING_FOUR);
        Assert.assertEquals(entity.getIdCausaCancelacion(), INTEGER_NUMBER);
        Assert.assertEquals(entity.getRfc(), STRING_ONE);
        Assert.assertEquals(entity.getNombrePais(), STRING_THREE);
        Assert.assertEquals(entity.getNombreMunicipio(), STRING_FOUR);
        Assert.assertEquals(entity.getNombreEstado(), STRING_ONE);
        Assert.assertEquals(entity.getNombreCiudad(), STRING_THREE);
        Assert.assertEquals(entity.getIdEstadoCliente(), SHORT_NUMBER);
        Assert.assertEquals(entity.getIdMarcaTipoOcra(), INTEGER_NUMBER);
//        Assert.assertEquals(entity.getTiempoUltimoCambio(), TIMESTAMP_ONE);
        Assert.assertEquals(entity.getIdUsoCNSF(), INTEGER_NUMBER);
        Assert.assertEquals(entity.getCuentaConSeguroObligatorio(), INTEGER_NUMBER);
        Assert.assertEquals(entity.getIdClienteEntidad(), INTEGER_NUMBER_02);
        Assert.assertEquals(entity.getIdClienteMunicipio(), INTEGER_NUMBER_03);
        Assert.assertEquals(entity.getIdTipoSeguro(), INTEGER_NUMBER_04);
        Assert.assertEquals(entity.getIdTipoSumaAsegurada(), INTEGER_NUMBER_05);
        Assert.assertEquals(entity.getIdGrupoDocumento(), INTEGER_NUMBER);
        Assert.assertEquals(entity.getDescGrupoDocumento(), "CANCELACION");
    }

    @Test
    public void test_CoberturaDWH() {
        CoberturaDWH entity = new CoberturaDWH();
        entity.setFechaTransaccion(INTEGER_NUMBER);
        entity.setNumDocumento(SHORT_NUMBER);
        entity.setIdOficina(SHORT_NUMBER);
        entity.setNumPoliza(LONG_NUMBER);
        entity.setNumCertificado(STRING_ONE);
        entity.setIdReglaCobertura(SHORT_NUMBER);
        entity.setIdCobertura(SHORT_NUMBER);
        entity.setIdCoberturaRR8(SHORT_NUMBER);
        entity.setDescCobertura(STRING_TWO);
        entity.setSumaAsegurada(NEW_BIG_DECIMAL);
        entity.setNumCotizacion(INTEGER_NUMBER);
        entity.setIdEstatusCobertura(SHORT_NUMBER);

        Assert.assertEquals(entity.getFechaTransaccion(), INTEGER_NUMBER);
        Assert.assertEquals(entity.getNumDocumento(), SHORT_NUMBER);
        Assert.assertEquals(entity.getIdOficina(), SHORT_NUMBER);
        Assert.assertEquals(entity.getNumPoliza(), LONG_NUMBER);
        Assert.assertEquals(entity.getNumCertificado(), STRING_ONE);
        Assert.assertEquals(entity.getIdReglaCobertura(), SHORT_NUMBER);
        Assert.assertEquals(entity.getIdCobertura(), SHORT_NUMBER);
        Assert.assertEquals(entity.getIdCoberturaRR8(), SHORT_NUMBER);
        Assert.assertEquals(entity.getDescCobertura(), STRING_TWO);
        Assert.assertEquals(entity.getSumaAsegurada(), NEW_BIG_DECIMAL);
        Assert.assertEquals(entity.getNumCotizacion(), INTEGER_NUMBER);
        Assert.assertEquals(entity.getIdEstatusCobertura(), SHORT_NUMBER);
    }
}
