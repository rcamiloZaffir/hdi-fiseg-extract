package mx.interware.hdi.fiseg.extract.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import mx.interware.hdi.fiseg.extract.exception.ExtractException;

public class UtilitiesTest {

    private static final Logger LOGGER = LogManager.getLogger(UtilitiesTest.class);

    @Test
    public void testDateUtils_parseYmdhms() {
        LOGGER.info("Iniciando test para parseo de fechas");

        String fecha = "2020-10-09 23:39:00.000";// yyyy-MM-dd hh:mm:ss.SSS
        LOGGER.info("String entrada => {}", fecha);

        Calendar c = Calendar.getInstance();
        c.set(2020, Calendar.OCTOBER, 9, 23, 39, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date expected = new Date(c.getTimeInMillis());
        LOGGER.info("Fecha Esperada => {}", expected.toString());
        Date result = DateUtils.parseYmdhms(fecha);
        LOGGER.info("Resultado => {}", result.toString());
        Assert.assertEquals(expected, result);

    }

    @Test
    public void testDateUtils_parseYmdhms_ERROR() {
        LOGGER.info("Iniciando test para parseo de fechas");

        String fecha = "2020-10-09";// yyyy-MM-dd hh:mm:ss.SSS
        try {
            DateUtils.parseYmdhms(fecha);
        } catch (ExtractException e) {
            LOGGER.error("Se obtuvo error esperado");
            Assert.assertTrue(Boolean.TRUE);
        }
    }

    @Test
    public void testDateUtils_parseYmd_ERROR() {
        LOGGER.info("Iniciando test para parseo de fechas");

        String fecha = "2020";
        try {
            DateUtils.parseDateYmd(fecha);
        } catch (ExtractException e) {
            LOGGER.error("Se obtuvo error esperado");
            Assert.assertTrue(Boolean.TRUE);
        }
    }

}
