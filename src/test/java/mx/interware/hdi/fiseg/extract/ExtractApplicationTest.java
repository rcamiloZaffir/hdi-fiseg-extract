package mx.interware.hdi.fiseg.extract;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import mx.interware.hdi.fiseg.extract.config.KafkaConfigurer;
import mx.interware.hdi.fiseg.extract.util.AppProperties;

//@ContextConfiguration(classes = { AppProperties.class })
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@ExtendWith(MockitoExtension.class)
public class ExtractApplicationTest {
	private static Logger log = LogManager.getLogger(ExtractApplicationTest.class);

	//@Test
	public void contextLoads() {
		ApplicationContext ctx = SpringApplication.run(ExtractApplication.class);

		log.info("Testing beans ...");
		String[] beanNames = ctx.getBeanDefinitionNames();
		Arrays.sort(beanNames);
		for (String beanName : beanNames) {
			log.info("bean: " + beanName);
		}

	}

}
