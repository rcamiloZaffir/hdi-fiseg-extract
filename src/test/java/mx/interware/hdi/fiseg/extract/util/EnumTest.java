package mx.interware.hdi.fiseg.extract.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class EnumTest {

    private static final Logger LOGGER = LogManager.getLogger(EnumTest.class);

    @Test
    public void test_LineaNegocio() {
        LOGGER.debug("Test data LineaNegocio");
        Assert.assertTrue(LineaNegocio.AUTR.getDescription().equals("Autos residentes"));
        Assert.assertTrue(LineaNegocio.AUTT.getDescription().equals("Autos turistas"));
        Assert.assertNotNull(LineaNegocio.findById("AUTR"));
        Assert.assertNotNull(LineaNegocio.findById("AUTT"));
        Assert.assertNull(LineaNegocio.findById("XYZ"));
    }

    @Test
    public void test_TipoEvento() {
        LOGGER.debug("Test data TipoEvento");
        Assert.assertTrue(TipoEvento.EXTRACTION_STARTED.getDescription().equals("Extracción iniciada"));
        Assert.assertNotNull(TipoEvento.findById((short)1));
        Assert.assertNull(TipoEvento.findById((short)12));
    }
    

    @Test
    public void test_TipoPoliza() {
        LOGGER.debug("Test data TipoPoliza");
        Assert.assertTrue(TipoPoliza.FLOTILLA.getDescription().equals("Flotilla"));
        Assert.assertNotNull(TipoPoliza.findById((short)4013));
        Assert.assertNull(TipoPoliza.findById((short)4));
    }
}
