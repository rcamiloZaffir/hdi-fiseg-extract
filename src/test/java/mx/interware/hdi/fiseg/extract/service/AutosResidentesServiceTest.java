package mx.interware.hdi.fiseg.extract.service;

import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_BLOCK_SIZE;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_LIMIT_MAX_RECORDS_AUTR;
import static mx.interware.hdi.fiseg.extract.util.Constants.SERVICE_LIMIT_MAX_RECORDS_AUTT;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import mx.interware.hdi.fiseg.extract.dto.MovimientoEventDTO;
import mx.interware.hdi.fiseg.extract.entity.destination.BeneficiarioEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.ClientePolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.ClienteVehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.CoberturaPolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.HistoriaProcesamientoEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.MovimientoPolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.PolizaEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.ProcesoExtraccionEntity;
import mx.interware.hdi.fiseg.extract.entity.destination.VehiculoEntity;
import mx.interware.hdi.fiseg.extract.entity.origin.MovimientoAutoResidenteEntity;
import mx.interware.hdi.fiseg.extract.exception.ExtractException;
import mx.interware.hdi.fiseg.extract.repository.destination.BeneficiarioDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.ClientePolizaDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.ClienteVehiculoDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.CoberturaPolizaDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.CoberturaVehiculoDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.HistoriaProcesamientoDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.MovimientoPolizaDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.PolizaDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.ProcesoExtraccionDAO;
import mx.interware.hdi.fiseg.extract.repository.destination.VehiculoDAO;
import mx.interware.hdi.fiseg.extract.repository.origin.MovimientoAutoResidenteDAO;
import mx.interware.hdi.fiseg.extract.util.AppProperties;
import mx.interware.hdi.fiseg.extract.util.DateUtils;
import mx.interware.hdi.fiseg.extract.util.JsonUtils;
import mx.interware.hdi.fiseg.extract.util.LineaNegocio;
import mx.interware.hdi.fiseg.extract.util.StringUtils;

@ContextConfiguration(classes = { AppProperties.class, EntityHelper.class })
@RunWith(SpringRunner.class)
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class AutosResidentesServiceTest {
    private static Logger LOGGER = LogManager.getLogger(AutosResidentesServiceTest.class);

    @Mock
    private AppProperties appProperties;

    @Autowired
    private EntityHelper entityHelper;

    @Mock
    private KafkaService kafkaService;

    @Mock
    private PolizaDAO polizaDAO;

    @Mock
    private MovimientoPolizaDAO movimientoPolizaDAO;

    @Mock
    private VehiculoDAO vehiculoDAO;

    @Mock
    private ClientePolizaDAO clientePolizaDAO;

    @Mock
    private CoberturaPolizaDAO coberturaPolizaDAO;

    @Mock
    private CoberturaVehiculoDAO coberturaVehiculoDAO;

    @Mock
    private ClienteVehiculoDAO clienteVehiculoDAO;

    @Mock
    private BeneficiarioDAO beneficiarioDAO;

    @Mock
    private MovimientoAutoResidenteDAO movimientoAutoResidenteDAO;

    @Mock
    private ProcesoExtraccionDAO procesoExtraccionDAO;

    @Mock
    private HistoriaProcesamientoDAO tbHisoriaProcesamientoDao;

    @InjectMocks
    private ProcessAutosService autosResidentesService;

    private ProcesoExtraccionEntity proceso;

    private final String jsonFile = "/movimiento-event-autr-01.json";

    private List<MovimientoAutoResidenteEntity> buildMovimientosResidentes(int size) {
        List<MovimientoAutoResidenteEntity> movimientosPoliza = new LinkedList<>();
        String fechaEmisiorStr = "2020-01-01 01:01:01.010";
        String fechaFinVigenciaStr = "2021-01-01 01:01:01.010";
        Short idPoliza = 1;
        Long numPoliza = 11111L;
        Short numDocumento = 1;
        Short idTipoDocumento = 1;
        Short modeloVehiculo = 2020;
        Short idPaquete = 1;

        Short[] idTipos = new Short[] { 4014, 4013, 4014, 4014, 4013 };

        for (int i = 0; i < size; i++) {

            Date fechaEmision = DateUtils.parseYmdhms(fechaEmisiorStr);
            Date fechaFinVigencia = DateUtils.parseYmdhms(fechaFinVigenciaStr);
            Date fechaTransaccion = DateUtils.parseYmdhms(fechaEmisiorStr);
            Integer fechaTransaccionInt = Integer.valueOf(DateUtils.formatDateYmd(fechaTransaccion));

            LOGGER.info("###### fechaEmision: {}", fechaEmision);
            LOGGER.info("###### fechaFinVigencia: {}", fechaFinVigencia);

            MovimientoAutoResidenteEntity movimientoPoliza = new MovimientoAutoResidenteEntity();
            Integer ageId = ((Math.random() > 0.5) ? 1 : 2);

            movimientoPoliza.setIdOficina(idPoliza);
            movimientoPoliza.setNumPoliza(numPoliza);
            movimientoPoliza.setNumDocumento(numDocumento);
            movimientoPoliza.setIdTipoDocumento(idTipoDocumento);
            movimientoPoliza.setNipPerfilAgente(ageId);
            movimientoPoliza.setFechaEmision(new Timestamp(2020, 10, 21, 12, 30, 0, 10));
            movimientoPoliza.setFechaFinVigencia(fechaFinVigencia);
            movimientoPoliza.setFechaTransaccion(fechaTransaccionInt);
            movimientoPoliza.setNumCertificado("1");
            movimientoPoliza.setModeloVehiculo(modeloVehiculo);
            movimientoPoliza.setIdPaquete(idPaquete);
            movimientoPoliza.setIdTipoPoliza(idTipos[i % 4]);
            movimientoPoliza.setNumCotizacion(21);
            movimientoPoliza.setIdGrupoDocumento(1);
            movimientoPoliza.setDescGrupoDocumento("CONTRATO");
            movimientoPoliza.setIdCausaCancelacion(12);
            movimientosPoliza.add(movimientoPoliza);
        }
        return movimientosPoliza;
    }

    public MovimientoEventDTO deserializeEvent(String json) {
        try {
            MovimientoEventDTO event = JsonUtils.deserialize(json);
            return event;
        } catch (Exception ex) {
            throw new ExtractException(ex);
        }
    }

    private MovimientoEventDTO buildEvent(String jsonFile) {
        LOGGER.info("jsonFile: {}", jsonFile);
        InputStream stream = AutosResidentesServiceTest.class.getResourceAsStream(jsonFile);
        LOGGER.info("stream: {}", stream);
        String json = StringUtils.streamAsString(stream);

        MovimientoEventDTO movimientoEvent = deserializeEvent(json);
        return movimientoEvent;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        proceso = new ProcesoExtraccionEntity();

        proceso.setFechaUltimoRegistro(new Timestamp(2020, 10, 21, 12, 30, 0, 10));

        MovimientoEventDTO movimientoEvent = buildEvent(jsonFile);
        PolizaEntity poliza = movimientoEvent.getPoliza();
        MovimientoPolizaEntity movimientoPoliza = poliza.getMovimientos().get(0);
        ClientePolizaEntity clientePoliza = movimientoPoliza.getClientes().get(0);
        CoberturaPolizaEntity coberturaPoliza = movimientoPoliza.getCoberturas().get(0);
        VehiculoEntity vehiculo = movimientoPoliza.getVehiculos().get(0);
        BeneficiarioEntity beneficiario = vehiculo.getBeneficiarios().get(0);
        ClienteVehiculoEntity clienteVehiculo = vehiculo.getClientes().get(0);

        List<MovimientoAutoResidenteEntity> movimientosResidentes = buildMovimientosResidentes(10);

        movimientoPoliza.setMovimientoPolizaId(1L);

        when(appProperties.getProperty(SERVICE_LIMIT_MAX_RECORDS_AUTR)).thenReturn("20");
        when(appProperties.getProperty(SERVICE_LIMIT_MAX_RECORDS_AUTT)).thenReturn("20");
        when(appProperties.getProperty(DS_ORIGIN_BLOCK_SIZE)).thenReturn("10");
        when(procesoExtraccionDAO.findByTipoProcesamiento(LineaNegocio.AUTR.getId())).thenReturn(proceso);

        proceso = new ProcesoExtraccionEntity();
        proceso.setProcesoExtraccionId(10);
        when(procesoExtraccionDAO.save(ArgumentMatchers.any(ProcesoExtraccionEntity.class))).thenReturn(proceso);

        HistoriaProcesamientoEntity historial = new HistoriaProcesamientoEntity();
        historial.setHistoriaProcesamientoId(10L);
        when(tbHisoriaProcesamientoDao.save(ArgumentMatchers.any(HistoriaProcesamientoEntity.class))).thenReturn(historial);
        when(movimientoAutoResidenteDAO.findUnprocessedAUTR(anyObject())).thenReturn(movimientosResidentes);
        when(movimientoPolizaDAO.findByMovimientoStr("AUTR_1_00000_1_20200101010101010_user")).thenReturn(movimientoPoliza);
        when(movimientoPolizaDAO.findByMovimientoStr("AUTR_2_00000_1_20200101010101010_user")).thenReturn(null);
        when(movimientoPolizaDAO.save(anyObject())).thenReturn(movimientoPoliza);
        when(vehiculoDAO.save(anyObject())).thenReturn(vehiculo);
        when(clientePolizaDAO.save(anyObject())).thenReturn(clientePoliza);
        when(coberturaPolizaDAO.save(anyObject())).thenReturn(coberturaPoliza);
        when(beneficiarioDAO.save(anyObject())).thenReturn(beneficiario);
        when(vehiculoDAO.findByIdMovimientoPoliza(anyObject())).thenReturn(null);
        when(clienteVehiculoDAO.findByIdVehiculo(anyObject())).thenReturn(null);
        when(coberturaVehiculoDAO.deleteByIdVehiculo(anyObject())).thenReturn(10);

//        HistoriaProcesamientoEntity startedHistory = new HistoriaProcesamientoEntity();
//        when(entityHelper.buildHistoryEntity(any(TipoEvento.class), LineaNegocio.AUTR, any(String.class), 0, Boolean.TRUE)).thenReturn(startedHistory);

        LOGGER.info("############# entityHelper: {}", entityHelper);
//		when(entityHelper.buildBeneficiario(anyObject())).thenReturn(beneficiario);
        when(clienteVehiculoDAO.save(anyObject())).thenReturn(clienteVehiculo);

        when(polizaDAO.save(anyObject())).thenReturn(poliza);
        doNothing().when(kafkaService).sendEvent(anyObject(), anyObject(), anyObject());

    }

    @Test
    public void testExtraction() {
        try {
            LOGGER.info("Testing [Autos residentes] extraction ...");

            autosResidentesService.setLineaNegocio(LineaNegocio.AUTR);
            try {
                FieldUtils.writeField(autosResidentesService, "appProperties", appProperties, true);
                FieldUtils.writeField(autosResidentesService, "entityHelper", entityHelper, true);
            } catch (Exception e) {
                LOGGER.info("Error al setear los elementos {}", e);
            }
            String processType = LineaNegocio.AUTR.getId();
            String extractResult = autosResidentesService.executeExtraction(processType);

            MovimientoEventDTO movimientoEvent = buildEvent(jsonFile);
            String eventStr = JsonUtils.toJson(movimientoEvent);
            LOGGER.info("evventStr : {}", eventStr);

//            Assert.assertEquals(SUCCESS, extractResult);

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

}
