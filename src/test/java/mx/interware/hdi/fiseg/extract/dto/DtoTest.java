package mx.interware.hdi.fiseg.extract.dto;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class DtoTest {

    private final Short SHORT_NUMBER = 1;
    private final Integer INTEGER_NUMBER = 1;
    private final Date DATE_ONE = new Date();
    private final String STRING_ONE = "XYZ";
    private final String STRING_TWO = "abc01";
    private final String STRING_THREE = "uoiea01";
    private final String STRING_FOUR = "9ijn8uhb";

    @Test
    public void test_MovimientoDTO() {
        MovimientoDTO dto = new MovimientoDTO();

        dto.setLineaNegocio(STRING_ONE);
        dto.setAgencia(STRING_TWO);
        dto.setPoliza(STRING_THREE);
        dto.setCotizacion(STRING_FOUR);
        dto.setClaveEndoso(INTEGER_NUMBER);
        dto.setEstatus(SHORT_NUMBER);
        dto.setFechaEmision(DATE_ONE);
        Assert.assertEquals(dto.getLineaNegocio(), STRING_ONE);
        Assert.assertEquals(dto.getAgencia(), STRING_TWO);
        Assert.assertEquals(dto.getPoliza(), STRING_THREE);
        Assert.assertEquals(dto.getCotizacion(), STRING_FOUR);
        Assert.assertEquals(dto.getClaveEndoso(), INTEGER_NUMBER);
        Assert.assertEquals(dto.getEstatus(), SHORT_NUMBER);
        Assert.assertEquals(dto.getFechaEmision(), DATE_ONE);

    }

    @Test
    public void testBeneficiarioEntity() {
        MovimientoEventDTO dto = new MovimientoEventDTO();
        dto.setEventId(STRING_ONE);
        Assert.assertEquals(dto.getEventId(), STRING_ONE);
    }

}
