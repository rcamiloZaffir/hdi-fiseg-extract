package mx.interware.hdi.fiseg.extract.rest;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import mx.interware.hdi.fiseg.extract.config.KafkaConfigurer;
import mx.interware.hdi.fiseg.extract.util.AppProperties;

@ContextConfiguration(classes = { AppProperties.class })
@RunWith(SpringRunner.class)
@SpringBootTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@ComponentScan(excludeFilters = { @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = { KafkaConfigurer.class }) })
public class HealthControllerTest {
	private static Logger log = LogManager.getLogger(HealthControllerTest.class);

	@Autowired
	private MockMvc mvc;

	@Test
	public void pingTest() throws Exception {
		log.info("Testing ping method...");
		String expected = "pong";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/ping");
		mvc.perform(requestBuilder.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(content().string(equalTo(expected)));
	}

	@Test
	public void healthTest() throws Exception {
		log.info("Testing health method ...");
		String expected = "{'action':'verifyHealth', 'key':'kptzin', 'status':'OK'}";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/health/kptzin");
		mvc.perform(requestBuilder.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(content().string(equalTo(expected)));
	}

	@Test
	public void helloTest() throws Exception {
		log.info("Testing sayHi method ...");
		String name = "kptzin";
		String expected = "Hello there " + name + "!";
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/sayHi/" + name);
		mvc.perform(requestBuilder.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(content().string(equalTo(expected)));
	}

}
