package mx.interware.hdi.fiseg.config;

import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_BOOTSTRAP_ADDRESS;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTR_TOPIC;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTT_TOPIC;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import mx.interware.hdi.fiseg.extract.config.KafkaConfigurer;
import mx.interware.hdi.fiseg.extract.util.AppProperties;

@ContextConfiguration(classes = { AppProperties.class })
@RunWith(SpringRunner.class)
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class KafkaConfigurerTest {
    private static Logger log = LogManager.getLogger(KafkaConfigurerTest.class);

    @Mock
    private AppProperties appProperties;

    @InjectMocks
    private KafkaConfigurer KafkaConfigurer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(appProperties.getProperty(KAFKA_BOOTSTRAP_ADDRESS)).thenReturn("");
        Mockito.when(appProperties.getProperty(KAFKA_EXTRACT_AUTR_TOPIC)).thenReturn("");
        Mockito.when(appProperties.getProperty(KAFKA_EXTRACT_AUTT_TOPIC)).thenReturn("");
    }

    @Test
    public void test_autrTopic() {
        NewTopic obj = KafkaConfigurer.autrTopic();
        Assert.assertNotNull(obj);
    }

    @Test
    public void test_auttTopic() {
        NewTopic obj = KafkaConfigurer.auttTopic();
        Assert.assertNotNull(obj);
    }

    @Test
    public void test_kafkaAdmin() {
        KafkaAdmin obj = KafkaConfigurer.kafkaAdmin();
        Assert.assertNotNull(obj);
    }

    @Test
    public void test_producerFactory() {
        ProducerFactory<String, String> obj = KafkaConfigurer.producerFactory();
        Assert.assertNotNull(obj);
    }

    @Test
    public void test_kafkaTemplate() {
        KafkaTemplate<String, String> obj = KafkaConfigurer.kafkaTemplate();
        Assert.assertNotNull(obj);
    }

}
