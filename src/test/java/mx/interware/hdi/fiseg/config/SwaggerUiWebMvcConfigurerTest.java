package mx.interware.hdi.fiseg.config;

import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.util.UrlPathHelper;

import mx.interware.hdi.fiseg.extract.config.SwaggerUiWebMvcConfigurer;

public class SwaggerUiWebMvcConfigurerTest {

    @InjectMocks
    private SwaggerUiWebMvcConfigurer swaggerUiWebMvcConfigurer;
    private ResourceHandlerRegistry resourceHandlerRegistry;
    private ResourceHandlerRegistration registration;
    private MockHttpServletResponse response;
    private GenericWebApplicationContext appContext;
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        appContext= new GenericWebApplicationContext();
        appContext.refresh();
        resourceHandlerRegistry = new ResourceHandlerRegistry(appContext, new MockServletContext(),
                new ContentNegotiationManager(), new UrlPathHelper());

        this.registration = this.resourceHandlerRegistry.addResourceHandler("/resources/**");
        this.registration.addResourceLocations("classpath:org/springframework/web/servlet/config/annotation/");
        this.response = new MockHttpServletResponse();
    }

    @Test
    public void addResourceHandlers() {
        swaggerUiWebMvcConfigurer.addResourceHandlers(new ResourceHandlerRegistry(new GenericWebApplicationContext(), new MockServletContext()));
        assertNotEquals(null,appContext);
    }

    @Test
    public void addViewControllers() {
        swaggerUiWebMvcConfigurer.addViewControllers(new ViewControllerRegistry(appContext) );
        assertNotEquals(null,appContext);
    }
}