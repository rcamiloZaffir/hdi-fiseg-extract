package mx.interware.hdi.fiseg.config;

import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_DRIVER;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_DESTINATION_URL;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_BLOCK_SIZE;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_DRIVER;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_SCHEMA;
import static mx.interware.hdi.fiseg.extract.util.Constants.DS_ORIGIN_URL;
import static mx.interware.hdi.fiseg.extract.util.Constants.KAFKA_EXTRACT_AUTR_TOPIC;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;

import mx.interware.hdi.fiseg.extract.config.DestinationDBConfigurer;
import mx.interware.hdi.fiseg.extract.config.OriginDBConfigurer;
import mx.interware.hdi.fiseg.extract.util.AppProperties;

@ContextConfiguration(classes = { AppProperties.class })
@RunWith(SpringRunner.class)
@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class DBConfigurerTest {
    private static Logger log = LogManager.getLogger(DBConfigurerTest.class);

    @Mock
    private AppProperties appProperties;
    @Mock
    private DataSource dataSource;

    @InjectMocks
    private DestinationDBConfigurer dBConfigurerDest;

    @InjectMocks
    private OriginDBConfigurer dBConfigurerOri;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(appProperties.getProperty(KAFKA_EXTRACT_AUTR_TOPIC)).thenReturn("");
        Mockito.when(appProperties.getProperty(DS_ORIGIN_DRIVER)).thenReturn("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Mockito.when(appProperties.getProperty(DS_DESTINATION_DRIVER)).thenReturn("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    }

    @Test
    public void testGetEM() {
        Mockito.when(appProperties.getProperty(DS_DESTINATION_URL)).thenReturn("jdbc:sqlserver://localhost:1433;databaseName=fiseg");
        Mockito.when(appProperties.getProperty(DS_ORIGIN_URL)).thenReturn("jdbc:sqlserver://localhost:1433;databaseName=fiseg");
        LocalContainerEntityManagerFactoryBean emDest = dBConfigurerDest.destinationEntityManager();
        LocalContainerEntityManagerFactoryBean emOri = dBConfigurerOri.originEntityManager();
        Assert.assertNotNull(emDest);
        Assert.assertNotNull(emOri);
    }

    @Test
    public void testGetDS_01() {
        Mockito.when(appProperties.getProperty(DS_DESTINATION_URL)).thenReturn("amRiYzpzcWxzZXJ2ZXI6Ly9sb2NhbGhvc3Q6MTQzMztkYXRhYmFzZU5hbWU9ZmlzZWc=");
        Mockito.when(appProperties.getProperty(DS_ORIGIN_URL)).thenReturn("amRiYzpzcWxzZXJ2ZXI6Ly9sb2NhbGhvc3Q6MTQzMztkYXRhYmFzZU5hbWU9ZmlzZWc=");
        DataSource emDest = dBConfigurerDest.destinationDataSource();
        DataSource emOri = dBConfigurerOri.originDataSource();
        Assert.assertNotNull(emDest);
        Assert.assertNotNull(emOri);
    }

    @Test
    public void testGetDS_02() {
        Mockito.when(appProperties.getProperty(DS_ORIGIN_URL)).thenReturn("jdbc:sqlserver://localhost:1433;databaseName=fiseg");
        Mockito.when(appProperties.getProperty(DS_DESTINATION_URL)).thenReturn("jdbc:sqlserver://localhost:1433;databaseName=fiseg");
        DataSource emDest = dBConfigurerDest.destinationDataSource();
        DataSource emOri = dBConfigurerOri.originDataSource();
        Assert.assertNotNull(emDest);
        Assert.assertNotNull(emOri);
    }

    @Test
    public void testGetTM() {
        Mockito.when(appProperties.getProperty(DS_ORIGIN_URL)).thenReturn("jdbc:sqlserver://localhost:1433;databaseName=fiseg");
        Mockito.when(appProperties.getProperty(DS_DESTINATION_URL)).thenReturn("jdbc:sqlserver://localhost:1433;databaseName=fiseg");
        PlatformTransactionManager emDest = dBConfigurerDest.destinationTransactionManager();
        PlatformTransactionManager emOri = dBConfigurerOri.originTransactionManager();
        Assert.assertNotNull(emDest);
        Assert.assertNotNull(emOri);
    }

    @Test
    public void testGetSQLI() {
        Mockito.when(appProperties.getProperty(DS_ORIGIN_SCHEMA)).thenReturn("Amis");
        Mockito.when(appProperties.getProperty(DS_ORIGIN_BLOCK_SIZE)).thenReturn("100");

        EmptyInterceptor emDest = dBConfigurerDest.destinationSqlInterceptor();
        EmptyInterceptor emOri = dBConfigurerOri.originSqlInterceptor();

        Assert.assertNotNull(emDest.onPrepareStatement("A"));
        Assert.assertNotNull(emOri.onPrepareStatement("A"));
    }
}
