# Proyecto REST base para Fiscalización de seguros #


### Arranque del servicio
```
mvn clean spring-boot:run
```

## Servicios

### Swagger

En un navegador ingresar a la URL <http://localhost:8090/swagger-ui/index.html>

### Servicio ping

Desde la linea de comandos, ejecutar:
```
curl http://localhost:8090/api/ping
```

### Servicio de salud

Desde la linea de comandos, ejecutar:
```
curl http://localhost:8090/api/health/k1
```

### schema-registry
Para agregar un esquema al servicio schema-registry
```
curl -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
    --data @src/main/json/MovimientoPolizaSchema.json \
    http://localhost:8081/subjects/MovimientoPoliza/versions
```


curl -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
    --data "{\"compatibility\": \"backward\"}" \
    http://localhost:8081/config/MovimientoPoliza

## Instalación en docker swarm

Para instalar el servicio, es necesario realizar los siguientes pasos:

### Crear red

Para verificar que exista: 
```bash
docker network ls | grep hdi_fiseg_data_net
```

Para crear la red: 
```bash
docker network create -d overlay hdi_fiseg_data_net
```

### Construir imagen docker

```bash
docker build -t dockerhub:5000/hdi-fiseg-extract:$(egrep version_servicio pom.xml | awk -F">" '{print $2}' | awk -F"<" '{print $1}') -f docker/Dockerfile .
```
Donde:
* $(egrep version_servicio pom.xml | awk -F">" '{print $2}' | awk -F"<" '{print $1}'): Obtiene la versión que esta en el archivo **pom.xml**

### Correr docker container en docker swarm(docker-compose)
Substituir la definición del docker compose:

```bash
version: '3'

services:

  extract:
    image: dockerhub:5000/hdi-fiseg-extract:0.1.2-SNAPSHOT
    restart: always
    ports:
      - 8090:8090
    environment:
      - SQL_HDI_DHW_J1=jdbc:sqlserver://dev.interware.com.mx:1433;databaseName=HDI_DWH
      - FISEG_DS_ORIGIN_USERNAME=sa
      - FISEG_DS_ORIGIN_PASSWORD=myPassword
      - FISEG_DS_ORIGIN_SCHEMA=dbo
      - SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1=jdbc:sqlserver://dev.interware.com.mx:1433;databaseName=fiseg
      - FISEG_DS_DESTINATION_USERNAME=sa
      - FISEG_DS_DESTINATION_PASSWORD=myPassword
      - FISEG_DS_DESTINATION_SCHEMA=dbo
      - FISEG_DS_DESTINATION_DDL_AUTO=none  
      - FISEG_RESIDENTES_CRON_EXPRESSION=0 0/2 * * * *
      - FISEG_TURISTAS_CRON_EXPRESSION=55 0/2 * * * *
      - FISEG_KAFKA_BOOTSTRAP_ADDRESS=dev.interware.com.mx:9092
      - FISEG_KAFKA_EXTRACT_AUTR_TOPIC=extract_autr_topic
      - FISEG_KAFKA_EXTRACT_AUTT_TOPIC=extract_autt_topic
    networks:
      - hdi_fiseg_net

networks:
  hdi_fiseg_net:
    external:
        name: hdi_fiseg_data_net         


```
**Campos a actualizar:**

* **image**: Actualizar **dockerhub:5000** con el hostname y puerto del docker registry. También actualizar la **versión** que deseamos instalar
* **ports**: Si deseamos que nuestro servicio se exponga en otro puerto cambiar el puerto del lazo izquierdo
* **SQL_HDI_DHW_J1**: URL de la base de datos SQL origen 
* **FISEG_DS_ORIGIN_USERNAME**: Usuario de la base de datos SQL origen 
* **FISEG_DS_ORIGIN_PASSWORD**: Contraseña de la base de datos SQL origen
* **FISEG_DS_ORIGIN_SCHEMA**: Esquema de la base de datos SQL origen
* **SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1**:URL de la base de datos SQL destino
* **FISEG_DS_DESTINATION_USERNAME**: Usuario de la base de datos SQL destino
* **FISEG_DS_DESTINATION_PASSWORD**: Contraseña de la base de datos SQL destino
* **FISEG_DS_DESTINATION_SCHEMA**: Esquema de la base de datos SQL destino
* **FISEG_RESIDENTES_CRON_EXPRESSION**:Expresion regular para establecer a que hora se extrae la información para Autos residentes
* **FISEG_TURISTAS_CRON_EXPRESSION**: Expresion regular para establecer a que hora se extrae la información para Autos Turistas
* **FISEG_KAFKA_BOOTSTRAP_ADDRESS**: Hostname y puerto donde esta corriendo el broker de Kafka
* **FISEG_KAFKA_EXTRACT_AUTR_TOPIC**: Topico de Kafka donde se depositaran los mensajes para Autos Residentes
* **FISEG_KAFKA_EXTRACT_AUTT_TOPIC**: Topico de Kafka donde se depositaran los mensajes para Autos Residentes

Ejecutar el siguiente comando:
```bash
docker stack deploy --prune --with-registry-auth --resolve-image always --compose-file=docker/docker-compose.yml hdi-fiseg-extract
```

### Correr docker container en docker swarm(docker service)

```bash
docker service create  \
--name hdi-fiseg-extract \
--env SQL_HDI_DHW_J1=jdbc:sqlserver://dev.interware.com.mx:1433;databaseName=HDI_DWH \
--env FISEG_DS_ORIGIN_USERNAME=sa \
--env FISEG_DS_ORIGIN_PASSWORD=myPassword \
--env FISEG_DS_ORIGIN_SCHEMA=dbo \
--env SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1=jdbc:sqlserver://dev.interware.com.mx:1433;databaseName=fiseg \
--env FISEG_DS_DESTINATION_USERNAME=sa \
--env FISEG_DS_DESTINATION_PASSWORD=myPassword \
--env FISEG_DS_DESTINATION_SCHEMA=dbo \
--env FISEG_DS_DESTINATION_DDL_AUTO=none \  
--env FISEG_RESIDENTES_CRON_EXPRESSION=0 0/2 * * * * \
--env FISEG_TURISTAS_CRON_EXPRESSION=55 0/2 * * * * \
--env FISEG_KAFKA_BOOTSTRAP_ADDRESS=dev.interware.com.mx:9092 \
--env FISEG_KAFKA_EXTRACT_AUTR_TOPIC=extract_autr_topic \
--env FISEG_KAFKA_EXTRACT_AUTT_TOPIC=extract_autt_topic \
--publish 8090:8090 \
dockerhub:5000/hdi-fiseg-extract:0.0.0 

```
**Donde:**

 * **image**: **dockerhub:5000** es el hostname y puerto del docker registry. 
 * **0.0.0** Es la **versión** que deseamos instalar
 * **publish**: Si deseamos que nuestro servicio se exponga en otro puerto cambiar el puerto del lazo izquierdo
 * **SQL_HDI_DHW_J1**: URL de la base de datos SQL origen 
 * **FISEG_DS_ORIGIN_USERNAME**: Usuario de la base de datos SQL origen 
 * **FISEG_DS_ORIGIN_PASSWORD**: Contraseña de la base de datos SQL origen
 * **FISEG_DS_ORIGIN_SCHEMA**: Esquema de la base de datos SQL origen
 * **SQL_MSSQL_MOVIMIENTOSBLOCKCHAIN_J1**:URL de la base de datos SQL destino
 * **FISEG_DS_DESTINATION_USERNAME**: Usuario de la base de datos SQL destino
 * **FISEG_DS_DESTINATION_PASSWORD**: Contraseña de la base de datos SQL destino
 * **FISEG_DS_DESTINATION_SCHEMA**: Esquema de la base de datos SQL destino
 * **FISEG_RESIDENTES_CRON_EXPRESSION**:Expresion regular para establecer a que hora se extrae la información para Autos residentes
 * **FISEG_TURISTAS_CRON_EXPRESSION**: Expresion regular para establecer a que hora se extrae la información para Autos Turistas
 * **FISEG_KAFKA_BOOTSTRAP_ADDRESS**: Hostname y puerto donde esta corriendo el broker de Kafka
 * **FISEG_KAFKA_EXTRACT_AUTR_TOPIC**: Topico de Kafka donde se depositaran los mensajes para Autos Residentes
 * **FISEG_KAFKA_EXTRACT_AUTT_TOPIC**: Topico de Kafka donde se depositaran los mensajes para Autos Residentes
